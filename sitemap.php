<?php

require_once 'kohana.php';

/** @var $root Model_Pages */
$root = ORM::factory('Pages', 1);
if(!$root->loaded()) return false;
$objects = $root->descendants(FALSE, FALSE)
    ->where('hide', '=', 0)
    ->and_where('created', '<=', DB::expr('NOW()'))
    ->order_by('priority', 'ASC')
    ->find_all()->as_array();

$host = 'www.web-ar.ru';
$items[] = array(
    'loc' => 'http://'.$host.'/'
);

foreach($objects as $_object) {
    $module = strtolower($_object->module);
    switch($module) {
        case 'articles':
            $articles = ORM::factory('Article')
                ->where('hide', '=', 0)
                ->and_where('created', '<=', DB::expr('NOW()'))
                ->order_by('created', 'DESC')
                ->find_all()
                ->as_array();

            $items[] = array(
                'loc' => 'http://'.$host.$_object->fullpath.'/',
            );

            foreach($articles as $_article) {
                /** @noinspection PhpUndefinedClassInspection */
                $items[] = array(
                    'loc' => 'http://'.$host.'/'.Route::get('public_articles')->uri(array('alias' => $_article->alias)).'/'
                );
            }
        break;

        case 'services':
            $services = ORM::factory('Service')
                ->where('hide', '=', 0)
                ->and_where('created', '<=', DB::expr('NOW()'))
                ->order_by('created', 'DESC')
                ->find_all()
                ->as_array();

            $items[] = array(
                'loc' => 'http://'.$host.$_object->fullpath.'/',
            );

            foreach($services as $_service) {
                /** @noinspection PhpUndefinedClassInspection */
                $items[] = array(
                    'loc' => 'http://'.$host.'/'.Route::get('public_services')->uri(array('alias' => $_service->alias)).'/'
                );
            }
        break;

        default:
            $items[] = array(
                'loc' => 'http://'.$host.$_object->fullpath.'/',
            );
        break;
    }
}

$template = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
$template .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
foreach($items as $_item) {
    $template .= '<url>
        <loc>'.$_item['loc'].'</loc>
        <changefreq>monthly</changefreq>
    </url>'.PHP_EOL;
}

$template .= '</urlset>';

// Flush to file
$filename = DOCROOT.'sitemap.xml';
$handle = fopen($filename, 'w');
fwrite($handle, $template);
fclose($handle);