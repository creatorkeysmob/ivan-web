/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    /*config.toolbar = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'others', items: [ '-' ] },
        { name: 'about', items: [ 'About' ] }
    ];*/

    config.toolbar_Basic =
        [
            ['Source'],  ['Bold', 'Italic','Underline','Strike'], ['Link','Unlink'], ['RemoveFormat']
        ];

    config.toolbar_Default =
        [
            ['Source','Templates','Print'],
            ['Cut','Copy','Paste','PasteText', 'PasteFromWord', 'SpellChecker'],
            ['Undo','Redo','Find','Replace','RemoveFormat'],
            ['NumberedList','BulletedList','Outdent','Indent','Blockquote','CreateDiv'],
            ['Maximize', 'ShowBlocks'],
            ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
            ['TextColor','BGColor'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink'],
            ['Image','Flash','Youtube','Table','HorizontalRule','SpecialChar'],
            ['Styles','Format']
        ];

    config.toolbar_Full =
        [
            ['Source','-','Save','NewPage','Preview','-','Templates'],
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
            '/',
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink','Anchor'],
            ['Image','Flash','Youtube','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
            '/',
            ['Styles','Format','Font','FontSize'],
            ['TextColor','BGColor'],
            ['Maximize', 'ShowBlocks','-','About']
        ];

    config.resize_enabled = false;
    config.toolbarCanCollapse = false;
    config.toolbarGroupCycling = false;
    config.templates_replaceContent = false;

    config.allowedContent = true;
    config.extraAllowedContent = [
        'ul[class]', 'div[class]'
    ];

    // Filebrowser
    config.filebrowserBrowseUrl = '/assets/elfinder/elfinder.html?mode=file';
    config.filebrowserImageBrowseUrl = '/assets/elfinder/elfinder.html?mode=image';
    config.filebrowserFlashBrowseUrl = '/assets/elfinder/elfinder.html?mode=flash';
    config.filebrowserWindowWidth = '980';
    config.filebrowserWindowHeight = '600';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.fillEmptyBlocks = false;
};
