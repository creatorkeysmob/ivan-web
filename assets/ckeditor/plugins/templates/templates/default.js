﻿/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.addTemplates("default", {imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"), templates: [
    {
        title: "Заголовок с иконкой",
        image: "template1.gif",
        description: "Шаблон заголовка с иконкой",
        html: '<div class="content-title-icon"><img alt="" src="/upload/user/icons/medium/icon-stat.png" /><h2>Продвижение по ЛИДам</h2></div>'
    },
    {
        title: "Список с иконками",
        image: "template1.gif",
        description: "Шаблон списка с иконками",
        html: '<ul class="iconic"><li><img src="/upload/user/icons/small/icon-case.png" alt=""/> Транзакция</li><li><img src="/upload/user/icons/small/icon-list.png" alt=""/> Заполнение формы обратной связи</li><li><img src="/upload/user/icons/small/icon-bubble.png" alt=""/> Звонок в офис компании</li><li><img src="/upload/user/icons/small/icon-stat.png" alt=""/> Увеличение оборота</li><li><img src="/upload/user/icons/small/icon-marker.png" alt=""/> Регистрация на сайте</li><li><img src="/upload/user/icons/small/icon-search.png" alt=""/> Просмотр ключевой страницы</li><li><img src="/upload/user/icons/small/icon-mail.png" alt=""/> Клик на e-mail</li></ul><div class="content-title-icon"><img alt="" src="/upload/user/icons/medium/icon-stat.png" /><h2>Продвижение по ЛИДам</h2></div>'
    },
    {
        title: "Акция",
        image: "template1.gif",
        description: "Шаблон акции (У ссылки класс popup-call или popup-work для всплывающих окон)",
        html: '<div class="content-stock"> <div class="content-stock-inner"> <img src="/upload/user/icons/big/stock.png" alt=""/> <div class="content-stock-head">Акция в марте!</div> <div class="content-stock-text"> Успейте заказать продвижение сайта с оплатой за результат!<br /> <a href="#">Подробнее здесь</a> или звоните по телефону (343) 382-53-70. </div> </div> </div>'
    }

]});