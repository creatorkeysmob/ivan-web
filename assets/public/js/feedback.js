var doc = $(document);

/***************** Галка *****************/

doc.on( 'change', '.section_argee', function(){
    var btn = $(this).parent().siblings('.form_send');
    if ( $(this).prop('checked') ) btn.removeClass('disabled')
    else btn.addClass('disabled');
} );

doc.on('focus', 'input[type="text"], input[type="email"], input[type="search"], textarea', function(){
    $(this).prop('placeholder', '');
});
doc.on('blur', 'input[type="text"], input[type="email"], input[type="search"], textarea', function(){
    var pls = $(this).data('pls');
    $(this).prop('placeholder', pls);
});


/***************** Фидбеки *****************/

$(document).on('click', '.form_send:not(.disabled)', function () {
    var msg   = '<table>',
        table = $(this).data('table'),
        title = $(this).data('title'),
        form  =  $(this).closest("form"),
        input = form.find('input:not(.agreement__checkbox,[type=submit]), textarea, select');
    input.each(
            function () {
                $(this).blur();
                var val   = $(this).val();
                var title = $(this).data('title');

                if ( val && val != '' ){
                    if ( $(this).prop('type') == 'file' ) {
                        msg += `<tr><td><b>${title}</b>: </td><td> `;
                        var N = 0;
                        $(this).parent().find('.form_file_list span:not(.error)').each(function(){
                            msg = (N > 0) ? msg + ', ' : msg;
                            msg += $(this).text().replace('×', '').trim();
                            N++;
                        });
                        msg += '</td></tr>';
                    } else if ( $(this).prop('type') == 'checkbox' ) {
                        if ( $(this).hasClass('checkboxes') ) {
                            if ( $(this).prop('checked') ){
                                msg += `<tr><td><b>${title}</b>: </td><td> ${val} </td></tr>`;
                            }
                        } else {
                            val = ( $(this).prop('checked') ) ? 'Да' : 'Нет';
                            msg += `<tr><td><b>${title}</b>: </td><td> ${val} </td></tr>`;
                        }
                    } else if ( $(this).prop('type') == 'radio' ) {
                        if ( $(this).prop('checked') ){
                            msg += `<tr><td><b>${title}</b>: </td><td> ${val} </td></tr>`;
                        }
                    } else {
                        msg += `<tr><td><b>${title}</b>: </td><td> ${val} </td></tr>`;
                    }
                }

            }
    );
    msg += '</table>';

    if (!input.hasClass('error')) {
        $(this).addClass('disabled');
        var data = new FormData,
            N = 0;
        form.find('.form_file_list span:not(.error)').each(function(){
            var id = $(this).data('id');
            data.append('file_'+N,  $(this).parent().parent().find('.form_file').prop('files')[id]);
            N++;
        });
        data.append('text',  msg);
        data.append('table', table);
        data.append('title', title);
        $.ajax({
            url: "/api/public_forms.sendForm",
            type: "POST",
            dataType: "JSON",
            data: data,
            processData: false,
            contentType: false,
            success: function (response) {
                $('.form_send').removeClass('disabled');
                input.val('').prop('checked', false).trigger('change');
                $('.form_file_list').html('');
                console.log('да');
                form.addClass('hidden').siblings('.tnx_msg').removeClass('hidden');
                setTimeout(function(){
                  form.removeClass('hidden').siblings('.tnx_msg').addClass('hidden');
                },5000);
            }
        });
    } else {
        console.log('нет');
    }

    return false;
});


/**************** Валидация ****************/
doc.on('click', '.required', function () {
    $(this).removeClass('error');
});

doc.on('blur', '.required', function () {

    var type = $(this)[0].tagName;
    if ( type == 'INPUT' || type == 'TEXTAREA' || type == 'SELECT' ) {
        var val = $(this).val();
    } else if ( type == 'DIV' ) {
        var val = $(this).text();
    }
    var func = validateEmpty(val);
    if ($(this).hasClass('mail')) {
        func = validateEmail(val);
    } else if ($(this).hasClass('phone')) {
        func = validatePhone(val);
    }
    if (!func) {
        $(this).addClass('error');
    } else {
        $(this).removeClass('error');
    }
});

/*******************************************/

/* только цифры */
doc.on('keydown', '.numbers', function(event) {
     if (
         event.keyCode === 46 ||
         event.keyCode === 8 ||
         event.keyCode === 9 ||
         event.keyCode === 27 ||
         (event.keyCode === 65 && event.ctrlKey === true) ||
         (event.keyCode >= 35 && event.keyCode <= 39)
     ) {
         return;
     } else {
         if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
             event.preventDefault();
             $(this).css('border-color', '#dd0000');
         }
     }
});
doc.on('mousedown', '.numbers', function(event) {
    if ( event.button != 0 ) {
        $(this).blur();
        return false;
    }
});
doc.on('keyup', '.numbers', function(event) {
     $(this).css('border-color', '');
});

doc.on('change', '.form_file', function(){
    var types  = $(this).data('type').toLowerCase();
        parent = $(this).parent().find('.form_file_list');
        parent.html('');
    $.each($(this).prop('files'), function( index, value ) {
        var ext = value.name.split('.').pop().toLowerCase();
            re  = types.indexOf(ext, 0);
        if ( re < 0 ) {
            var text = '<span class="error">'+value.name+' (не будет прикреплён)</span>';
        } else {
            var text = '<span data-id="'+index+'">'+value.name+'<b class="remove_file_form">×<b></span>';
        }
        parent.append(text);
    });
});

doc.on('click', '.remove_file_form', function(){
    var text = $(this).parent().text().replace('×', '');
        html = text+' (не будет прикреплён)<b class="back_file_form">↻<b>';
        $(this).parent().addClass('error').html(html);
});

doc.on('click', '.back_file_form', function(){
    var text = $(this).parent().text().replace(' (не будет прикреплён)↻', '');
        html = text+'<b class="remove_file_form">×<b>';
        $(this).parent().removeClass('error').html(html);
});

/*******************************************/


$( function() {

    $("select.select2").select2();

/**************** Календарь ****************/
    $( ".form_calendar" ).datepicker({
          currentText: 'Сейчас',
          closeText: 'Закрыть',
          prevText: '<Пред',
          nextText: 'След>',
          monthNames: ['Январь','Февраль','Март','Апрель','Май', 'Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
          monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
          dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
          dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
          dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
          weekHeader: 'Не',
          dateFormat: 'dd.mm.yy',
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''
    });


    $( ".form_calendar_time" ).datetimepicker({
          hour: 9,
          minute: 00,
          currentText: 'Сейчас',
          closeText: 'Закрыть',
          prevText: '<Пред',
          nextText: 'След>',
          monthNames: ['Январь','Февраль','Март','Апрель','Май', 'Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
          monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
          dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
          dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
          dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
          weekHeader: 'Не',
          dateFormat: 'dd.mm.yy',
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: '',

          timeOnlyTitle: 'Выберите время',
          timeText: 'Время',
          hourText: 'Часы',
          minuteText: 'Минуты',
          secondText: 'Секунды',
          millisecText: 'Миллисекунды',
          timezoneText: 'Часовой пояс',
          currentText: 'Сейчас',
          closeText: 'Закрыть',
          timeFormat: 'HH:mm',
          amNames: ['AM', 'A'],
          pmNames: ['PM', 'P'],
          isRTL: false
    });


    $( ".form_time" ).timepicker({
          hour: 9,
          minute: 00,
          timeOnlyTitle: 'Выберите время',
          timeText: 'Время',
          hourText: 'Часы',
          minuteText: 'Минуты',
          secondText: 'Секунды',
          millisecText: 'Миллисекунды',
          timezoneText: 'Часовой пояс',
          currentText: 'Сейчас',
          closeText: 'Закрыть',
          timeFormat: 'HH:mm',
          amNames: ['AM', 'A'],
          pmNames: ['PM', 'P'],
          isRTL: false
    });

/*******************************************/

});



function validateEmpty(text) {
    if (text === '') {
        if  (text.trim() === '') return false;
        return false;
    } else {
        return true;
    }
}

function validateEmail(email) {
    if (email === '')
        return false;
    var re = /([\w.-]+)@([\w.-]+).([\w.-]+)/; // это МОЯ
    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    if (phone === '')
        return false;
    var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/;
    return re.test(phone);
}
