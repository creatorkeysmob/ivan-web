$(function () {

    var Menu = {
        headerMenu: $('.nav__overlay').eq(0),
        menuItem: $('.menu__item'),
        menuContacts: $('.nav_contacts').eq(0),
        menuBtn: $('.nav-toggle').eq(0),

        toggleMenu: function() {
            if (!this.headerMenu.hasClass('js-menu__open')) {
                this.headerMenu.animate({
                    right: 0,
                    opacity: 1,
                }, 500);

                var index = 0;
                function setProperties() {
                    Menu.menuItem.eq(index).css({
                        'opacity': '1',
                        'transform': 'initial',
                    })
                    index++;
                    if (index < Menu.menuItem.length) {
                        setTimeout(setProperties, 60);
                    }
                }
                setTimeout(setProperties, 120)

            } else {
                this.headerMenu.delay(120).animate({
                    right: '-100%',
                    opacity: 0.75,
                }, 500);

                var index = 0;
                function setProperties() {

                    Menu.menuItem.eq(index).css({
                        'opacity': '0',
                        'transform': 'matrix(0.8, 0, 0, 0.8, 100, 0)',
                    })
                    index++;
                    if (index < Menu.menuItem.length) {
                        setTimeout(setProperties, 60);
                    }
                }
                setProperties();

            };

            this.headerMenu.toggleClass('js-menu__open');
            this.menuBtn.toggleClass('nav-toggle_active');

        },
    }

    Menu.menuBtn.on('click', function() {
        Menu.toggleMenu();
    });




    //button color
    $('.main-slider').find('.main-slider__btn_more').each(function(i){
        var $btnColor = $(this).data('color');
        var $btnHoverColor = $(this).data('hovercolor');
        $(this).css("background-color", $btnColor);
        $(this).hover(
            function(){
                $(this).css("background-color", $btnHoverColor);
            },
            function(){
                $(this).css("background-color", $btnColor);
            }
        );
    });
    //button nav color
    $('.main-slider__item-wrap').on('afterChange', function(event, slick, currentSlide, nextSlide) {        
        var elSlide = $(slick.$slides[currentSlide]);
        var $navBorderColor = elSlide.data('bordercolor');
        var $navHoverColor = elSlide.data('navhovercolor');        
        var $navArrows = $('.slick-arrow');
        $navArrows.css("border-color" , $navBorderColor);
        $navArrows.hover(
            function(){
                $(this).css({"border-color": $navHoverColor, "background-color": $navHoverColor});
            },
            function(){
                $(this).css({"border-color": $navBorderColor, "background-color": 'transparent'});
            }
        );
    });  
   
    //custom select
    $('.js-form__select').customSelect();
    
    
    //modal
    var $MODAL_OVERLAY = $('.modal__overlay');
	$(document).on('click', '.js-modal_open', function (e) {
		var $self = $(this);
        console.log(this)
		close();
		$($self.attr('href')).show(200);
        $MODAL_OVERLAY.show();
		return false;
	});
	$(document).on('click', function (event) {
		var $target = $(event.target);
		if ($target.hasClass('modal') || $target.closest('.modal').length > 0) {
			
		} else {
            close();
		}
	});
	$(document).on('click', '.modal__close', close);
	function close() {
		$('.modal').hide(200);
        $MODAL_OVERLAY.hide();
	}
    
    //анимированные числа
    $('.numbers__wrap').find('.number__count span').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
	
   //кнопка наверх    
    var offset = 100,
    scroll_top_duration = 700,
    $back_to_top = $('.btn-top'),
    $thedial = $('.dial'),
    $progress_bar = $('.progress-bar');
    $thedial.knob({
        'min' : 0,
        'max' : 100,
        'width' : 65,
        'height' : 65,
        'fgColor' : '#a3e9f5',
        'skin' : 'tron',
        'thickness' : .2,
        'displayInput' : false,
        'displayPreview' : false,
        'readOnly' : true
    });    
    $(window).scroll(function(){(
        $(this).scrollTop() > offset ) ? $progress_bar.addClass('is-visible') : $progress_bar.removeClass('is-visible');
        var s = $(window).scrollTop(),
        d = $(document).height(),
        c = $(window).height();
        scrollPercent = (s / (d-c)) * 100;
        $('.dial').val(scrollPercent).change();
        if (s > 0 ) {
            $('header').addClass('scrolled fade');
        }
        if (s <= 0 ) {
            $('header').removeClass('scrolled fade');
        }
    });
    $('.btn-top').click(function(e){
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });

    // Sliders	
	$(window).on('load resize', function() {
	  if ($(window).width() < 576) {
			$('.event__wrap:not(.slick-initialized)').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				arrows: false,
				autoplay: false,
				swipe: true,
				fade: false,
                infinite: false,
			});
	    $('.articles__wrap:not(.slick-initialized)').slick({
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: true,
	        arrows: false,
	        autoplay: false,
	        // swipe: true,
	        fade: false,
            infinite: false,
	    });
	  } else {
            $(".event__wrap.slick-initialized").slick("unslick");
            $(".articles__wrap.slick-initialized").slick("unslick");
	  }

	  $('.instagram__widget:not(.slick-initialized)').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrows: false,
      autoplay: false,
      fade: false,
      infinite: false,
	  responsive: [
		  {
		    breakpoint: 1280,
		    settings: {
		      slidesToShow: 4,
		      dots: true,
		    }
		  },
		  {
		    breakpoint: 1044,
		    settings: {
		      slidesToShow: 3,
		      dots: true,
		    }
		  },
		  {
		    breakpoint: 728,
		    settings: {
		      slidesToShow: 2,
		      dots: true,
		    }
		  },
		  {
		    breakpoint: 492,
		    settings: {
		      slidesToShow: 1,
		      dots: true,
		    }
		  }
		],
    });

	});

	
    
    /* Скрытие лого 'ZUBAREV PRO' на главной странице */
    $('.about__name').children('img').filter(function() {
        return $(this).attr('alt') == 'Иван Зубарев | Предприниматель'
    }).css('visibility', 'hidden');




    /**************************************************/
    //  $(window).on('load resize', function() {
      //   var list = $('.instagram__widget'),
      //       item = $('.instagram__item'),      
      //       itemHeight = item.height();

      //   if ($(window).width() < 576) {
      //    list.height(itemHeight + 74);
      //   } else {
      //    list.height(itemHeight + 74);
      //   }
      // });
    /**************************************************/


});