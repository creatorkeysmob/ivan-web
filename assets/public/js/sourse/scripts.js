var doc = $(document);

function scripts() {

/****************** Поиск ******************/

    doc.on('click', '.search__btn', function(){
        var input = $(this).parent().parent().find('input[type="search"]');
        var val   = input.val();
        if ( val && val !== '' ) window.location = '/search?q='+val;
        return false;
    });

    doc.on('keyup', 'input[type="search"]', function(event){
        if(event.keyCode == 13){
            var val   = $(this).val();
            if ( val !== '' ) window.location = '/search?q='+val;
        }
        return false;
    });

    doc.on('mouseenter', '.form__input[type="number"]', function(){
        this.focus();
    });
    doc.on('mouseleave', '.form__input[type="number"]', function(){
        this.blur();
        var val = ($(this).val() == '' || $(this).val() < 1) ? 1 : $(this).val() ;
        $(this).val(val);
    });


/*******************************************/

};

document.addEventListener("DOMContentLoaded", scripts);

function ru_to_en(text){
        var transl=new Array();
            transl['А']=transl['A']='a';   transl['а']='a';   transl[' ']='-';
            transl['Б']=transl['B']='b';   transl['б']='b';   transl['_']='-';
            transl['В']=transl['V']='v';   transl['в']='v';   transl['?']='';
            transl['Г']=transl['G']='g';   transl['г']='g';   transl['!']='';
            transl['Д']=transl['D']='d';   transl['д']='d';   transl['.']='';
            transl['Е']=transl['E']='e';   transl['е']='e';   transl[',']='';
            transl['Ё']='yo';              transl['ё']='yo';  transl['\\']='';
            transl['Ж']='zh';              transl['ж']='zh';  transl['/']='';
            transl['З']=transl['Z']='z';   transl['з']='z';   transl[':']='';
            transl['И']=transl['I']='i';   transl['и']='i';   transl[';']='';
            transl['Й']=transl['J']='j';   transl['й']='j';   transl['|']='';
            transl['К']=transl['K']='k';   transl['к']='k';   transl['#']='';
            transl['Л']=transl['L']='l';   transl['л']='l';   transl['"']='';
            transl['М']=transl['M']='m';   transl['м']='m';   transl['\'']='';
            transl['Н']=transl['N']='n';   transl['н']='n';   transl['(']='';
            transl['О']=transl['O']='o';   transl['о']='o';   transl[')']='';
            transl['П']=transl['P']='p';   transl['п']='p';   transl['[']='';
            transl['Р']=transl['R']='r';   transl['р']='r';   transl[']']='';
            transl['С']=transl['S']='s';   transl['с']='s';   transl['{']='';
            transl['Т']=transl['T']='t';   transl['т']='t';   transl['}']='';
            transl['У']=transl['U']='u';   transl['у']='u';   transl['%']='';
            transl['Ф']=transl['F']='f';   transl['ф']='f';   transl['*']='';
            transl['Х']=transl['H']='h';   transl['х']='h';
            transl['Ц']=transl['C']='c';   transl['ц']='c';
            transl['Ч']='ch';              transl['ч']='ch';
            transl['Ш']='sh';              transl['ш']='sh';
            transl['Щ']='shh';             transl['щ']='shh';
            transl['Ъ']='';                transl['ъ']='';
            transl['Ы']=transl['Y']='y';   transl['ы']='y';
            transl['Ь']='';                transl['ь']='';
            transl['Э']='e';               transl['э']='e';
            transl['Ю']='yu';              transl['ю']='yu';
            transl['Я']='ya';              transl['я']='ya';

            var result='';
            for(i=0;i<text.length;i++) {
                if(transl[text[i]]!==undefined) { result+=transl[text[i]]; }
                else { result+=text[i]; }
            }
            return result;
}

function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
