var doc = $(document);
document.addEventListener("DOMContentLoaded", ready);

function ready() {

    /* Выезжающее меню */

    doc.on('click', '.menu-btn', function(){
        $('.sub-menu, .menu-btn').toggleClass('active');
        $('.life_edit_save_all').toggleClass('need_hide_active');
    });

    /* Закрытие по клику вне области (пока только меню) */

    // doc.mouseup(function (e){ // событие клика по веб-документу
	// 	var div = $(".active"); // тут указываем ID элемента
	// 	if (!div.is(e.target) // если клик был не по нашему блоку
	// 	    && div.has(e.target).length === 0) { // и не по его дочерним элементам
	// 		div.removeClass('active'); // скрываем его
    //     //    $('.close').click();
	// 	}
	// });

    /* Красивый чекбокс */

    doc.on('click', '.checkbox_rond', function(){

        if( $(this).hasClass('actived') ){
            $(this).removeClass('actived').attr('title', 'Включить');
            $('.switch', $(this)).removeClass('green');
            var on = 0;
            $('.wa_admin').removeClass('life_edit');
            window.location.reload(true);
        }else{
            $(this).addClass('actived').attr('title', 'Выключить');
            $('.switch', $(this)).addClass('green');
            var on = 1;
            $('.wa_admin').addClass('life_edit');

            $('.life_edit').each(function(){
                var id = $(this).attr('id');
                console.log(id, typeof id);
                if ( id ) {
                    var editor = new Jodit('#'+id, {
                        "enter": "BR",
                        preset: 'inline'
                    });
                }
            });
        }

        $.ajax({
            url      : "/api/public_forms.switchLifeEdit",
            type     : "POST",
            dataType : "JSON",
            data     : {
                'on'    : on
            },
            success  : function (response) {
            //    window.location = window.location;
            }
        });
    });

    /* Выход из админки */

    doc.on('click', '.logout_link span', function(){
        $.ajax({
            url      : "/admin/logout",
            type     : "POST",
            dataType : "JSON",
            data     : {},
            success  : function (response) {
            }
        });
        $('.sub-menu, .menu-btn').remove();
        $('.wa_admin').removeClass('wa_admin life_edit');
        setTimeout(function(){
             window.location.reload(true);
        }, 1000);

    });

    /* Хоткеи: ctrl+s - сохранить */

    var isCtrl = false;
    doc.keyup(function (e) {
        if (e.which === 17)
            isCtrl = false;
    }).keydown(function (e) {
        if (e.which === 17)
            isCtrl = true;
        if (e.which === 83 && isCtrl === true) {
            if(e.preventDefault) e.preventDefault();
            e.returnValue = false;
            //$('.save').click();
            $('.save').each(function(){
                $(this).click();
            });

            $('.life_edit_save_all, .save_all_button').removeClass('active');
            return false;
        }
    });

    /* Включение визуального редактора */

    set_life_adit();

    /* При фокусе показывает кнопочку для сохранения */

    doc.on('focus', '.life_edit', function(){
        var buttons = "<span class='save'></span>";
        if ( $(this).find('.save').length == 0 ) {
            $(this).append(buttons);
        }

        $('.life_edit_save_all, .save_all_button').addClass('active');
    });
    // doc.on('blur', '.life_edit', function(){
    //     $('.save').remove();
    // })

    /* Сохранение редактора */

    doc.on('click', '.life_edit_save_all, .save_all_button', function(){
        $('.save').each(function(){
            $(this).click();
        });
        $('.life_edit_save_all, .save_all_button').removeClass('active');
    });

    doc.on('click', '.save', function(){
        var life_edit = $(this).parent('.life_edit'),
            id          = life_edit.data('id'),
            table       = life_edit.data('table'),
            field       = life_edit.data('field');
            text        = life_edit.find('.jodit_wysiwyg').html().trim();
        $(this).remove();
        $.ajax({
            url      : "/api/public_forms.saveLifeEdit",
            type     : "POST",
            dataType : "JSON",
            data     : {
                'id'    : id,
                'table' : table,
                'field' : field,
                'text'  : text
             },
            success  : function (response) {
                if ( $('.save').length == 0 ) {
                    $('.life_edit_save_all, .save_all_button').removeClass('active');
                }
            }
        });
    });

}

function set_life_adit(){
    $('.life_edit').each(function(){
        var id = $(this).attr('id');
        if ( id ) {
            var editor = new Jodit('#'+id, {
                "enter": "BR",
                "preset": "inline",
                "popup": {
                    "selection": false
                },
            });
        }
    });
}
