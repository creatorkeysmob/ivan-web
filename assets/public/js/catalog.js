'use strict';

function ready(){

    document.getElementById("catalog") && new Vue({
            delimiters: ['[[', ']]'],
            el: '#catalog',
            data: {
                catalog           : [],
                search            : '',
                count             : 0,
                totalCount        : 0,
                selectedIndustry  : [],
                selectedFunctions : [],
                selectedProduct   : [],
                perPage           : 5,
                pagination        : {},
                industryFilter    : {},
                functionsFilter   : {},
                productFilter     : {},
                adaptive_filter   : false
            },
            mounted() {
                let self = this;

                fetch('/api/public_forms.Catalog')
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(item) {
                        self.catalog = item;
                        self.count = self.totalCount = item.length
                        self.setPage(1);
                        self.industryFilter  = self.getFilterByParam('industry');
                        self.functionsFilter = self.getFilterByParam('functions');
                        self.productFilter   = self.getFilterByParam('product');
                    });

            },
            computed: {
                filtrJs(){
                    let self = this;
                    return self.getFiltrJs({ industry: {}, functions: {}, product: {} })
                },
                collection(){
                    let self = this;
                    return self.paginate(self.catalog.filter((item) => {
                        return item.show
                    }));
                },
                computedCount(){
                    let self = this;
                    if ( self.count == self.totalCount ) {
                        return `<strong>Показано: </strong><span>Всё</span>`;
                    } else {
                        let din_count = self.count.toFixed(0)
                        let letter = declOfNum(din_count,['результат','результатa','результатов']);
                        return `<strong>Найдено: </strong><span> ${din_count} ${letter} </span>`;
                    }
                }
            },
            methods: {
                setPage(p){
                    let self = this;
                    setTimeout(()=>{
                        let count = self.catalog.filter((item) => {
                            return item.show
                        }).length;
                        self.pagination = self.paginator(count, p);
                    }, 1);

                    if ( document.getElementsByClassName("life_edit__menu-btn").length > 0 ) {
                        setTimeout(set_life_adit, 1)
                    }
                },
                paginate(data){
                    let self    = this;
                    return _.slice(data, self.pagination.startIndex, self.pagination.endIndex + 1);
                },
                paginator(totalItems, currentPage){
                    let self       = this;
                    let perpage    = (self.perPage*1 < 1) ? 1 : self.perPage;
                    var startIndex = (currentPage - 1) * perpage,
                        endIndex   = Math.min(startIndex + perpage - 1, totalItems - 1),
                        pages      = _.range(1, Math.ceil(totalItems / perpage) + 1),
                        page_count = pages.length
                    return {
                        currentPage,
                        startIndex,
                        endIndex,
                        pages,
                        page_count
                    };
                },
                getFilterByParam( parametr, only_showed = false ){
                    let self     = this;
                    let filtr_js = {};
                    self.catalog.forEach((item) => {
                        if ( only_showed ) {
                            if ( item.show ) {
                                item[parametr].split(',')
                                    .forEach((param) => {
                                        if (param) {
                                            param = param.trim().toLowerCase();
                                            let param_en = ru_to_en(param);
                                            param = param[0].toUpperCase() + param.slice(1);
                                            if (filtr_js[param_en]) {
                                                if (filtr_js[param_en].last_item == item.head+'_'+item.id) {
                                                    var N = filtr_js[param_en].count;
                                                } else {
                                                    var N = filtr_js[param_en].count + 1;
                                                }
                                            } else {
                                                var N = 1;
                                            }
                                            filtr_js[param_en] = {
                                                'title'   : param,
                                                'count'   : N,
                                                last_item : item.head+'_'+item.id
                                            };
                                        }
                                    });
                            }
                        } else {
                            item[parametr].split(',')
                                .forEach((param) => {
                                    if (param) {
                                        param = param.trim().toLowerCase();
                                        let param_en = ru_to_en(param);
                                        param = param[0].toUpperCase() + param.slice(1);
                                        if (filtr_js[param_en]) {
                                            if (filtr_js[param_en].last_item == item.head+'_'+item.id) {
                                                var N = filtr_js[param_en].count;
                                            } else {
                                                var N = filtr_js[param_en].count + 1;
                                            }
                                        } else {
                                            var N = 1;
                                        }
                                        filtr_js[param_en] = {
                                            'title'   : param,
                                            'count'   : N,
                                            last_item : item.head+'_'+item.id
                                        };
                                    }
                                });
                        }

                    });

                    if ( Object.keys(filtr_js).length > 0 ) {
                        filtr_js = Object.values(filtr_js).sort(function(a, b){
                            if (a.title > b.title) return 1;
                            if (a.title < b.title) return -1;
                        });
                    }

                    return filtr_js;
                },

                filterStage( item, parametrs_arr ){
                    if ( parametrs_arr.length > 0 ) {
                        let filtr = 0;
                        for ( let i of parametrs_arr ){
                            for ( let j of item.split(',') ) {
                                filtr == (j.trim().toLowerCase() == i.trim().toLowerCase()) ? filtr : filtr++;
                            }
                        }
                        let show = filtr > 0;
                        return show;

                    }
                    return true;
                },
                totalFilter( type ){
                    let self      = this;
                    let search    = self.search.toLowerCase();
                    let industry  = self.selectedIndustry;
                    let functions = self.selectedFunctions;
                    let product   = self.selectedProduct;
                    let catalog   = self.catalog;
                    let catcount  = 0;

                    catalog.map((item) => {

                        // 1 stage
                        let show = item.head.toLowerCase().indexOf(search.toLowerCase()) > -1;
                        if ( !show ){
                            item.show = show;
                            return item;
                        }

                        // 2 stage
                        show = self.filterStage(item.industry, industry);
                        if ( !show ){
                            item.show = show;
                            return item;
                        }

                        // 3 stage
                        show = self.filterStage(item.functions, functions);
                        if ( !show ){
                            item.show = show;
                            return item;
                        }

                        // 4 stage
                        show = self.filterStage(item.product, product);
                        if ( !show ){
                            item.show = show;
                            return item;
                        }

                        catcount++;
                        item.show = show;
                        return item;
                    });


                    let filter_params = {
                        'industry'  : { 'length' : industry.length,  'module' : 'industryFilter'  },
                        'functions' : { 'length' : functions.length, 'module' : 'functionsFilter' },
                        'product'   : { 'length' : product.length,   'module' : 'productFilter'   }
                    };

                    // let all_filter = !!search;
                    // //let all_filter = self.adaptive_filter;
                    // if ( type == 'text' && Object.keys( filter_params ).length > 0 ) {
                    //     for ( let filter in filter_params ) {
                    //         self[filter_params[filter]]  = self.getFilterByParam(filter, all_filter);
                    //     }
                    // }

                    /***************************************/
                    let adapt_filter = self.adaptive_filter;

                    if ( adapt_filter ) {

                            // добавить сюда же условие из строкового поиска

                        if ( industry.length == 0 && functions.length == 0 && product.length == 0 ){
                            self.industryFilter  = self.getFilterByParam('industry');
                            self.functionsFilter = self.getFilterByParam('functions');
                            self.productFilter   = self.getFilterByParam('product');
                        }
                        if ( industry.length != 0 && functions.length == 0 && product.length == 0 ){
                            self.industryFilter  = self.getFilterByParam('industry',  false);
                            self.functionsFilter = self.getFilterByParam('functions', true);
                            self.productFilter   = self.getFilterByParam('product',   true);
                        }
                        if ( industry.length == 0 && functions.length != 0 && product.length == 0 ){
                            self.industryFilter  = self.getFilterByParam('industry',  true);
                            self.functionsFilter = self.getFilterByParam('functions', false);
                            self.productFilter   = self.getFilterByParam('product',   true);
                        }
                        if ( industry.length == 0 && functions.length == 0 && product.length != 0 ){
                            self.functionsFilter = self.getFilterByParam('functions', true);
                            self.industryFilter  = self.getFilterByParam('industry',  true);
                            self.productFilter   = self.getFilterByParam('product',   false);
                        }
                        // if ( search.length > 0  ){
                        //     for ( let filter in filter_params ) {
                        //         self[filter_params[filter].module]  = self.getFilterByParam(filter, true);
                        //     }
                        // }

                        for (let filter_step in filter_params) {
                            for (let filter in filter_params) {
                                if (filter != filter_step) {
                                    if (filter_params[filter_step].length > 0) {
                                        self[filter_params[filter].module] = self.getFilterByParam(filter, true);
                                    }
                                }
                            }
                        }

                    } else {
                        self.industryFilter  = self.getFilterByParam('industry');
                        self.functionsFilter = self.getFilterByParam('functions');
                        self.productFilter   = self.getFilterByParam('product');
                    }


                    /***************************************/


                    self.setPage(1);
                    TweenLite.to(this.$data, .3, { count: catcount });
                    //self.count = catcount;

                },

            }

        });


}

document.addEventListener("DOMContentLoaded", ready);
