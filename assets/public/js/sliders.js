$(function () {   
   //main slider
    $('.main-slider__item-wrap').slick({
        dots: false,
        infinite: true,
        swipe: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: $('.slider__nav'),
        prevArrow: '<button type="button" class="slick-prev main-slider-prev main-slider__arrow"></button>',
        nextArrow: '<button type="button" class="slick-next main-slider-next main-slider__arrow"></button>'
    });
    
    //photogallery slider
	$('.photogallery-slider__item__wrap').slick({
		dots: true,
		infinite: true,
		swipe: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button type="button" class="slick-prev gallery-slider-prev gallery-slider__arrow"></button>',
		nextArrow: '<button type="button" class="slick-next gallery-slider-next gallery-slider__arrow"></button>'
	});

    
    //video slider
    $('.video-slider__item__wrap').slick({
        asNavFor: '.video-slider__nav-item__wrap',
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
		swipe: true
    });
    $('.video-slider__nav-item__wrap').slick({
        asNavFor: '.video-slider__item__wrap',
        slidesToShow: 3,
        slidesToScroll: 1,
        focusOnSelect: true,
        dots: false,
        swipe: true,
        prevArrow: '<button type="button" class="slick-prev gallery-slider-prev gallery-slider__arrow"></button>',
        nextArrow: '<button type="button" class="slick-next gallery-slider-next gallery-slider__arrow"></button>'
    });
});