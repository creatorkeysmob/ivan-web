var doc = $(document);

doc.ready(function () {

    doc.mouseup(function (e){ // событие клика по веб-документу
		var div = $(".active"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			div.removeClass('active'); // скрываем его
		}
	});

    get_butt();

    var isCtrl = false;
    doc.keyup(function (e) {
        if (e.which === 17)
            isCtrl = false;
    }).keydown(function (e) {
        if (e.which === 17)
            isCtrl = true;
        if (e.which === 83 && isCtrl === true) {
        // if (e.which === 18 && isCtrl === true) {
            // save();

            $('#form').submit();
            return false;
        }
    });

    doc.on('click', '.menu-btn', function(){
        $('.sub-menu, .menu-btn').toggleClass('active');
    });

    $('#up').click(function () {
        $("body").animate({"scrollTop": 0}, "slow");
    });
    $('#down').click(function () {
        var h = $(document).height();
        $("body").animate({"scrollTop": h}, "slow");
    });

    $('.former-category-title').each(function () {
        var title = $(this).text();
        var text = '<div class="punkt">' + title + '</div>';
        $('#float-menu').append(text);
    });

    doc.on('click', '.punkt', function () {
        var title = $(this).text();
        var moove = $('.former-category-title:contains("' + title + '")').offset().top;
        $("body, html").animate({"scrollTop": moove}, 500);
        setTimeout(function () {
            stop();
            $(this).addClass('active');
        }, 500);
    });


    $(document).on('click', '.checkboxes_label', function(){
        var clas = $(this).data('attr');
        $('.'+clas).click();
    });

    doc.on('click', '.activate_text, .checkbox_rond, .activate_date', function(){
        var name = $(this).data('name');
        if( $(this).hasClass('actived') ){
            $('.'+name).attr('disabled','disabled').addClass('disabled');
            $('#'+name).removeAttr('checked');
            $(this).removeClass('actived').attr('title', 'Активировать');
            $('.switch', $(this)).removeClass('green');
        }else{
            $('.'+name).removeAttr('disabled').removeClass('disabled');
            $('#'+name).attr('checked','checked');
            $(this).addClass('actived').attr('title', 'Снять');
            $('.switch', $(this)).addClass('green');
        }
    });


    $('.on').each(function(){
        var clas = $(this).data("class");
        CKEDITOR.replace( clas , { toolbar : 'Default', enterMode : CKEDITOR.ENTER_BR });
        $(this).css('display', 'none');
        $('#off').css('display', '');
    });

    doc.on('click', '.on', function(){
        var clas = $(this).data("class");

        CKEDITOR.replace( clas , { toolbar : 'Default', enterMode : CKEDITOR.ENTER_BR });
        $(this).css('display', 'none');
        $('#off').css('display', '');

        $('#'+clas).attr('id', clas+'_ck');
    });

    /************* Функционал Link *************/

    doc.on('click', '.link', function(){
        var name = $(this).data('who');
        if ( name == '' ) name = 'head';
        var text = $('input[name="'+name+'"]').val();
        var result = ru_to_en(text);
        $(this).prev('.en_text').val(result);
    });

    doc.on('keydown', '.en_text', function(event) {

          // Разрешаем: backspace, delete, tab и escape
          if ( event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 ||
               // Разрешаем: Ctrl+A
              (event.keyCode === 65 && event.ctrlKey === true) ||
               // Разрешаем: home, end, влево, вправо
              (event.keyCode >= 35 && event.keyCode <= 39)) {
                   // Ничего не делаем
                   return;
          }
          else {
              var en_texts = ['1','2','3','4','5','6','7','8','9','0',
                                'q','w','e','r','t','y','u','i','o','p',
                                'a','s','d','f','g','h','j','k','l',
                                'z','x','c','v','b','n','m'];
              var key = event.key.toLowerCase();

              // Убеждаемся, что это цифра, и останавливаем событие keypress
              if ( $.inArray( key, en_texts ) < 0 ) {
                  event.preventDefault();
                  $(this).css('border-color', '#dd0000');
              }
          }
      });

      doc.on('keydown', '.en_text', function(event) {
          $(this).css('border-color', '#ccc');
      })

    // $('.en_text').keydown().keyup(function(){
    //
    //   });

    /*******************************************/

    /**************** Валидация ****************/

    doc.on('blur', '.required', function(){
        var val  = $(this).val();
        var func = validateEmpty(val);
        if ( $(this).hasClass('mail') ) {
            func = validateEmail(val);
        }
        if ( $(this).hasClass('phone') ) {
            func = validatePhone(val);
        }
        if ( !func ) {
            $(this).addClass('error');
        } else {
            $(this).removeClass('error');
        }
    });

    /*******************************************/

    /***************** Фидбеки *****************/

    doc.on('click', '.send_wrineus', function(){
        var msg   = '<table><tr><td><b>Сайт</b></td><td> box.web-ar.ru</td></tr>';

        var title = 'Заявка из админки сайта Box';
        var input = $('input:not([type=submit], [type=button], [type=hidden]), textarea');
        input.each(
            function(){
                $(this).blur();
                var val   = $(this).val();
                var title = $(this).parent().parent().find('.span2').text();
                title = title.replace(/[^A-Za-zА-Яа-яЁё]/g, "");
                msg += '<tr><td><b>'+title+'</b>: </td><td> '+val+' </td></tr>';
            }
        );
        msg += '</table>';

        if ( !input.hasClass('error') ) {
            $('.send_wrineus').prop('disabled',true);
            $.ajax({
                url: "/api/public_forms.sendForm",
                type: "POST",
                dataType: "JSON",
                data: {
                    text  : msg,
                    title : title,
                    table : 'not_save'
                },
                success: function(response){
                    input.val('');
                    $('.msg_send').addClass('show');
                    $('.send_wrineus').prop('disabled',false);
                    setTimeout( function(){$('.msg_send').removeClass('show');}, 3000 );
                }
            });
        } else {
            console.log('нет');
        }

        return false;
    });

    /*******************************************/

    /***** Удаление картинки из фотоальбома ****/

    doc.on('click', '.pic_delete', function(){
        var who   = $(this).data('del');
        var title = $(this).data('title');
        $('.fon_del_pic_'+who).toggleClass('showed');
        if( $('.fon_del_pic_'+who).hasClass('showed') ){
            $('.del_check_'+who).val(title);
        } else {
            $('.del_check_'+who).val('');
        }
    });

    /*******************************************/

    /****** Добавление элемента в Groups *******/

    doc.delegate( ".former-add", "click", function() {
        var name   = $(this).data('name');
        var $table = $(this).closest('table.former-'+name+'-table');
        var size   = $table.find('tr.former-'+name+'-empty').size();
        var L      = $table.find('tr.former-'+name+'-row').size() + 1;
        if(size == 1){
            $table.find('tr.former-'+name+'-empty').remove();
        }
        var params = {'L':L}

        var $parameters_row_template = $( "#template-upload-"+name).tmpl(params);
        $table.children('tbody').prepend($parameters_row_template);

            $('#imulated_'+name+'_new_element').removeAttr('id').attr('id', 'imulated_'+name+'_'+L);
            $('#file_imulated_'+name+'_new_element').removeAttr('id').attr( {'id':'file_imulated_'+name+'_'+L, 'data-id':'imulated_'+name+'_'+L} );
            $('.file_button_'+name+'_new_element').removeClass('file_button_'+name+'_new_element').attr('data-id', 'file_imulated_'+name+'_'+L);
            $('#'+name+'_text_new_element').removeClass('file_button_'+name+'_new_element').attr('data-id', 'file_imulated_'+name+'_'+L);

            $('.on').attr('data-id', name+'_text_'+L);
            $("select.select2").select2();

        return false;
    });

    /******* Удаление элемента в Groups *******/

    doc.delegate( ".former-remove", "click", function() {
        var name              = $(this).data('name');
        var counter           = $(this).data('counter');
        var $table            = $(this).closest('table.former-'+name+'-table');
        var $parameters_empty = '<tr class="former-'+name+'-empty"><td colspan="2">Список пуст</td></tr>';

        $(this).closest('tr.former-'+name+'-row').remove();
        var counter = $table.find('tr.former-'+name+'-row td.counter')
        var N       = $table.find('tr.former-'+name+'-row').size()
        counter.each(function(){
            $(this).text(N + ' ' + counter);
            N = N-1;
        });
        if($table.find('tr.former-'+name+'-row').size() == 0){
            $table.children('tbody').append($parameters_empty);
        }
     //   $('.for-'+name+'-del-pic-arr').append('<input type="hidden" name="delited_pic[]" value="" >');
        return false;
    });

    /*******************************************/

    /*********** Drag-n-Drop в groups **********/

    if ( $('*').hasClass('groups_sortable_tbody') ){
        $( ".groups_sortable_tbody" ).sortable({
            start: function(){
                var height = $('.groups_sortable_tbody tr').height();
                $('.ui-state-highlight').height(height);
            },
            axis: "y",
            handle: ".move_groups",
            placeholder: "ui-state-highlight",
        }).disableSelection();
    }

    /*******************************************/

    /******* Загрузка картинок в groups ********/

    doc.on('change', '.file_input_hidden', function(){
        var id  = $(this).data('id');
        var val = this.value.replace('C:\\fakepath\\', '');
        $('#'+id).val(val);
    });
    doc.on('click', '.file_input_button', function(){
        var id = $(this).data('id');
        $('#'+id).click();
    });

    /*******************************************/

    /************* Select в groups *************/

    doc.on('change', '.groups_select', function(){
        var id  = $(this).data('id');
        var val = $('option:selected', this).text();
        $('#'+id).val(val);

        var val2 = $(this).val();
        $(this).parent().parent().find('.bonus_text').addClass('hidden');
        $(this).parent().parent().find('.b_'+val2).removeClass('hidden');

    });
    doc.on('click', '.select_input_textbox', function(){
        var id = $(this).data('id');
        // alert(id);
        $('#'+id).click();
    });

    doc.on('click', '.for_select_input_textbox', function(){
        $(this).addClass('hidden');
    });

    /*******************************************/

    /********** MiltiSelect в groups ***********/

    doc.on('change', '.groups_multiselect', function(){
        var id    = $(this).data('id');
            input = $(this).siblings('#'+$(this).data('id')),
            list  = '';
        $('option:selected', this).each(function(){
            var val = $(this).val();
            list  = (list == '') ? list : list+',';
            list += val;
        });
        input.val(list);
    });

    $('.groups_multiselect_text').each(function(){
        var id  = $(this).prop('id'),
            val = $(this).val(),
            arr = val.split(',');
        $.each(arr, function(index, value){
            $(".groups_multiselect[data-id="+id+"] option[value='"+value+"']").attr("selected","selected");
        });
    });

    /*******************************************/

    /************ Чекбоксы в groups  ***********/

    doc.on('click', '.groups_checkbox', function(){
        var input = $(this).siblings('#'+$(this).data('id')),
            val   = input.val()*1;
            input.val(!val*1);
    })

    $('.groups_checkbox_1').addClass('actived').find('.switch').addClass('green');

    /*******************************************/

    /******** Группа Чекбоксов в groups  *******/

    doc.on('click', '.groups_checkboxgroup', function(){
        var id    = $(this).data('id'),
            input = $(this).siblings('#'+id),
            chkbc = $('.groups_checkboxgroup[data-id='+id+']'),
            list  = '';
        chkbc.each(function(i){
            var act = 0;
            if ( $(this).hasClass('actived') ) {
                act = $(this).data('key');
            }
            list  = (list == '') ? list : list+',';
            list += act;
        });
        input.val(list);
    })

    $('.groups_checkboxgroup_text').each(function(){
        var id  = $(this).prop('id'),
            val = $(this).val(),
            arr = val.split(',');
        $.each(arr, function(index, value){
            if ( value != 0 ) {
                $('.groups_checkboxgroup_'+value+'[data-id='+id+']').addClass('actived').find('.switch').addClass('green');
            } else {
                $('.groups_checkboxgroup_'+value+'[data-id='+id+']').removeClass('actived').find('.switch').removeClass('green');
            }
        });
    });

    /*******************************************/

    /******** Только цифры в поле Integer ******/

      $('.integer').keydown(function(event) {
            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 ||
                 // Разрешаем: Ctrl+A
                (event.keyCode === 65 && event.ctrlKey === true) ||
                 // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // Ничего не делаем
                     return;
            }
            else {
                // Убеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                     $(this).css('border-color', '#dd0000');
                }
            }
        }).keyup(function(){
            $(this).css('border-color', '#ccc');
        });

    /*******************************************/

    /***** для работы селекта с подселектом *****/

        $(document).on('change', '.select2lvl', function(){
            var id = $(this).val();
            var el = $(this).data('el');
            if ( id == '-') {
                $('.input_'+el+'_1lvl').val('');
                $('.sub_'+el).addClass('hidden');
            } else {
                $('.sub_select').val(null).trigger('change');
                $('.input_'+el+'_1lvl').val(id);
                $('.sub_'+el).addClass('hidden');
                $('.el_'+id).removeClass('hidden');
            }

        });
        $(document).on('change', '.sub_select', function(){
            var id  = $(this).val();
            var el  = $(this).data('el');
            var val = $('.select_'+el+' option:selected').val();
            $('.input_'+el+'_1lvl').val(val+','+id);
        });

    /*********************************************/

    /******* Отображение страниц в меню ********/

        doc.on( 'click', '.toggle_menushow', function(){
            var id     = $(this).data('id'),
                show   = $(this).data('show'),
                params = {};
                params[id] = show;
            jQuery.ajax({
                url: App.url+'/menushow',
                cache: false,
                dataType:'text',
                data: { 'id': id },
                type: 'post',
                success: function(response){ }
            });
            var status = show == 0 ? '<i title="Отображать в меню" class="icon-remove"></i>' : '<i title="Не отображать в меню" class="icon-ok"></i>';
            $(".toggle_menushow[data-id="+id+"]").html(status).data('show', !show);
        });

    /*******************************************/

    /***************** Удаление ****************/

    // doc.on('change', '.del_checkbox', function(){
    //     alert(1);
    // });


    doc.on('click', '.page_delete', function(){
        if (!confirm('Вы действительно хотите удалить элемент?')) return;
        var id = 'item_' + $(this).data('id');
        var params = {};
            params[id] = 'true';
        jQuery.ajax({
            url: App.url+'/drop',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);
                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {
                    for( var item_id in response.data.items ) {
                        var tr = $('#row_item_'+item_id);

                        if( tr.size() !== 0 ) {
                            var size = tr.find('td').size();
                            var td = $("<td/>",{
                                colspan: size
                            }).text('Позиция удалена');
                            tr.empty().append(td);
                        }
                    }
                    console.log('Helper: delete items [success]');
                } else {
                    console.log('Helper: delete items [failed]');
                }
            },

            error:function () {
                console.log('Helper: delete items [failed]');
            }
        });

        $(this).parent().parent().hide(300);
    })

    doc.on('click', '.delete', function(){
        if (!confirm('Вы действительно хотите удалить элемент?')) return;
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "JSON",
            data: { },
            success: function(response){}
        });
        $(this).parent().parent().hide(300);
    });

    doc.on('click', '.hierarchy_delete', function(){
        if (confirm('Удалить раздел вместе со всеми вложениями?')) {
            var url = $(this).attr('href');
            var id  = $(this).data('id');
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: { 'id':id },
                success: function(response){
                    $.each(response, function(index, value) {
                        $('#row_item_'+value+', #row_item_'+id).hide(300);
                    });
                }
            });
        }
    });

    doc.on('click', '.del_by_creator', function(){
        if (!confirm('Вы действительно хотите удалить элемент?')) return;
        var name   = $(this).data('alias'),
            lvl    = $(this).data('lvl'),
            img    = $(this).data('img'),
            search = $(this).data('search'),
            main   = $(this).data('main');
        $.ajax({
            url      : App.url,
            type     : "POST",
            dataType : "JSON",
            data     : {
                'name'   : name,
                'lvl'    : lvl,
                'main'   : main,
                'img'    : img,
                'search' : search
             },
            success  : function(response){}
        });
    //    location.reload(true);
        $(this).parent().parent().hide(300);
    })

    /*******************************************/

    /************** Редактирование *************/

        $('.can_click').each(function(){
            var id = $(this).attr('id');
            if ( id ) {
                var editor = new Jodit('#'+id, {
                    "enter": "BR",
                    "preset": 'inline',
                    "popup": {
                        //"selection": ["bold", "underline", "italic"]
                        "selection": false
                    },
                });
                // editor.popup = {
                //     "selection": ["bold", "underline", "italic"]
                // };
            }
        });

        doc.on('focus', '.can_click', function(){
            var buttons = "<span class='btn btn-icon save'><i class='icon-ok'></i></span>";
            $(this).append(buttons);
            // $('.life_edit_save_all').addClass('active');
        });

        doc.on('click', '.save', function(){
            var life_edit = $(this).parent('.can_click'),
                id        = life_edit.data('id'),
                table     = life_edit.data('table'),
                field     = life_edit.data('field'),
                text      = life_edit.find('.jodit_wysiwyg').html().trim();
            $(this).siblings('.save').remove();
            $(this).remove();
            $.ajax({
                url      : App.url+'/fastedit',
                type     : "POST",
                dataType : "JSON",
                data     : {
                    'id'    : id,
                    'table' : table,
                    'field' : field,
                    'text'  : text
                 },
                success  : function (response) {
                    // if ( $('.save').length == 0 ) {
                    //     $('.life_edit_save_all, .save_all_button').removeClass('active');
                    // }
                }
            });
        });

    /*******************************************/

    /**************** Сортировка ***************/

        if ( $('*').hasClass('sortable') ){

			var fixHelper = function(e, ui) {
    			ui.children().each(function() {
        			$(this).width($(this).width());
    			});
    			return ui;
			};

            $( ".sortable tbody" ).sortable({
				helper: fixHelper,
                start: function( event, ui ){
                    var id = ui.item.context.id;
                        height = $('#'+id).height();
                    $('.ui-state-highlight').height(height);
                },
                update: function(){
                    var params = {};
                        n = 1;
                    $('.sortable tbody tr').each(function(){
                        var id  = 'item_' + $(this).data('id');
                        var arr = {};
                            arr[id] = n;
                            n++;
                        Object.assign(params, arr);
                    });

                    params['view'] = App.view;
                    jQuery.ajax({
                        url: App.url+'/sort',
                        cache: false,
                        dataType:'text',
                        data: params,
                        type: 'post',
                        error:function () {
                            console.log('Helper: priority update [failed]');
                        }
                    });

                },
                axis: "y",
                handle: ".move_groups",
                placeholder: "ui-state-highlight",
            }).disableSelection();
        }

    /*******************************************/

    /************* Компактный режим ************/

    doc.on('click', '.activate_compact_menu', function(){
        //$('.compact_menu').removeClass('showed');
        var act = +$(this).hasClass('actived');
        $('.uncompact_menu_td, .compact_menu_td').toggleClass('hidden');
        var date        = new Date(0);
        document.cookie = "compact_menu=; path=/; expires=" + date.toUTCString(); // удалим старую куку для php
        document.cookie = "compact_menu="+act+';path=/'; // сохраним в куку для php
    });

    doc.on('click', '.get_compact_menu', function(){
        $(this).toggleClass('actived').siblings('.compact_menu').toggleClass('showed');
    });

    /*******************************************/

    /*************** Карта сайта ***************/

    doc.on('click', '.sitemapshow', function(){
        var id     = $(this).data('id'),
            table  = $(this).data('table'),
            status = $(this).find('.sitemap_status').hasClass('icon-eye-open'),
            item   = $('.sitemapshow[data-id="'+id+'"][data-table="'+table+'"]');
        //     parent = $(this).parent().prev().find('.sitemap_status').hasClass('icon-eye-open');
        // if ( !parent ) return false;
        if ( status ){
            item
                .addClass('gray')
                    .find('.sitemap_status')
                    .removeClass('icon-eye-open')
                    .addClass('icon-eye-close')
                        .parent()
                        .next('ul')
                        .addClass('gray');
        } else {
            item
                .removeClass('gray')
                    .find('.sitemap_status')
                    .removeClass('icon-eye-close')
                    .addClass('icon-eye-open')
                        .parent()
                        .next('ul')
                        .removeClass('gray')
        }
        $.ajax({
            url: App.url,
            type: "POST",
            dataType: "JSON",
            data: {
                id     : id,
                table  : table,
                status : !status*1
            },
            success: function (response) {

            }
        });
    });

    /*******************************************/

    /*************** Imgcheckbox ***************/

    doc.on('click', '.imgcheckbox_view', function(){
        var num = $(this).data('num');
        $('.imgcheckbox_fullimg').hide();
        $('.imgcheckbox_fullimg_'+num).show();
    })
    doc.on('click', '.ok_but', function(){
        $('.imgcheckbox_fullimg').hide();
    })

    $('.prev').draggable({
        start: function() {
            $('.imgareaselect-outer').hide();
            $('.imgareaselect-border4').parent().hide();
        },
        stop: function() {
            $(window).trigger('resize', true);
            $('.imgareaselect-outer').show();
            $('.imgareaselect-border4').parent().show();
            var top  = $(this).css('top');
            var left = $(this).css('left');
        }
    });

    /*******************************************/

    /**************** Календарь ****************/
        $( ".form_calendar" ).datepicker({
              currentText: 'Сейчас',
              closeText: 'Закрыть',
              prevText: '<Пред',
              nextText: 'След>',
              monthNames: ['Январь','Февраль','Март','Апрель','Май', 'Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
              monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
              dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
              dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
              dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
              weekHeader: 'Не',
              dateFormat: 'dd.mm.yy',
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: false,
              yearSuffix: ''
        });


        $( ".form_calendar_time" ).datetimepicker({
            showOn: "button",
            buttonImage: "/assets/admin/images/calendar.svg",
            buttonImageOnly: true,
            buttonText: "Select date",
              hour: 9,
              minute: 00,
              currentText: 'Сейчас',
              closeText: 'Закрыть',
              prevText: '<Пред',
              nextText: 'След>',
              monthNames: ['Январь','Февраль','Март','Апрель','Май', 'Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
              monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
              dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
              dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
              dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
              weekHeader: 'Не',
              dateFormat: 'dd.mm.yy',
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: false,
              yearSuffix: '',

              timeOnlyTitle: 'Выберите время',
              timeText: 'Время',
              hourText: 'Часы',
              minuteText: 'Минуты',
              secondText: 'Секунды',
              millisecText: 'Миллисекунды',
              timezoneText: 'Часовой пояс',
              currentText: 'Сейчас',
              closeText: 'Закрыть',
              timeFormat: 'HH:mm',
              amNames: ['AM', 'A'],
              pmNames: ['PM', 'P'],
              isRTL: false
        });


        $( ".form_time" ).timepicker({
              hour: 9,
              minute: 00,
              timeOnlyTitle: 'Выберите время',
              timeText: 'Время',
              hourText: 'Часы',
              minuteText: 'Минуты',
              secondText: 'Секунды',
              millisecText: 'Миллисекунды',
              timezoneText: 'Часовой пояс',
              currentText: 'Сейчас',
              closeText: 'Закрыть',
              timeFormat: 'HH:mm',
              amNames: ['AM', 'A'],
              pmNames: ['PM', 'P'],
              isRTL: false
        });

    /*******************************************/



});


function save(){
    $.ajax({
        url: App.url,
        type: "POST",
        dataType: "JSON",
        data: $('#form').serializeArray(),
        success: function (response) {

        }
    });
    return false;
}

function back() {
    var href = $('#breadcrumb_link').val();
    window.location = href;
}

$(window).scroll(function () {
    var scrl = get_butt();
    get_active(scrl);
});

function get_butt() {
    var h = $(document).height();
    var wh = $(window).height();
    var w = h - wh;
    var scrl = $(window).scrollTop();
    if (scrl >= 180) {
        //   $('.butt').fadeIn(300);
        $('.butt').addClass('showed');
    } else {
        //    $('.butt').fadeOut(0).stop();
        $('.butt').removeClass('showed');
    }
    return scrl;
}

function get_active(scrl) {
    $('.punkt').removeClass('active');
    var dh = $(document).height();
    var wh = $(window).height();
    $('.former-category-title').each(function () {
        var div_h = $(this).parent().height();
        var H = $(this).offset().top;
        var text = $(this).text();
        if ((scrl > H - 40 && scrl < H + div_h) || (H > scrl && (dh - wh) === scrl)) {
            $('.punkt').removeClass('active');
            $('.punkt:contains("' + text + '")').addClass('active');
        }
    });
}

function ru_to_en(text){
        var transl=new Array();
            transl['А']=transl['A']='a';   transl['а']='a';   transl[' ']='-';
            transl['Б']=transl['B']='b';   transl['б']='b';   transl['_']='-';
            transl['В']=transl['V']='v';   transl['в']='v';   transl['?']='';
            transl['Г']=transl['G']='g';   transl['г']='g';   transl['!']='';
            transl['Д']=transl['D']='d';   transl['д']='d';   transl['.']='';
            transl['Е']=transl['E']='e';   transl['е']='e';   transl[',']='';
            transl['Ё']='yo';              transl['ё']='yo';  transl['\\']='';
            transl['Ж']='zh';              transl['ж']='zh';  transl['/']='';
            transl['З']=transl['Z']='z';   transl['з']='z';   transl[':']='';
            transl['И']=transl['I']='i';   transl['и']='i';   transl[';']='';
            transl['Й']=transl['J']='j';   transl['й']='j';   transl['|']='';
            transl['К']=transl['K']='k';   transl['к']='k';   transl['#']='';
            transl['Л']=transl['L']='l';   transl['л']='l';   transl['"']='';
            transl['М']=transl['M']='m';   transl['м']='m';   transl['\'']='';
            transl['Н']=transl['N']='n';   transl['н']='n';   transl['(']='';
            transl['О']=transl['O']='o';   transl['о']='o';   transl[')']='';
            transl['П']=transl['P']='p';   transl['п']='p';   transl['[']='';
            transl['Р']=transl['R']='r';   transl['р']='r';   transl[']']='';
            transl['С']=transl['S']='s';   transl['с']='s';   transl['{']='';
            transl['Т']=transl['T']='t';   transl['т']='t';   transl['}']='';
            transl['У']=transl['U']='u';   transl['у']='u';   transl['%']='';
            transl['Ф']=transl['F']='f';   transl['ф']='f';   transl['*']='';
            transl['Х']=transl['H']='h';   transl['х']='h';
            transl['Ц']=transl['C']='c';   transl['ц']='c';
            transl['Ч']='ch';              transl['ч']='ch';
            transl['Ш']='sh';              transl['ш']='sh';
            transl['Щ']='shh';             transl['щ']='shh';
            transl['Ъ']='';                transl['ъ']='';
            transl['Ы']=transl['Y']='y';   transl['ы']='y';
            transl['Ь']='';                transl['ь']='';
            transl['Э']='e';               transl['э']='e';
            transl['Ю']='yu';              transl['ю']='yu';
            transl['Я']='ya';              transl['я']='ya';

            var result='';
            for(i=0;i<text.length;i++) {
                if(transl[text[i]]!==undefined) { result+=transl[text[i]]; }
                else { result+=text[i]; }
            }
            return result;
}


function validateEmpty(text) {
    if ( text === '' ) {
        return false;
    } else {
        return true;
    }
}

function validateEmail(email) {
    if ( email === '' ) return false;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    if ( phone === '' ) return false;
    var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/;
    return re.test(phone);
}
