Helper = new function() {
    this.checkAll = function(el) {
        var checked = false;
        if (el.checked == true) {
            checked = true;
        }

        var i_items = document.getElementsByTagName( 'input' );
        for(var i = 0; i < i_items.length; i++) {
            var m = null
            if ((i_items[i].type == 'checkbox') && (m = i_items[i].id.match(/^[a-z_\-]+_([0-9]+)$/))) {
                i_items[i].checked = checked;
            }
        }
    }

    this.updatePriority = function(list_id, params) {
        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/sort',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {

                    console.log('Helper: priority update [success]');
                } else {
                    console.log('Helper: priority update [failed]');
                }
            },
            error:function () {
                console.log('Helper: priority update [failed]');
            }
        });
    }

    this.deleteItems = function() {
        var count = 0;

        var params = new Object();
        var del_items = document.getElementsByTagName( 'input' );
        for(var i = 0; i < del_items.length; i++) {
            var m = null
            if ((del_items[i].type == 'checkbox') && (del_items[i].checked) && (m = del_items[i].id.match(/^[a-z0-9_\-]+_([0-9]+)$/i))) {
                var item_id = m[1];
                params['item_'+item_id] = true;
                count++;
            }
        }

        if (count == 0) {
            alert('Вы не выбрали ни одной позиции');
            return;
        }

        if (!confirm('Вы действительно хотите удалить выбранные позиции?\nОтмечено: '+count+'')) return;

        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/drop',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {
                    for( var item_id in response.data.items ) {
                        var tr = $('#row_item_'+item_id);

                        if( tr.size() !== 0 ) {
                            var size = tr.find('td').size();
                            var td = $("<td/>",{
                                colspan: size
                            }).text('Позиция удалена');
                            tr.empty().append(td);
                        }
                    }
                    console.log('Helper: delete items [success]');
                } else {
                    console.log('Helper: delete items [failed]');
                }
            },

            error:function () {
                console.log('Helper: delete items [failed]');
            }
        });

        return false;
    }

    this.setHide = function(link, id) {
        link.blur();
        jQuery.ajax({
            url: App.url+'/hide',
            cache: false,
            dataType:'text',
            data: { 'id': id, 'view': App.view },
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);
                if( typeof response.data.hide !== 'undefined' ) {
                    if(response.data.status_message){
                        var status = response.data.status_message;
                    }else {
                        var status = response.data.hide==1?'<i title="Показать" class="icon-eye-close"></i>':'<i title="Скрыть" class="icon-eye-open"></i>';
                    }
                    $(".toggle_hide[data-id="+id+"]").html(status);
                    console.log('Helper: save hide [success]');
                } else {
                    console.log('Helper: save hide [failed]');
                }
            },

            error:function () {
                console.log('Helper: save hide [failed]');
            }
        });
        return false;
    }
    return this;
}();
