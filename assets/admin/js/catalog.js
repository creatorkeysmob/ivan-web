Catalog = new function() {
    this.checkAll = function(el) {
        var checked = false;
        if (el.checked == true) {
            checked = true;
        }

        var i_items = document.getElementsByTagName( 'input' );
        for(var i = 0; i < i_items.length; i++) {
            var m = null
            if ((i_items[i].type == 'checkbox') && (m = i_items[i].id.match(/^[a-z_\-]+_([0-9]+)$/))) {
                i_items[i].checked = checked;
            }
        }
    }

    this.updateGroupsPriority = function(list_id, params) {
        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/group_sort',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {

                    console.log('Catalog: priority update [success]');
                } else {
                    console.log('Catalog: priority update [failed]');
                }
            },
            error:function () {
                console.log('Catalog: priority update [failed]');
            }
        });
    }

    this.deleteGroups = function() {
        var count = 0;

        var params = new Object();
        var del_items = document.getElementsByTagName( 'input' );
        for(var i = 0; i < del_items.length; i++) {
            var m = null
            if ((del_items[i].type == 'checkbox') && (del_items[i].checked) && (m = del_items[i].id.match(/^[a-z0-9_\-]+_([0-9]+)$/i))) {
                var item_id = m[1];
                params['item_'+item_id] = true;
                count++;
            }
        }

        if (count == 0) {
            alert('Вы не выбрали ни одной позиции');
            return;
        }

        if (!confirm('Вы действительно хотите удалить выбранные позиции?\nОтмечено: '+count+'')) return;

        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/group_drop',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {
                    for( var item_id in response.data.items ) {
                        var tr = $('#row_item_'+item_id);

                        if( tr.size() !== 0 ) {
                            var size = tr.find('td').size();
                            var td = $("<td/>",{
                                colspan: size
                            }).text('Позиция удалена');
                            tr.empty().append(td);
                        }
                    }
                    console.log('Catalog: delete groups [success]');
                } else {
                    console.log('Catalog: delete groups [failed]');
                }
            },

            error:function () {
                console.log('Catalog: delete groups [failed]');
            }
        });

        return false;
    }

    this.setGroupsHide = function(link, id) {
        link.blur();
        jQuery.ajax({
            url: App.url+'/group_hide',
            cache: false,
            dataType:'text',
            data: { 'id': id, 'view': App.view },
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);
                if( typeof response.data.hide !== 'undefined' ) {
                    if(response.data.status_message){
                        var status = response.data.status_message;
                    }else {
                        var status = response.data.hide==1?'<i class="icon-remove"></i>':'<i class="icon-ok"></i>';
                    }
                    $(link).html(status);
                    console.log('Catalog: save hide [success]');
                } else {
                    console.log('Catalog: save hide [failed]');
                }
            },

            error:function () {
                console.log('Catalog: save hide [failed]');
            }
        });
        return false;
    }

    this.updateItemsPriority = function(list_id, params) {
        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/item_sort',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {

                    console.log('Catalog: priority update [success]');
                } else {
                    console.log('Catalog: priority update [failed]');
                }
            },
            error:function () {
                console.log('Catalog: priority update [failed]');
            }
        });
    }

    this.deleteItems = function() {
        var count = 0;

        var params = new Object();
        var del_items = document.getElementsByTagName( 'input' );
        for(var i = 0; i < del_items.length; i++) {
            var m = null
            if ((del_items[i].type == 'checkbox') && (del_items[i].checked) && (m = del_items[i].id.match(/^[a-z0-9_\-]+_([0-9]+)$/i))) {
                var item_id = m[1];
                params['item_'+item_id] = true;
                count++;
            }
        }

        if (count == 0) {
            alert('Вы не выбрали ни одной позиции');
            return;
        }

        if (!confirm('Вы действительно хотите удалить выбранные позиции?\nОтмечено: '+count+'')) return;

        params['view'] = App.view;
        jQuery.ajax({
            url: App.url+'/item_drop',
            cache: false,
            dataType:'text',
            data: params,
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);

                if( typeof response.data !== "undefined" && typeof response.data.items !== "undefined" ) {
                    for( var item_id in response.data.items ) {
                        var tr = $('#row_item_'+item_id);

                        if( tr.size() !== 0 ) {
                            var size = tr.find('td').size();
                            var td = $("<td/>",{
                                colspan: size
                            }).text('Позиция удалена');
                            tr.empty().append(td);
                        }
                    }
                    console.log('Catalog: delete groups [success]');
                } else {
                    console.log('Catalog: delete groups [failed]');
                }
            },

            error:function () {
                console.log('Catalog: delete groups [failed]');
            }
        });

        return false;
    }

    this.setItemsHide = function(link, id) {
        link.blur();
        jQuery.ajax({
            url: App.url+'/item_hide',
            cache: false,
            dataType:'text',
            data: { 'id': id, 'view': App.view },
            type: 'post',
            complete:function (xhr) {
                var response = $.parseJSON(xhr.responseText);
                if( typeof response.data.hide !== 'undefined' ) {
                    if(response.data.status_message){
                        var status = response.data.status_message;
                    }else {
                        var status = response.data.hide==1?'<i class="icon-remove"></i>':'<i class="icon-ok"></i>';
                    }
                    $(link).html(status);
                    console.log('Catalog: save hide [success]');
                } else {
                    console.log('Catalog: save hide [failed]');
                }
            },

            error:function () {
                console.log('Catalog: save hide [failed]');
            }
        });
        return false;
    }
    return this;
}();