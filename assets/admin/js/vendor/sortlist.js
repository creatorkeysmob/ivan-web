function in_array(arr, needle) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == needle) {
            return true;
        }
    }
    return false;
}

function _Sortlist() {
    this.edit_elements = ['input', 'textarea', 'a', 'label'];
	this.lists = new Object();
	
	this._create = function(list_id) {
		this.lists[list_id] = {
            type: null,
			move_item: null,
			on_update: null,
			is_change: false,
			css: {
		        hover:  'hover',
		        active: 'active'
		    }
		}
	}
	
	this.create = function(list_id) {
		var elem = document.getElementById(list_id);
		if ((elem == null) || ((elem.nodeName != 'UL') && (elem.nodeName != 'TABLE'))) {
			alert( 'Ошибка при создании Sortlist:\nэлемент с id: "'+list_id+'" либо не создан, либо не является списком пригодным для сортировки' );
			return;
		}
		this._create(list_id);
        
        if ((arguments.length > 1) && (typeof(arguments[1]) == 'object')) {
            if ((arguments[1].onUpdate != undefined) && (typeof(arguments[1].onUpdate) == 'function')) {
                this.lists[list_id]['on_update'] = arguments[1].onUpdate;
            }
			if (arguments[1]['css_hover']  != null) this.lists[list_id]['css']['hover']  = arguments[1]['css_hover'];
			if (arguments[1]['css_active'] != null) this.lists[list_id]['css']['active'] = arguments[1]['css_active'];
        }
		if (elem.nodeName == 'TABLE') {
            this.lists[list_id]['type'] = 'table';
            var tbody = elem.getElementsByTagName('tbody')[0];
			if (tbody == null) {
				alert( 'Некорректная структура таблицы' );
				return;
			}
            for (var i = 0; i < tbody.childNodes.length; i++) {
                var tr = tbody.childNodes[i];
                if ((tr.tagName == 'TR') && (tr.id.match(/^[a-z0-9_\-]+_([0-9]+)$/i))) {
                    Sortlist.insertItem(list_id, tr);
                }
            }
		} else {
            this.lists[list_id]['type'] = 'ul';
            for (var i = 0; i < elem.childNodes.length; i++) {
                var li = elem.childNodes[i];
                if ((li.tagName == 'LI') && (li.id.match(/^[a-z0-9_\-]+_([0-9]+)$/i))) {
                    Sortlist.insertItem(list_id, li);
                }
            }
		}
	}
    
    this.insertItem = function(list_id, li) {
        if ((li.nodeName == 'LI') || (li.nodeName == 'TR')) {
			li.onselectstart = function() { return false; }
            li.onmousedown = function(e) {
                if (!e) e = window.event;
                var elem = e.target || e.srcElement;
                
                while (elem.parentNode) {
                    if (in_array(Sortlist.edit_elements, elem.tagName.toLowerCase())) {
                        return true;
                    }
                    elem = elem.parentNode;
                }
                
                Sortlist.lists[list_id].move_item = li;
				Sortlist.addClassName(li, Sortlist.lists[list_id]['css']['active']);
				
				return false;
            }
            li.onmouseover = function() {
				var move_item = Sortlist.lists[list_id].move_item;
                if ((move_item != null) && (move_item != li)) {
					Sortlist.swapItems(list_id, move_item, li);
				} else {
					Sortlist.addClassName(li, Sortlist.lists[list_id]['css']['hover']);
				}
            }
            li.onmouseout = function() {
				Sortlist.removeClassName(li, Sortlist.lists[list_id]['css']['hover']);
            }
        }
	}
	
	this.swapItems = function(list_id, src_item, dest_item) {
		var l = src_item.parentNode;
        
        for (var i = 0; i < l.childNodes.length; i++) {
            if (l.childNodes[i] == src_item) {
                src_i = i;
            }
            if (l.childNodes[i] == dest_item) {
                dest_i = i;
            }
        }
		
		if (src_i > dest_i) {
			l.insertBefore(src_item, dest_item);
		} else {
			l.insertBefore(src_item, dest_item.nextSibling);
		}
		this.lists[list_id].is_change = true;
	}
	
    this.addClassName = function(elem, name) {
		var reg = new RegExp('\\b'+name+'\\b');
        if (!elem.className.match(reg)) {
            elem.className += ' '+name;
        }
    }
    
    this.removeClassName = function(elem, name) {
		var reg = new RegExp('\\b'+name+'\\b');
		
		elem.className = elem.className.replace(reg, ' ');
    }

    $(document).bind('mouseup', function(){
        for (var list_id in Sortlist.lists) {
            if (Sortlist.lists[list_id].move_item != null) {
                var move_item = Sortlist.lists[list_id].move_item;
                Sortlist.removeClassName(move_item, Sortlist.lists[list_id]['css']['hover']);
                Sortlist.removeClassName(move_item, Sortlist.lists[list_id]['css']['active']);
                Sortlist.lists[list_id].move_item = null;
            }
            if (Sortlist.lists[list_id].is_change) {
                if (typeof(Sortlist.lists[list_id].on_update) == 'function') {
                    Sortlist.lists[list_id].is_change = false;
                    var params = new Object();
                    if (Sortlist.lists[list_id].type == 'table') {
                        var priority = 1;
                        var elem = document.getElementById( list_id );
                        var tbody = elem.getElementsByTagName( 'tbody' )[0];
                        for (var i = 0; i < tbody.childNodes.length; i++) {
                            var tr = tbody.childNodes[i];
                            var m = null;
                            if ((tr.nodeName == 'TR') && (m = tr.id.match(/^[a-z_\-]+_([0-9]+)$/i))) {
                                params['item_'+m[1]] = priority;
                                priority++;
                            }
                        }
                    } else {
                        var priority = 1;
                        var elem = document.getElementById( list_id );
                        for (var i = 0; i < elem.childNodes.length; i++) {
                            var li = elem.childNodes[i];
                            var m = null;
                            if ((li.nodeName == 'LI') && (m = li.id.match(/^[a-z_\-]+_([0-9]+)$/i))) {
                                params['item_'+m[1]] = priority;
                                priority++;
                            }
                        }
                    }
                    Sortlist.lists[list_id].on_update(i, params);
                }
            }
        }
    });

	return this;
}
var Sortlist = new _Sortlist();