function prepareImulated(id, elem){
    var value = elem.value;
    var path = value.split(/\\/);
    document.getElementById(id).value = path[path.length-1];
}

$(document).ready(function(){
    $('input, textarea').placeholder();
    $.pnotify.defaults.history = false;
    $("select.select2").select2();

    //show messages
    if( App.messages.length > 0 ) {
        for( var index in App.messages ) {
            $.pnotify({
                text: App.messages[index].text,
                type: App.messages[index].type,
                delay: 1000,
                width: '170px',
                closer: false,
                sticker: false
            });
        }
    }
});
