<?php defined('SYSPATH') or die('No direct script access.');

class Basic extends Migration
{
  public function up()
  {
      $config = Kohana::$config->load('database.default');

      // Create slider structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_slider` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `head` varchar(255) NOT NULL,
          `subhead` varchar(255) NOT NULL,
          `img` varchar(255) NOT NULL,
          `priority` int(11) NOT NULL,
          `hide` tinyint(3) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      // Create clients structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_clients` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `head` varchar(255) NOT NULL,
                `subhead` varchar(255) NOT NULL,
                `created` datetime NOT NULL,
                `img` varchar(255) NOT NULL,
                `priority` int(11) NOT NULL DEFAULT '0',
                `main` tinyint(3) NOT NULL DEFAULT '0',
                `hide` tinyint(3) NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      // Create stock structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_stock` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `alias` varchar(255) NOT NULL,
                `head` varchar(255) NOT NULL,
                `title` varchar(255) NOT NULL,
                `anounce` text NOT NULL,
                `body` text NOT NULL,
                `created` datetime NOT NULL,
                `description` text NOT NULL,
                `keywords` text NOT NULL,
                `hide` tinyint(3) NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`),
                KEY `fname` (`alias`),
                KEY `publish_date` (`created`),
                FULLTEXT KEY `head` (`head`,`body`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      // Create pages structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_pages` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
              `section_id` int(11) unsigned NOT NULL DEFAULT '0',
              `head` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
              `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `body` longtext COLLATE utf8_unicode_ci NOT NULL,
              `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `module` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
              `fullpath` text COLLATE utf8_unicode_ci NOT NULL,
              `created` datetime NOT NULL,
              `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `description` text COLLATE utf8_unicode_ci NOT NULL,
              `keywords` text COLLATE utf8_unicode_ci NOT NULL,
              `lft` int(11) NOT NULL,
              `rgt` int(11) NOT NULL,
              `lvl` int(11) NOT NULL,
              `scope` int(11) NOT NULL,
              `priority` int(11) NOT NULL DEFAULT '0',
              `menushow` tinyint(1) NOT NULL DEFAULT '1',
              `mapshow` tinyint(1) NOT NULL DEFAULT '1',
              `hide` tinyint(3) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              KEY `parent_id` (`parent_id`),
              KEY `module` (`module`),
              FULLTEXT KEY `head` (`head`,`body`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;"
      );

      $this->sql(
           "INSERT INTO `pe_pages` (`id`, `section_id`, `parent_id`, `head`, `alias`, `body`, `redirect`, `module`, `fullpath`, `created`, `title`, `description`, `keywords`, `lft`, `rgt`, `lvl`, `scope`, `priority`, `menushow`, `mapshow`, `hide`) VALUES
            (1, 0, 0, 'root', 'root', '', '', '', '', '2013-01-01 00:00:00', '', '', '', 1, 28, 1, 1, 1, 0, 0, 1),
            (7, 7, 1, 'Текстовая страница', 'test', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget fermentum augue. Morbi pretium est mi, quis ullamcorper elit aliquet sed. Donec imperdiet nunc sit amet sapien molestie congue. Cras imperdiet mattis felis vestibulum rhoncus. Suspendisse auctor tellus a dignissim aliquam. Donec tempor risus sed orci condimentum, sed volutpat magna iaculis. Nullam rutrum, neque eu interdum tempor, augue nisl aliquet mauris, et volutpat purus ante id sapien. Donec malesuada purus dui, id viverra urna egestas et. Nam aliquet porta adipiscing.</p>\n<p>Nunc eu aliquam mi, vitae pellentesque est. Donec euismod a tellus non egestas. Maecenas laoreet eros cursus enim eleifend, nec volutpat odio suscipit. Sed luctus consectetur blandit. Sed nunc diam, auctor eget ligula nec, auctor porttitor elit. Curabitur lorem dui, varius eu massa sed, fringilla pulvinar quam. Fusce malesuada tellus dolor, nec consectetur odio facilisis sit amet. Aenean vitae neque a augue malesuada ultrices. Aliquam ultricies, enim quis varius consectetur, risus arcu tempus odio, in commodo orci purus nec justo. Ut mattis laoreet malesuada. Etiam venenatis ipsum urna, id ultrices erat scelerisque vitae. Nullam non eros arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>', '', 'text', '/test', '2013-10-07 14:05:00', '', '', '', 20, 21, 2, 1, 2, 1, 1, 0),
            (11, 11, 1, 'Новости', 'news', '', '', 'news', '/news', '2013-10-07 16:22:00', '', '', '', 22, 23, 2, 1, 1, 1, 1, 0),
            (12, 12, 1, 'Поиск', 'search', '', '', 'search', '/search', '2013-10-07 16:22:00', '', '', '', 24, 25, 2, 1, 4, 0, 0, 0),
            (13, 13, 1, 'Каталог', 'catalog', '', '', 'catalog', '/catalog', '2013-10-07 16:23:00', '', '', '', 26, 27, 2, 1, 3, 1, 1, 0);"
      );

      // Create settings structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_settings` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `key` varchar(50) CHARACTER SET utf8 NOT NULL,
              `value` text CHARACTER SET utf8 NOT NULL,
              PRIMARY KEY (`id`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;"
      );

      // Create search structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_search` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `related` int(11) NOT NULL,
              `hash` varchar(32) NOT NULL,
              `module` varchar(64) NOT NULL,
              `head` tinytext NOT NULL,
              `body` text NOT NULL,
              `score` double NOT NULL DEFAULT '0',
              `sdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `fullpath` tinytext NOT NULL,
              PRIMARY KEY (`id`),
              KEY `search_md5` (`hash`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      // Create roles structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_roles` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(32) NOT NULL,
              `description` varchar(255) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `uniq_name` (`name`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      $this->sql(
          "INSERT INTO `pe_roles` (`id`, `name`, `description`) VALUES
           (1, 'login', 'Login privileges, granted after account confirmation'),
           (2, 'admin', 'Administrative user, has access to everything.');"
      );

      // Create roles-users structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_roles_users` (
              `user_id` int(10) unsigned NOT NULL,
              `role_id` int(10) unsigned NOT NULL,
              PRIMARY KEY (`user_id`,`role_id`),
              KEY `fk_role_id` (`role_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
      );

      $this->sql(
          "INSERT INTO `pe_roles_users` (`user_id`, `role_id`) VALUES
            (1, 1),
            (1, 2),
            (2, 1);"
      );

      // Create users structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_users` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `email` varchar(254) NOT NULL,
              `fio` varchar(255) NOT NULL,
              `username` varchar(32) NOT NULL DEFAULT '',
              `password` varchar(64) NOT NULL,
              `modules` text NOT NULL,
              `logins` int(10) unsigned NOT NULL DEFAULT '0',
              `last_login` int(10) unsigned DEFAULT NULL,
              `exclude` tinyint(3) NOT NULL DEFAULT '0',
              `hide` tinyint(3) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `uniq_username` (`username`),
              UNIQUE KEY `uniq_email` (`email`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      $password = $config['connection']['password'];
      if(empty($password)) {
          $password = 'rootroot';
      }

      $hash = Auth::instance()->hash($password);
      $this->sql(
          "INSERT INTO `pe_users` (`id`, `email`, `fio`, `username`, `password`, `modules`, `logins`, `last_login`, `exclude`, `hide`) VALUES
            (1, 'root@wearelions.ru', 'Суперпользователь', 'root', '$hash', '[]', 0, NULL, 1, 0),
            (2, 'test@wearelions.ru', 'Тестовый доступ', 'test', '58bb04112f9ec918cad97ae49f4d4ab0366a844d3e5f5b045e7c26040c0ebc37', '[\"pages\",\"news\",\"catalog\",\"files\",\"users\",\"settings\"]', 0, NULL, 0, 0);"
      );

      // Create users-token structure
      $this->sql(
          "CREATE TABLE IF NOT EXISTS `pe_user_tokens` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `user_id` int(11) unsigned NOT NULL,
              `user_agent` varchar(40) NOT NULL,
              `token` varchar(40) NOT NULL,
              `created` int(10) unsigned NOT NULL,
              `expires` int(10) unsigned NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `uniq_token` (`token`),
              KEY `fk_user_id` (`user_id`),
              KEY `expires` (`expires`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
      );

      // Create foreign keys
      $this->sql(
          "ALTER TABLE `pe_roles_users`
          ADD CONSTRAINT `pe_roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pe_users` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `pe_roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `pe_roles` (`id`) ON DELETE CASCADE;"
      );

      $this->sql(
          "ALTER TABLE `pe_user_tokens`
          ADD CONSTRAINT `pe_user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pe_users` (`id`) ON DELETE CASCADE;"
      );

      unset($config);
  }

  public function down() {

    $this->drop_table('pe_clients');
    $this->drop_table('pe_stock');
    $this->drop_table('pe_pages');
    $this->drop_table('pe_settings');
    $this->drop_table('pe_search');

    $this->drop_table('pe_user_tokens');
    $this->drop_table('pe_roles_users');
    $this->drop_table('pe_roles');
    $this->drop_table('pe_users');

    // Restart sessions
    Session::instance()->restart();
  }
}