<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;
if (is_file(APPPATH.'classes/Kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/Kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/Kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('Asia/Yekaterinburg');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));
require_once DOCROOT.'vendor/autoload.php';

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('ru-ru');

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(
    array(
        'base_url'   => '/',
        'index_file' => FALSE
    )
);

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Load current environment from config
 */

$kohanaConfig = Kohana::$config->load('kohana')->as_array();
Kohana::$environment = Arr::get($kohanaConfig, 'environment', Kohana::DEVELOPMENT);

/**
 * Load DI container
 */
require_once 'services.php';

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
$development_modules = array();
if( Kohana::$environment == Kohana::DEVELOPMENT OR PHP_SAPI == 'cli' ) {
    $development_modules = array(
        'minion'       => MODPATH . 'minion', // Minion module
        'generator'    => MODPATH . 'generator', // Generator module
        'debugtoolbar' => MODPATH . 'debugtoolbar', // Debug module
        'migrations'   => MODPATH . 'migrations', // Migrations module
    );
}

$production_modules = array(
    'helper'     => MODPATH . 'helper', // Helper module
    'yaml'       => MODPATH . 'yaml', // YAML config & i18n reader
    'mailer'     => MODPATH . 'mailer', // Swiftmailer
    'auth'       => MODPATH . 'auth', // Basic authentication
    'cache'      => MODPATH . 'cache', // Caching with multiple backends
    'codebench'  => MODPATH . 'codebench', // Benchmarking tool
    'database'   => MODPATH . 'database', // Database access
    'thumb'      => MODPATH . 'thumb', // Image manipulation
    'image'      => MODPATH . 'image', // Image manipulation
    'orm'        => MODPATH . 'orm', // Object Relationship Mapping
    'orm-mptt'   => MODPATH . 'orm-mptt', // ORM MPTT
    'breadcrumb' => MODPATH . 'breadcrumb', // Basic breadcrumb module
    'message'    => MODPATH . 'message', // Basic message module
    'twig'       => MODPATH . 'twig', // Twig template engine,
    'pagination' => MODPATH . 'pagination', // Simple pagination
    'former'     => MODPATH . 'former', // Simple form buider
    'restful'    => MODPATH . 'restful', // Api module
    'admin'      => MODPATH . 'admin', // Admin module
    'public'     => MODPATH . 'public', // Site module
);

Kohana::modules( array_merge($development_modules, $production_modules) );

/**
 * Set the routes. Admin route
 */
Cookie::$salt = '6cab0009d491fee280d8c8904954ab8f';

// Loading routes
require_once 'route.php';