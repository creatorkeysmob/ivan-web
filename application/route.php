<?php defined('SYSPATH') OR die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
Route::set('migrations', 'migrations')->defaults(
    array(
        'controller' => 'Controller_Flexiblemigrations',
        'action' => 'index',
        'prefix' => ''
    )
);