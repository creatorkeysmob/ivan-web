<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Widget
 */
abstract class Widget {
    protected static $_menu     = null;
    protected static $_template = null;
    protected static $_submenu  = null;
    protected static $_id       = null;
    protected static $_slider   = null;
    protected static $_list     = null;
    protected static $_biglist  = null;
    protected static $_item     = null;
    protected static $_form     = null;


    public static function getTemplate($fullpath){
        $template = ORM::factory('Pages')
            ->where('fullpath','=', '/'. $fullpath)
            ->find_all()
            ->as_array();

        self::$_template = $template[0]->template;
        unset($template);

        return self::$_template;
    }

    public static function getPageId($fullpath){
        $item      = ORM::factory('Pages')->where('fullpath', '=', $fullpath)->find()->as_array();
        $id        = $item['id'];
        self::$_id = $id;
        unset($id);
        return self::$_id;
    }

    public static function getSubMenu($parent, $template){
        $submenu = ORM::factory('Pages')
            ->where('parent_id','=', $parent)
            ->and_where('template','=', $template)
            ->find_all()
            ->as_array();

        self::$_submenu = $submenu;
        unset($submenu);

        return self::$_submenu;
    }

    /**
     * @param int $parentCategoryId
     * @param int $categoryDeep
     * @param string $key
     * @return array
     */
    public static function getMenu($parentCategoryId = 1, $categoryDeep = 1, $key = 'menushow') {
        if(!is_null(self::$_menu[$parentCategoryId][$categoryDeep][$key])) {
            return self::$_menu[$parentCategoryId][$categoryDeep][$key];
        }

        if(empty($parentCategoryId)) $parentCategoryId = 1;

        /** @noinspection PhpUndefinedClassInspection */
        /** @var $rootCategory Model_Pages */
        $rootCategory = ORM::factory('Pages', $parentCategoryId);
        if(!$rootCategory->loaded()) {
            return array();
        }

        $pagesTree = array();
        $categoryDescedants = $rootCategory->descendants(FALSE, FALSE)
            ->where('hide', '=', 0)
            ->and_where($key, '=', 1)
           // ->and_where('created', '<=', DB::expr('NOW()'))
            ->order_by('priority', 'ASC')
            ->and_where('lvl', '>', $rootCategory->lvl)
            ->and_where('lvl', '<=', $rootCategory->lvl + $categoryDeep)
            ->find_all()
            ->as_array();

        if(sizeof($categoryDescedants)) {
            /** @var $documentInstance Document */
            $documentInstance = Registry::get(Registry::documentManager);
            $currentPage = $documentInstance->getModel();
            $currentPageParents = array();

            if(!empty($currentPage->id)) {
                $currentPageParents = $currentPage->parents(FALSE)
                                                  ->where('hide','=',0)
                                                  ->find_all()
                                                  ->as_array(null, 'id');
                array_push($currentPageParents, $currentPage->id);
            }

            $currentPageId = $currentPage->id;

            /** @noinspection PhpUndefinedClassInspection */
            $currentFullpath = Request::$current ? Request::$current->detect_uri() : null;
            $currentFullpath = '/'.$currentFullpath;
            $pageObjects = array_map(
                function($page) use($currentPageParents, $currentPageId, $currentFullpath){
                    /** @var $page Model_Pages */
                    $page = $page->as_array();
                    $page['active'] = in_array($page['id'], $currentPageParents) ? true : false;
                    $page['current'] = ($page['fullpath'] == $currentFullpath) ? true : false;
                    return $page;
                },
                $categoryDescedants
            );

            $pagesTree = Helper_Tree::transformArrayToTree($pageObjects);
            unset($pageObjects);

            // Filter invalid entities
            $rootLevelAllowed = $rootCategory->lvl + 1;
            $pagesTree = array_filter($pagesTree, function($page) use ($rootLevelAllowed) {
                return $page['lvl'] == $rootLevelAllowed ? true : false;
            });
        }
        unset($categoryDescedants);
        self::$_menu[$parentCategoryId][$categoryDeep][$key] = $pagesTree;
        return $pagesTree;
    }

    public function getItem($table, $tmpl, $param, $page = false) {

        /** @var $item Model_News */
        $item = ORM::factory($table)
            ->where($param[0], $param[1], $param[2])
            ->and_where('hide', '=', 0)
            ->find();

        if ( !$item->loaded() ) {
            /** @noinspection PhpUndefinedClassInspection */
            throw new HTTP_Exception_404();
        }
        Breadcrumb::instance()->addItem($item->head);
        $item->body = html_entity_decode($item->body);

        $this->render($tmpl, array(
                'item'  => $item,
                'table' => strtolower($table),
            )
        );
    }

    public static function getModule($table, $params_and = array(), $limit = 99999999999 ){
        $list = ORM::factory($table)
            ->where('hide','=', 0);

        foreach ($params_and as $param) {
            $list = $list->and_where($param['field'], $param['sign'], $param['arg']);
        }

        $list = $list->order_by('priority','ASC')
            ->limit($limit)
            ->find_all()
            ->as_array();

        foreach ($list as $key => $value) {
            $list[$key]->head = html_entity_decode($value->head);
            $list[$key]->body = html_entity_decode($value->body);
        }

        $_biglist = $list;
        unset($list);

        return $_biglist;
    }
    public static function getUltraModule (
                                $table,
                                $need_fullpath = false,
                                $params_and = array(),
                                $params_or = array(),
                                $limit = 9999999999,
                                $order = array('priority', 'ASC'),
                                $hide = true ){

        $items = ORM::factory($table);
        if( $hide ){
            $items = $items->where('hide', '=', 0);
        } else {
            $items = $items->where('hide', '>=', 0);
        }
        if ( count($params_and) > 0 ){
            foreach ($params_and as $param) {
                $items = $items->and_where($param['field'], $param['sign'], $param['arg']);
            }
        }

        if ( count($params_or) > 0  ) {
            $items = $items->where_open();
            foreach ($params_or as $param) {
                $items = $items->or_where($param['field'], $param['sign'], $param['arg']);
            }
            $items = $items->where_close();
        }
        $items = $items
                ->order_by($order[0], $order[1])
                ->limit($limit)
                ->find_all()
                ->as_array();

        // $filtr_params = array(
        //     // 'industry' => array(),
        //     // 'function' => array(),
        //     // 'product'  => array(),
        // );

        if($items && $need_fullpath) {
            /** @noinspection PhpUndefinedClassInspection */
            $currentFullpath = Request::$current ? Request::$current->detect_uri() : null;
            $currentFullpath = '/'.$currentFullpath;
            $table = strtolower($table);
            $filtr = array();
            $items = array_map(
                function($element) use ($table, &$filtr_params){
                    /** @var $element Model_Service */
                    $element = $element->as_array();
                    /** @noinspection PhpUndefinedClassInspection */
                    $element['fullpath'] = '/'.Route::get("public_$table")->uri(
                        array(
                            'alias' => $element['alias']
                        )
                    );

                    $element['current']  = ($element['fullpath'] == $currentFullpath) ? true : false;
                    $element['show'] = true; // for filter
                    $element['head'] = html_entity_decode($element['head']);
                    $element['body'] = html_entity_decode($element['body']);

                    // foreach ($filtr_params as $key => $value) {
                    //     $filtr_param = explode(',', $element[$key]);
                    //     foreach ($filtr_param as $value) {
                    //         $filtr_params[$key][Widget::translit(mb_strtolower(trim($value)))] = $value;
                    //     }
                    // }


                    return $element;
                },
                $items
            );
                $items = array_diff($items, array(''));
        }
        $_item = $items;
        unset($items);

        //return array( 'items' => $_item, 'filtr' => $filtr_params );
        return  $_item;
    }



    public static function getBigModule ( $table, $limit = 99999999999 ){
        $list = ORM::factory($table)
            ->where('hide','=', 0)
            ->order_by('priority','ASC')
            ->limit($limit)
            ->find_all()
            ->as_array();

        if($list) {
            /** @noinspection PhpUndefinedClassInspection */
            // $currentFullpath = Request::$current ? Request::$current->detect_uri() : null;
            // $currentFullpath = '/'.$currentFullpath;
            $table = strtolower($table);
            $filtr = array();
            $list = array_map(
                function($element) use ($table){
                    /** @var $element Model_Service */
                    $element = $element->as_array();
                    /** @noinspection PhpUndefinedClassInspection */
                    $element['fullpath'] = '/'.Route::get("public_$table")->uri(
                        array(
                            'alias' => $element['alias']
                        )
                    );
                    $element['body'] = html_entity_decode($element['body']);
                    $element['img']  = ($element['img']) ? $element['img'] : '/assets/public/images/no-photo.png';

                    // $element['current']  = ($element['fullpath'] == $currentFullpath) ? true : false;
                    return $element;
                },
                $list
            );
                $list = array_diff($list, array(''));
        }

        $_list = $list;
        unset($list);
        return $_list;
    }




    public static function getItemsByParam($table, $params, $limit = 9999999999 ){
        $items = ORM::factory($table)
                ->where($params['field'], $params['sign'], $params['arg'])
                ->and_where('hide', '=', 0)
                ->limit($limit)
                ->find_all()
                ->as_array();

        if($items) {
            /** @noinspection PhpUndefinedClassInspection */
            $currentFullpath = Request::$current ? Request::$current->detect_uri() : null;
            $currentFullpath = '/'.$currentFullpath;
            $table = strtolower($table);
            $filtr = array();
            $items = array_map(
                function($element) use ($table){
                    /** @var $element Model_Service */
                    $element = $element->as_array();
                    /** @noinspection PhpUndefinedClassInspection */
                    $element['fullpath'] = '/'.Route::get("public_$table")->uri(
                        array(
                            'alias' => $element['alias']
                        )
                    );

                    $element['current']  = ($element['fullpath'] == $currentFullpath) ? true : false;
                    return $element;
                },
                $items
            );
                $items = array_diff($items, array(''));
        }

        $_item = $items;
        unset($items);
        return $_item;
    }


    public function getForm () {
        $form = ORM::factory('Formconstruct')
            ->find_all()
            ->as_array();

        if ( $form ){
            $new_form = array();
            foreach ($form as $key => $value) {
                $value = $value->as_array();
                $fields = unserialize($value['fields']);
                if ( $fields ){
                    foreach ($fields as $key => $field) {
                        if ( $field[3] != '0' ) {
                            $value['html_fields'][] = Twig::factory('Formconstruct/'.ucfirst($field[3]), array('data' => $field))->render();
                        }

                    }
                }
                $new_form[$value['alias']] = $value;

            }
        }


        $_form = $new_form;
        unset($form);
        unset($new_form);

        return $_form;
    }

    public function getRusMonth($month = ''){
        $rus_month = array(
            1  => 'января',
            2  => 'февраля',
            3  => 'марта',
            4  => 'апреля',
            5  => 'мая',
            6  => 'июня',
            7  => 'июля',
            8  => 'августа',
            9  => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        );

        return strtr($month, $rus_month);
    }

// склонение существительного после числительного
    public function pluralForm($number, $after) {
      $cases = array (2, 0, 1, 1, 1, 2);
      return $after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }

    public static function getBreadcrumbs(){
        return Breadcrumb::instance()->getItems();
    }

    public function _group_by($array, $key) {
        $resultArr = array();
        foreach ($array as $val) {
            $resultArr[$val[$key]][] = $val;
        }
        return $resultArr;
    }

    public function translit ($name = '') {
        $tr_table = array (
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',   'Д' => 'D',  'Е' => 'E', 'Ё' => 'E',  'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => "'", 'К' => 'K', 'Л' => 'L',   'М' => 'M',  'Н' => 'N', 'О' => 'O',  'П' => 'P',  'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',   'Х' => 'H',  'Ц' => 'C', 'Ч' => 'Ch', 'Щ' => 'Sh', 'Ш' => 'Sh',
        'Ь' => '',  'Ы' => 'Y', 'Ъ' => '',  'Э' => 'E',   'Ю' => 'Yu', 'Я' => 'Ya',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',   'д' => 'd',  'е' => 'e', 'ё' => 'e',  'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'i', 'к' => 'k', 'л' => 'l',   'м' => 'm',  'н' => 'n', 'о' => 'o',  'п' => 'p',  'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',   'х' => 'h',  'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh',
        'ь' => '',  'ы' => 'y', 'ъ' => '',  'э' => 'e',   'ю' => 'yu', 'я' => 'ya',
        ' ' => '_', "'" => '',  '"' => '',  '&' => 'and', ',' =>'',    '/' =>'',   '%' =>'');
        return strtr($name, $tr_table);
    }

    public function dirDel ($dir) {
        $d=opendir($dir);
        while(($entry=readdir($d))!==false) {
            if ($entry != "." && $entry != ".."){
                if (is_dir($dir."/".$entry)) {
                    self::dirDel($dir."/".$entry);
                } else {
                    unlink ($dir."/".$entry);
                }
            }
        }
        closedir($d);
        rmdir ($dir);
    }

}
