<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Registry
 */
abstract class Registry {
    const documentManager = 'document';
    const moduleManager = 'module';
    const settingsManager = 'settings';
    const standartContainer = 'container';

    /**
     * @var array
     */
    protected static $_storedVariables = array();

    /**
     * @param $key
     * @param bool $loadFromContainer
     * @return mixed
     */
    public static function get($key, $loadFromContainer = TRUE) {
        $objectInstance = Registry::$_storedVariables[$key];
        if(is_null($objectInstance) && $loadFromContainer) {
            $standartContainer = Registry::get(Registry::standartContainer, FALSE);
            $objectInstance = $standartContainer[$key];
            Registry::set($key, $objectInstance);
            unset($standartContainer);
        }
        return $objectInstance;
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value) {
        Registry::$_storedVariables[$key] = $value;
    }
}