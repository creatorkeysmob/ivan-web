<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Component_Document
 */
class Component_Document {
    /**
     * Loaded Pages model
     * @var Model_Pages
     */
    protected $_model = null;
    /**
     * @var array
     */
    protected $_matches = null;
    /**
     * @var string
     */
    protected $_fullpath = null;

    /**
     * @var string
     */
    protected $_uri = null;

    /**
     * @var string
     */
    protected $_module = null;

    /**
     * @param string $fullpath
     */
    public function setFullpath($fullpath) {
        $this->_fullpath = $fullpath;
    }

    /**
     * @return string
     */
    public function getFullpath() {
        return $this->_fullpath;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri) {
        $this->_uri = $uri;
    }

    /**
     * @return string
     */
    public function getUri() {
        return $this->_uri;
    }

    /**
     * @param array $matches
     */
    public function setMatches($matches) {
        $this->_matches = $matches;
    }

    /**
     * @return array
     */
    public function getMatches() {
        return $this->_matches;
    }

    /**
     * @param Model_Pages $model
     */
    public function setModel($model) {
        $this->_model = $model;
    }

    /**
     * @return Model_Pages
     */
    public function getModel() {
        return $this->_model;
    }

    /**
     * @param string $module
     */
    public function setModule($module) {
        $this->_module = $module;
    }

    /**
     * @return string
     */
    public function getModule() {
        return $this->_module;
    }
}