<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Component_Module
 */
class Component_Module {
    protected $_loadedConfigurations = null;

    /**
     * @param $config
     */
    public function setConfig($config) {
        $this->_loadedConfigurations = $config;
    }

    /**
     * Get module information
     * @param null $path
     * @throws Exception
     * @return mixed
     */
    public function getConfig($path = null) {
        if(is_null($this->_loadedConfigurations)) {
            $modules = Kohana::$config->load('admin/main.modules.public');
            if( !$modules ) {
                throw new Exception('ModuleManager: public modules does not exists');
            }

            try{
                // Get binded module
                $binded = DB::select('module','fullpath')->from('pages')
                    ->where('module','<>','')
                    ->and_where('module','<>','text')
                    ->group_by('module')
                    ->execute()
                    ->as_array('module','fullpath');

            } catch(Exception $e) {
                $binded = array();
            }

            foreach($modules as $_key => $_value) {
                if(isset($binded[$_key])) {
                    $modules[$_key]['fullpath'] = trim($binded[$_key], '/');
                } else {
                    $modules[$_key]['fullpath'] = false;
                }
            }

            $this->_loadedConfigurations = $modules;

            unset($binded);
            unset($modules);
        }

        if(!is_null($path)) {
            /** @noinspection PhpParamsInspection */
            return Arr::path( $this->_loadedConfigurations, $path, FALSE );
        }
        return $this->_loadedConfigurations;
    }
}
