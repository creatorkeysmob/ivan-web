<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Component_Settings
 */
class Component_Settings {
    /**
     * @var
     */
    protected $_vars;
    /**
     * @return array
     */
    public function getVars() {
        if (is_null($this->_vars)) {
            /** @noinspection PhpUndefinedClassInspection */
            $vars = ORM::factory('Settings')->find_all()->as_array('key', 'value');
            foreach($vars as $_key => $_value) {
                $data = json_decode($_value, true);
                if(!json_last_error()) $vars[$_key] = $data;
                if($_key == 'flamp') $vars[$_key] = htmlspecialchars_decode($_value);
            }
            $this->_vars = $vars;
        }
        return $this->_vars;
    }
}