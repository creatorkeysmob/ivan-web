<?php defined('SYSPATH') OR die('No direct script access.');

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404 {

    /**
     * Generate a Response for the 404 Exception.
     * The user should be shown a nice 404 page.
     * @return Response
     */
    public function get_response()
    {
        /** @var $settingsManager Component_Settings */
        $settingsManager = Registry::get(Registry::settingsManager);

        $vars  = $settingsManager->getVars();
        $admin = ( $_COOKIE['wa_admin'] == 1 && $vars['life_edit'] ) ? true : false;

        $document= ORM::factory('Pages')
            ->where('module', '=', '404')
            ->find();

        $template = Twig::factory( 'Errors/404',
            array(
                'css'          => '/assets/public/css',
                'js'           => '/assets/public/js',
                'document'      => $document,
                'message'       => $this->getMessage(),
                'vars'          => $vars,
                'menu'          => Widget::getMenu(1,2),
                'bottommenu'    => Widget::getMenu(1,1,'bottomshow'),
                'admin'         => $admin,
                'wa_admin'      => $_COOKIE['wa_admin'],
            )
        );

        $response = Response::factory()
            ->status(404)
            ->body($template);

        return $response;
    }
}
