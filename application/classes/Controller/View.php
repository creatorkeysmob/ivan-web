<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Controller_View
 */
class Controller_View extends Controller_Template {
    /* Template types */
    const PLAIN_TEXT_TYPE = 1;
    const PHP_TEMPLATE_TYPE = 2;
    const TWIG_TEMPLATE_TYPE = 3;

    /**
     * Template variables
     * @var array
     */
    protected $_template_variables = array();

    public function before(){
        $this->auto_render = FALSE;
        parent::before();
    }

    public function after() {
        parent::after();
    }

    /**
     * Add template variable
     * @param $key
     * @param $value
     * @return $this
     */
    public function addTemplateVariable( $key, $value ) {
        Arr::set_path($this->_template_variables,$key,$value);
        return $this;
    }

    /**
     * Init template variables array
     */
    public function loadTemplateVariables() {
        return $this->getTemplateVariables();
    }

    /**
     * @return array
     */
    public function getTemplateVariables() {
        return $this->_template_variables;
    }

    /**
     * @param array $template_variables
     */
    public function setTemplateVariables( $template_variables ) {
        $this->_template_variables = $template_variables;
    }

    /**
     * Remove template variable
     * @param $key
     * @return $this
     */
    public function deleteTemplateVariable( $key ) {
        if( isset($this->_template_variables[$key]) ) {
            unset($this->_template_variables[$key]);
        }
        return $this;
    }

    /**
     * Template render method
     * @param $view
     * @param array $params
     * @param int $template_type
     */
    public function render( $view, $params = array(), $template_type = self::TWIG_TEMPLATE_TYPE ) {
        $this->beforeRender();
        $params = Arr::merge($params, $this->loadTemplateVariables());
        switch($template_type) {
            case self::PLAIN_TEXT_TYPE:
                $template = $view;
                break;

            case self::PHP_TEMPLATE_TYPE:
                $template = View::factory( $view, $params );
                break;

            case self::TWIG_TEMPLATE_TYPE:
            default:
                $template = Twig::factory( $view, $params );
                break;
        }

        $this->response->body( $template );
        $this->afterRender();
    }

    public function beforeRender(){ }
    public function afterRender(){ }
}
