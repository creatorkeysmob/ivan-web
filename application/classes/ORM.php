<?php defined('SYSPATH') OR die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
class ORM extends Kohana_ORM {
    public function table_name($prefix = FALSE)
    {
        if($prefix) {
            return Database::instance()->table_prefix().$this->_table_name;
        }
        return $this->_table_name;
    }

    public function list_columns()
    {
        if(Kohana::$environment == Kohana::PRODUCTION) {
            if ( Cache::instance()->get( 'table_columns_' . $this->_object_name ) ) {
                return Cache::instance()->get( 'table_columns_' . $this->_object_name );
            }

            $table_columns = $this->_db->list_columns( $this->table_name() );
            Cache::instance()->set( 'table_columns_' . $this->_object_name, $table_columns );
            return $table_columns;
        }

        // Proxy to database
        return $this->_db->list_columns( $this->table_name() );
    }
}
