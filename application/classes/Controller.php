<?php defined('SYSPATH') OR die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
abstract class Controller extends Kohana_Controller {
    /**
     * Issues a HTTP redirect.
     * Proxies to the [HTTP::redirect] method.
     * @param  string  $uri   URI to redirect to
     * @param  int     $code  HTTP Status code to use for the redirect
     * @throws HTTP_Exception
     */
    public static function redirect($uri = '', $code = 302)
    {
        if( strpos($uri, '/') !== 0 ) {
            $uri = '/' . $uri;
        }
        return HTTP::redirect($uri, $code);
    }
}
