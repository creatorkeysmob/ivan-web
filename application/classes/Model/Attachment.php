<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Model_Attachment.
* @property int id
* @property string head
* @property string img
* @property int priority
* @property int hide
*/
class Model_Attachment extends Model_Abstract {
    protected $_table_name = 'feedback_attachment';

    public function save(Validation $validation = NULL)
    {
        return parent::save($validation);
    }
}
