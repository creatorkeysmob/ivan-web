<?php defined('SYSPATH') OR die('No direct script access.');

class Model_Formconstruct extends Model_Abstract {
    protected $_table_name = 'formconstruct';

    const FORMCONSTRUCT_MODEL            = 'Admin/Formconstruct/Model';
    const FORMCONSTRUCT_FORMADMIN        = 'Admin/Formconstruct/FormAdmin';
    const FORMCONSTRUCT_CONTROLLERADMIN  = 'Admin/Formconstruct/ControllerAdmin';

    public function save(Validation $validation = NULL)
    {

        /*** очистка файлового кеша ***/

            $cashe = APPPATH.'cache/';
            $dir   = scandir($cashe);
            foreach ($dir as $key => $value) {
                if ( preg_match('!^\w{2}$!', $value) ){
                    foreach ( glob($cashe.$value.'/*') as $filename ) {
                        unlink($filename);
                    }
                    rmdir($cashe.$value);
                }
            }

        /******************************/

        $table    = $_POST['alias'];
        $title    = $_POST['head'];

        // если нет названия поля, то генерим его транслитируя имя
        if( $table == '' ){
            $table = trim(Helper_Text::translit(mb_strtolower($title)));
            $table = preg_replace('/[^a-z0-9_]+/', '', $table);
        }

        $name_big = ucfirst($table);

        // проверка на наличие такого
        $module = ORM::factory('formconstruct')
            ->where('alias', 'REGEXP', $table.'[0-9]*')
            ->find_all()
            ->as_array();

        // если есть, то даем ему после название порядковый номер
        if ( count($module) > 0 ) {
            $table    = $table.count($module);
            $name_big = $name_big.count($module);
        }


        // создадим табличку
        $create =  "CREATE TABLE IF NOT EXISTS `wa_feedback_$table` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `text` text NOT NULL,
                        `date` datetime NOT NULL,
                        PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        Database::instance()->query(NULL, $create);

        // массив путей с шаблонами и куда что создавать
        $file_arr = array(
            0 => array(
                'path' => APPPATH."classes/Model/",
                'file' => $name_big.".php",
                'tmpl' => self::FORMCONSTRUCT_MODEL,
            ),
            1 => array(
                'path' => MODPATH."admin/classes/Admin/Controller/",
                'file' => $name_big.".php",
                'tmpl' => self::FORMCONSTRUCT_CONTROLLERADMIN,
            ),
            2 => array(
                'path' => MODPATH."admin/classes/Admin/Form/",
                'file' => $name_big.".php",
                'tmpl' => self::FORMCONSTRUCT_FORMADMIN,
            ),
        );

        $name   = array('title' => $title, 'name_small' => $table, 'name_big' => $name_big);

        // пробегаем, создаём всё что надо
        foreach ($file_arr as $key => $file) {
            if ( !file_exists($file['path']) ) {
                mkdir($file['path']);
            }
            if ( !file_exists($file['path'].$file['file']) ) {
                $body = Twig::factory($file['tmpl'], array('data' => $name))->render();
                file_put_contents($file['path'].$file['file'], trim($body));
            }
        }

        // дописываем конфигурационный файл админки
        $main    = Kohana::$config->load('admin/main');
        $modules = Kohana::$config->load('admin/main.modules');
        $menu    = Kohana::$config->load('admin/main.menu');

        $modules['admin'][$table] = array(
            'name'       => $table,
            'controller' => $name_big,
        );
        if ( !$menu['msg'] ){
            $menu['msg'] = array(
                'name'  => 'Сообщения',
                'icon'  => 'plus',
                'items' => array(),
            );
        }
        $menu['msg']['items'][$table] = array(
            'name'       => $title,
            'controller' => $name_big,
            'icon'       => 'arrow-right',
        );

        $main->set('modules',  $modules);
        $main->set('menu',     $menu);

        /**********************************/

        // сохраним основные параметры для дальнейшей работы
        // $data = array(
        //     'head'  => $title,
        //     'alias' => $name_big,
        // );
        //
        // $result = DB::insert('formconstruct')
        //     ->columns(array_keys($data))
        //     ->values(array_values($data))
        //     ->execute($this->_db);

        return parent::save($validation);

        // редиркетим к списку, так как редактировать ничо нельзя
        header("Refresh:0; url=/admin/formconstruct/list");

    }
}
