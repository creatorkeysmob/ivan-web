<?php defined('SYSPATH') OR die('No direct access allowed.');

/** @noinspection PhpUndefinedClassInspection */
class Model_User extends Model_Auth_User {
    // Roles constant
    const ROLE_LOGIN = 1;
    const ROLE_ADMIN = 2;

    // Type constant
    const TYPE_USER = 1;
    const TYPE_ADMIN = 2;

    protected $_roles = array();
    /**
     * @return bool
     */
    public function is_root(){
        return ($this->has_role(self::ROLE_ADMIN)) ? TRUE : FALSE;
    }

    /**
     * @param int $role
     * @return bool
     */
    public function has_role( $role ){
        if(Auth::instance()->logged_in()){
            if(!$this->_roles) {
                /** @noinspection PhpUndefinedFieldInspection */
                $this->_roles = $this->roles->find_all()->as_array(null,'id');
            }

            if(in_array($role,$this->_roles)) return TRUE;
        }
        return FALSE;
    }



    // Check for unique
    public function is_unique($login,$email) {
        $errors = array();
        $query = DB::select('id','username', 'email', 'exclude')->from($this->table_name())
            ->and_where_open()
            ->where('username', '=', $login)
            ->or_where('email', '=', $email)
            ->and_where_close();

        if( !empty($this->id) ) {
            $query->where('id', '<>', $this->id);
        }

        $user = $query->execute()->current();
        if($user) {
            if($user['exclude']) {
                if( $user['username'] == $login ) $errors['username'] = 'Пользователь с таким логином не может быть зарегистрирован';
                if( $user['email'] == $email ) $errors['email'] = 'Пользователь с такой эл. почтой не может быть зарегистрирован';
            } else {
                if( $user['username'] == $login ) $errors['username'] = 'Пользователь с таким логином уже зарегистрирован';
                if( $user['email'] == $email ) $errors['email'] = 'Пользователь с такой эл. почтой уже зарегистрирован';
            }
        }

        return $errors;
    }
}