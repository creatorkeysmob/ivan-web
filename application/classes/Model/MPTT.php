<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Class Model_MPTT
 * @property int lft
 * @property int rgt
 * @property int lvl
 * @property int scope
 */
class Model_MPTT extends Kohana_Model_MPTT {
    /**
     * @var
     */
    protected $_title_key = 'head';

    /**
     * @param mixed $title_key
     */
    public function setTitleKey($title_key) {
        $this->_title_key = $title_key;
    }

    /**
     * @return mixed
     */
    public function getTitleKey() {
        return $this->_title_key;
    }

    /**
     * Return model title
     * @return mixed|null
     */
    public function getTitleValue() {
        if($this->loaded() && isset($this->{$this->_title_key}) ) {
            return $this->{$this->_title_key};
        }
        return NULL;
    }
}