<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Abstract
 * @property mixed hide
 * @property int priority
 */
class Model_Abstract extends ORM {
    /**
     * @var
     */
    protected $_title_key = 'head';

    /**
     * @param mixed $title_key
     */
    public function setTitleKey($title_key) {
        $this->_title_key = $title_key;
    }

    /**
     * @return mixed
     */
    public function getTitleKey() {
        return $this->_title_key;
    }

    /**
     * Return model title
     * @return mixed|null
     */
    public function getTitleValue() {
        if($this->loaded() && isset($this->{$this->_title_key}) ) {
            return $this->{$this->_title_key};
        }
        return NULL;
    }

    /**
     * Init model parameters
     */
    public function init(){}

}