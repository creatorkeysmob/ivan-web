<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Creator extends Model_Abstract {
    protected $_table_name = 'creator';

    const CREATOR_MODEL            = 'Admin/Creator/Model';
    const CREATOR_FORMADMIN        = 'Admin/Creator/FormAdmin';
    const CREATOR_CONTROLLERADMIN  = 'Admin/Creator/ControllerAdmin';
    const CREATOR_CONTROLLERPUBLIC = 'Admin/Creator/ControllerPublic';
    const CREATOR_MAINMODULEADMIN  = 'Admin/Creator/MainModuleAdmin';
    const CREATOR_MAINMODULE       = 'Admin/Creator/MainModule';
    const CREATOR_MAINMODULELVL    = 'Admin/Creator/MainModuleLvl';
    const CREATOR_TMPLMAINMODULE   = 'Admin/Creator/Tmpl/TmplMainModule';
    const CREATOR_TMPLINDEX        = 'Admin/Creator/Tmpl/TmplIndex';
    const CREATOR_TMPLLIST         = 'Admin/Creator/Tmpl/TmplList';
    const CREATOR_INIT             = 'Admin/Creator/Init';

    public function save(Validation $validation = NULL) {
        // var_dump($_POST);exit;
        /*** очистка файлового кеша ***/

            $cashe = APPPATH.'cache/';
            $dir   = scandir($cashe);
            foreach ($dir as $key => $value) {
                if ( preg_match('!^\w{2}$!', $value) ){
                    foreach ( glob($cashe.$value.'/*') as $filename ) {
                        unlink($filename);
                    }
                    rmdir($cashe.$value);
                }
            }

        /******************************/

        $title       = $_POST['head'];
        $name_small  = preg_replace('/[\-]+/', '', strtolower($_POST['alias']));
        // если нет названия поля, то генерим его транслитируя имя
        if( $name_small == '' ){
            $name_small = trim(Helper_Text::translit(mb_strtolower($title)));
            $name_small = preg_replace('/[^a-z0-9_]+/', '', $name_small);
        }
        $name_big    = ucfirst($name_small);
        $lvl         = $_POST['lvl'];
        $img         = $_POST['img'];
        $main_module = $_POST['main'];
        $search      = $_POST['search'];
        $main_count  = $main_count= $_POST['main_count'];
        $tmpl        = $_POST['tmpl'];



        // проверка на наличие такого
        $module = ORM::factory('creator')
            ->where('alias', 'REGEXP', $name_big.'[0-9]*')
            ->find_all()
            ->as_array();

        // если есть, то даем ему после название порядковый номер
        if ( count($module) > 0 ) {
            $name_small = $name_small.count($module);
            $name_big   = $name_big.count($module);
        }


        $name   = array('title' => $title, 'name_small' => $name_small, 'name_big' => $name_big);
        $count  = count($_POST['fields']);
        $sql    = "";
        $fields = array();
        // собираем массив параметров модуля
        for ( $i=0; $i <= ($count-1); $i++ ) {
            if ( $_POST['fields_1'][$i] != '' ){
                // если нет названия поля, то генерим его транслитируя имя
                if( $_POST['fields_2'][$i] == '' ){
                    $_POST['fields_2'][$i] = trim(Helper_Text::translit(mb_strtolower($_POST['fields_1'][$i])));
                    $_POST['fields_2'][$i] = preg_replace('/[^a-z0-9_]+/', '', $_POST['fields_2'][$i]);
                }
                // дефолтное значение параметра
                if( $_POST['fields_3'][$i] === 0 || $_POST['fields_3'][$i] === '0' ) $_POST['fields_3'][$i] = 'text;varchar(255)';

                $type2type = explode(';', $_POST['fields_3'][$i]);

                $sql .= "`".$_POST['fields_2'][$i]."` ".$type2type[1].",";
                $fields[] = array(
                    'title'    => $_POST['fields_1'][$i],
                    'name'     => $_POST['fields_2'][$i],
                    'function' => $type2type[0],
                    'type'     => $type2type[1],
                );
            }

        }
        // если есть что доабвлять, то доабвляем в общий массив с параметрами
        if ( count($fields) > 0 ) {
            $name['fields'] = $fields;
        }

        // если у модуля есть подстраницы, то надо что бы они были в карте сайта
        if ( $lvl ) {
            $sql .= "`sitemapshow` tinyint(1) DEFAULT '1',";
            $sql .= "`alias` varchar(255),";
            $sql .= "`title` varchar(255),";
            $sql .= "`description` text,";
            $sql .= "`keywords` text,";
            
            $name['lvl'] = true;
        }
        // если у модуля есть основное изображение
        if ( $img ) {
            $sql .= "`img` varchar(255),";
            $name['img'] = true;
        }
        // если у модуля есть возможность добавления на главную
        if ( $main_module ) {
            $name['main'] = true;
            $name['tmpl'] = $tmpl;
        }
        // количество элементова на главной
        if ( $main_count && $main_count > 0 ) $name['count'] = $main_count;
        // если у модуля есть поисковик
        if ( $search ) {
            $name['search'] = $search;
            // добавим в конфиг поиска
            $search_conf = Kohana::$config->load('public/modules/search');
            $models      = Kohana::$config->load('public/modules/search.models');
            $models[]    = $name_big;
            $search_conf->set('models', $models);
        }

        // создадим табличку с дефолтными и собранными параметрами
        $create =  "CREATE TABLE IF NOT EXISTS `wa_$name_small` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `head` varchar(255) NOT NULL,
                        `alias` varchar(255) NOT NULL,
                        `body` text,
                        $sql
                        `priority` int(11) NOT NULL,
                        `hide` tinyint(3) NOT NULL DEFAULT '0',
                        PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        Database::instance()->query(NULL, $create);

        // массив путей с шаблонами и куда что создавать
        $file_arr = array(
            0 => array(
                'path' => APPPATH."classes/Model/",
                'file' => $name_big.".php",
                'tmpl' => self::CREATOR_MODEL,
            ),
            1 => array(
                'path' => MODPATH."admin/classes/Admin/Controller/",
                'file' => $name_big.".php",
                'tmpl' => self::CREATOR_CONTROLLERADMIN,
            ),
            2 => array(
                'path' => MODPATH."admin/classes/Admin/Form/",
                'file' => $name_big.".php",
                'tmpl' => self::CREATOR_FORMADMIN,
            ),
            3 => array(
                'path' => MODPATH."public/classes/Public/Controller/",
                'file' => $name_big.".php",
                'tmpl' => self::CREATOR_CONTROLLERPUBLIC,
            ),
            4 => array(
                'path' => MODPATH."public/views/".$name_big."/",
                'file' => "Index.twig",
                'tmpl' => self::CREATOR_TMPLINDEX,
            ),
            5 => array(
                'path' => MODPATH."public/views/".$name_big."/",
                'file' => 'List.twig',
                'tmpl' => self::CREATOR_TMPLLIST,
            ),
        );

        // если есть возможность добавления модуля на главную
        if ( $main_module ) {
            $file_arr[] = array(
                'path' => MODPATH."public/views/MainModules/",
                'file' => $name_big.".twig",
                'tmpl' => self::CREATOR_TMPLMAINMODULE,
            );
        }

        // пробегаем, создаём всё что надо
        foreach ($file_arr as $key => $file) {
            if ( !file_exists($file['path']) ) {
                mkdir($file['path']);
            }
            if ( !file_exists($file['path'].$file['file']) ) {
                $body = Twig::factory($file['tmpl'], array('data' => $name))->render();
                file_put_contents($file['path'].$file['file'], trim($body));
            }
        }

        // аналогичное для модулей на главной
        if ( $main_module ) {
            $modules_arr = Twig::factory(self::CREATOR_MAINMODULEADMIN, array('data' => $name))->render();
            if( $lvl ){
                $main_controller = Twig::factory(self::CREATOR_MAINMODULELVL, array('data' => $name))->render();
            } else {
                $main_controller = Twig::factory(self::CREATOR_MAINMODULE, array('data' => $name))->render();
            }

            $modules_form_file    = MODPATH.'admin/classes/Admin/Form/Modules.php';
            $main_controller_file = MODPATH.'public/classes/Public/Controller/Main.php';

            $main_array = array(
                0 => array(
                    'file'    => $modules_form_file,
                    'content' => $modules_arr,
                ),
                1 => array(
                    'file'    => $main_controller_file,
                    'content' => $main_controller,
                ),
            );

            foreach ($main_array as $param) {
                $file_read   = file_get_contents($param['file']);
                $file_arr    = explode('#################################', $file_read);
                $file_arr[1] = $param['content'];
                $file_arr[1] = trim($file_arr[1]);
                file_put_contents($param['file'], implode($file_arr));
            }
        }


        // дописывание в инит для роутинга
        $init        = MODPATH.'public/init/init_creator.php';
        $init_read   = file_get_contents($init);
        $init_arr    = explode('#################################', $init_read);
        $init_arr[1] = Twig::factory(self::CREATOR_INIT, array('data' => $name))->render();
        $init_arr[1] = trim($init_arr[1]);
        file_put_contents($init, implode($init_arr));

        // дописываем конфигурационный файл админки
        $main    = Kohana::$config->load('admin/main');
        $modules = Kohana::$config->load('admin/main.modules');
        $menu    = Kohana::$config->load('admin/main.menu');

        $modules['admin'][$name_small] = array(
            'name'       => $title,
            'controller' => $name_big,
        );
        $modules['public'][$name_small] = array(
            'name'       => $title,
            'controller' => $name_big,
        );
        if ( !$menu['creator'] ){
            $menu['creator'] = array(
                'name'  => 'Добавленные',
                'icon'  => 'plus',
                'items' => array(),
            );
        }
        $menu['creator']['items'][$name_small] = array(
            'name'       => $title,
            'controller' => $name_big,
            'icon'       => 'plus',
        );
        if ( $lvl ) {
            $sitemap    = Kohana::$config->load('admin/main.sitemap');
            $sitemap[]  = $name_small;
            $main->set('sitemap', $sitemap);
        }

        $main->set('modules',  $modules);
        $main->set('menu',     $menu);

        /**********************************/

        // сохраним основные параметры для дальнейшей работы
        $data = array(
            'head'   => $title,
            'alias'  => $name_big,
            'lvl'    => $lvl,
            'img'    => $img,
            'main'   => $main_module,
            'search' => $search,
        );

        $result = DB::insert('creator')
            ->columns(array_keys($data))
            ->values(array_values($data))
            ->execute($this->_db);

        // редиркетим к списку, так как редактировать ничо нельзя
        header("Refresh:0; url=/admin/creator/list");
        // можно редиректить сразу в созданный модуль
        //header("Refresh:0; url=/admin/$name_small/list");

    }
}
