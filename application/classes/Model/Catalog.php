<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Model_Catalog.
* @property int id
* @property string alias
* @property string head
* @property string title
* @property string who
* @property string body
* @property string created
* @property string description
* @property string keywords
* @property int priority
* @property int hide
*/
class Model_Catalog extends Model_Abstract {
    protected $_table_name = 'catalog';

    /**
     * Generate alias
     * @return string
     */
    protected function _alias()
    {
        $alias = '';
        if ($this->head OR $this->alias) {
            if ($this->alias) {
                $alias = $this->alias;
            } elseif ($this->head) {
                $alias = $this->head;
            }

            // Generate unique
            $alias = trim(Helper_Text::translit(mb_strtolower($alias)));
            //$alias = substr($alias, 0, 60);
            $alias = preg_replace('/[^a-z0-9_\-]+/', '', $alias);

            $exist = DB::select()->from($this->table_name())
                ->where('alias', '=', $alias)
                ->and_where('id', '<>', intval($this->id))
                ->execute()->count();

            while ($exist) {
                $postfix = substr(md5(uniqid(rand(), 1)), 0, 10);
                $exist = DB::select()->from($this->table_name())
                    ->where('alias', '=', $alias . '_' . $postfix)
                    ->and_where('id', '<>', intval($this->id))
                    ->execute()->count();

                if (!$exist) {
                    $alias .= '_' . $postfix;
                }
            }
        }

        return $alias;
    }

    /**
     * Get priority
     * @return int
     */
    protected function _priority() {
        $priority = DB::select(array(DB::expr('MAX(priority)'),'priority'))
            ->from($this->_table_name)
            ->execute()
            ->get('priority');

        return intval($priority) + 1;
    }

    public function save(Validation $validation = NULL)
    {
        $this->alias = $this->_alias();
        if(empty($this->priority)) {
            $this->priority = $this->_priority();
        }

        return parent::save($validation);
    }

    public function search($query = NULL, $limit = 999999999)
    {
        $items = array();
        if (!is_null($query)) {
            // Search items
            $items = DB::query(Database::SELECT,
                'SELECT `id`, `head`, `body`, `alias`, ' .
                'MATCH (`head`, `body`, `industry`, `functions`, `product`) AGAINST (:query) AS `score` ' .
                'FROM :table WHERE MATCH (`head`, `body`, `industry`, `functions`, `product`) AGAINST (:query IN BOOLEAN MODE) '.
                'ORDER BY `score` DESC '.
                'LIMIT '.$limit
            )->parameters(
                array(
                    ':table' => DB::expr('wa_catalog'),
                    ':query' => $query
                )
            )->execute()->as_array();

            // Process fullpath and module
            $moduleManager = Registry::get(Registry::moduleManager);
            $fullpath = $moduleManager->getConfig('catalog.fullpath');

            $items = array_map(
                function($element) use($fullpath) {
                    $element['module'] = 'catalog';
                    if($fullpath) {
                        $element['fullpath'] = '/'.$fullpath.'/'.$element['alias'];
                    } else {
                        $element['fullpath'] = '#';
                    }
                    return $element;
                },
                $items
            );
        }
        return $items;
    }


}
