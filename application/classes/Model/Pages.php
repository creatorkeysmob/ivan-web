<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Model_Pages
 * @property int id
 * @property int parent_id
 * @property mixed section_id
 * @property string alias
 * @property string head
 * @property string title
 * @property string body
 * @property int menushow
 * @property int bottomshow
 * @property string redirect
 * @property mixed created
 * @property string description
 * @property string keywords
 * @property string module
 * @property int priority
 * @property int hide
 * @property mixed fullpath
 */

class Model_Pages extends Model_MPTT {
    protected $_table_name = 'pages';

    /**
     * Generate alias
     * @return string
     */
    protected function _alias() {
        $alias = '';
        if($this->head OR $this->alias) {
            if($this->alias) {
                $alias = $this->alias;
            } elseif($this->head) {
                $alias = $this->head;
            }

            // Generate unique
            $alias = trim( Helper_Text::translit( mb_strtolower($alias) ) );
           // $alias = substr( $alias, 0, 60 );
            $alias = preg_replace('/[^a-z0-9_\-]+/', '', $alias);

            $exist = DB::select()->from($this->table_name())
                ->where('alias', '=', $alias)
                ->and_where('id', '<>', intval($this->id))
                ->and_where('parent_id', '=', intval($this->parent_id))
                ->execute()->count();

            while ($exist) {
                $postfix = substr(md5(uniqid(rand(),1)), 0, 10);
                $exist = DB::select()->from($this->table_name())
                    ->where('alias', '=', $alias.'-'.$postfix)
                    ->and_where('id', '<>', intval($this->id))
                    ->and_where('parent_id', '=', intval($this->parent_id))
                    ->execute()->count();

                if(!$exist) {
                    $alias .= '-' . $postfix;
                }
            }
        }
        return $alias;
    }

    /**
     * Generate fullpath
     * @return string
     */
    protected function _fullpath() {
        $fullpath = '';
        if($this->alias) {
            $parents = $this->parents(FALSE)->find_all();
            if(sizeof($parents)) {
                /** @var $_parent Model_Pages */
                foreach($parents as $_parent) {
                    $fullpath .= '/' . $_parent->alias;
                }
            }
            $fullpath .= '/' . $this->alias;
            unset($parents);
        }
        return $fullpath;
    }

    /**
     * Get priority
     * @return int
     */
    protected function _priority() {
        $priority = DB::select(array(DB::expr('MAX(priority)'),'priority'))
            ->from($this->_table_name)
            ->where('parent_id','=',$this->parent_id)
            ->execute()->get('priority');
        return intval($priority) + 1;
    }

    /**
     * @param Validation $validation
     * @return ORM
     */
    public function save(Validation $validation = NULL)
    {

        if(empty($this->module)){
            $this->module = 'text';
        }

        if(empty($this->priority)) {
            $this->priority = $this->_priority();
        }

        $this->alias = $this->_alias();
        $this->fullpath = $this->_fullpath();

        return parent::save($validation);
    }

    public function search($query = NULL)
    {
        $items = array();
        if (!is_null($query)) {
            $items = DB::query(Database::SELECT,
                'SELECT `id`, `head`, `body`, `module`, `fullpath`, ' .
                'MATCH (`head`, `body`) AGAINST (:query) AS `score` ' .
                'FROM :table WHERE MATCH (`head`, `body`) AGAINST (:query IN BOOLEAN MODE) '.
                'ORDER BY `score` DESC'
            )->parameters(
                array(
                   ':table' => DB::expr($this->table_name(TRUE)),
                   ':query' => $query
                )
            )->execute()->as_array();
        }
        return $items;
    }
}
