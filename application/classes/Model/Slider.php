<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Model_Slider.
* @property int id
* @property string head
* @property string subhead
* @property string img
* @property string img_fon
* @property int priority
* @property int hide
*/
class Model_Slider extends Model_Abstract {
    protected $_table_name = 'slider';
    /**
     * Get priority
     * @return int
     */
    protected function _priority() {
        $priority = DB::select(array(DB::expr('MAX(priority)'),'priority'))
            ->from($this->_table_name)
            ->execute()
            ->get('priority');

        return intval($priority) + 1;
    }

    public function save(Validation $validation = NULL)
    {
        if(empty($this->priority)) {
            $this->priority = $this->_priority();
        }
        return parent::save($validation);
    }
}
