<?php defined('SYSPATH') OR die('No direct script access.');

class Model_About extends Model_Abstract {
    protected $_table_name = 'about';

    /**
     * Get priority
     * @return int
     */
    protected function _priority() {
        $priority = DB::select(array(DB::expr('MAX(priority)'),'priority'))
            ->from($this->_table_name)
            ->execute()
            ->get('priority');

        return intval($priority) + 1;
    }

}
