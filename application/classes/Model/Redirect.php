<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Model_Redirect.
* @property int id
* @property string from
* @property string to
* @property int hide
*/
class Model_Redirect extends Model_Abstract {
    protected $_title_key = 'from';
    protected $_table_name = 'redirect';

    /**
     * @param Validation $validation
     * @return ORM
     */
    public function save(Validation $validation = NULL)
    {
        $this->from = Helper_Text::uri($this->from);
        $this->to = Helper_Text::uri($this->to);

        if($this->from !== $this->to) {
            return parent::save($validation);
        }
        return false;
    }
}
