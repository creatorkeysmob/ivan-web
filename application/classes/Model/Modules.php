<?php defined('SYSPATH') OR die('No direct script access.');

class Model_Modules extends Model_Abstract {
    protected $_table_name = 'modules';

    /**
     * Get priority
     * @return int
     */
    protected function _priority() {
        $priority = DB::select(array(DB::expr('MAX(priority)'),'priority'))
            ->from($this->_table_name)
            ->execute()
            ->get('priority');

        return intval($priority) + 1;
    }

    public function save(Validation $validation = NULL)
    {
        return parent::save($validation);
    }

}
