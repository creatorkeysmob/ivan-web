<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Model_Settings
 * @property int id
 * @property string key
 * @property string value
 */

class Model_Settings extends Model_Abstract {
    protected $_table_name = 'settings';
}