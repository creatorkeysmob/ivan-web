/**
 * Class Model_Thumb
 вызывается слудующим образом
 Model_Thumb::factory('путь до изменяемой картинки', $config)->render()
 В Шаблонизаторе так:
 {{thumb('/upload/images/2013/08/26/474749-1440x900.jpg',{'actions':['resize_320_320'],'attributes':{'class':'class1'}})}}
 * $config array - набор параметров для изменения изображения.
 * ['actions']:{
        'resize_320_320', // ресайз 320x320
        'crop_(crop width)_(crop_height)_(offset_y)_(offset_y),
        'watermark_(offset x)_(offset y)_(opacity), //путь до картинки ватермарка указывается в атрибутах
}
 * ['attributes']=>array('alt',
 *                       'title',
 *                       'class',
                         'image'=>'путь до изображения ватермарка'
 *                       'id',
 *                       'link'=>1 флаг: возвращать ли ссылку на изображение),
 *      // параметры изображения
 *------------------------------------------------------------------
 */