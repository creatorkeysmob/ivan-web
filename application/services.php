<?php defined('SYSPATH') OR die('No direct script access.');

$container = new Pimple();

// Load components
$container['document'] = $container->share(function($c) { return new Component_Document(); });
$container['module'] = $container->share(function($c) { return new Component_Module(); });
$container['settings'] = $container->share(function($c) { return new Component_Settings(); });

// Save container into registry
Registry::set(Registry::standartContainer, $container);