<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'default' => array(
        'driver'       => 'ORM',
        'hash_method'  => 'sha256',
        'hash_key'     => 344170077,
        'lifetime'     => 1209600,
        'session_type' => Session::$default,
        'session_key'  => 'auth_user',
        'models'       => array(
            'user'  => 'User',
            'role'  => 'Role',
            'token' => 'User_Token'
        )
    )
);