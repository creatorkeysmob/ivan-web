<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Modified Preorder Tree Traversal Class.
 *
 * @author     Kiall Mac Innes
 * @author     Mathew Davies
 * @author     Mike Parkin
 * @copyright  (c) 2008-2010
 * @package    ORM_MPTT
 * @property int lft
 * @property int rgt
 * @property int lvl
 * @property int scope
 */
class Model_MPTT extends Kohana_Model_MPTT { }
