<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Kohana_Breadcrumb
 */
class Kohana_Breadcrumb {
    protected static $_instances = array();
    protected $_items = array();

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    /**
     * @param string $group
     * @return Kohana_Breadcrumb
     */
    public static function instance($group = 'default')
    {
        if( !isset(self::$_instances[$group]) OR self::$_instances[$group] === null) {
            self::$_instances[$group] = new Kohana_Breadcrumb();
        }
        return self::$_instances[$group];
    }

    /**
     * @param $label
     * @param $url
     * @param bool $enable
     * @return $this
     */
    public function addItem($label, $url = null, $enable = true) {
        if( strpos($url, '/') !== 0 ) {
            $url = '/' . $url;
        }

        $this->_items[] = array(
            'label' => $label,
            'url'   => $url,
            'enable' => $enable
        );
        return $this;
    }

    /**
     * @return array
     */
    public function getItems() {
        return $this->_items;
    }

    /**
     * @param $index
     * @return $this
     */
    public function removeItem( $index ) {
        if( isset($this->_items[$index]) ) {
            unset($this->_items[$index]);
        }
        return $this;
    }

    /**
     * @param $index
     * @param array $params
     */
    public function updateItem( $index, $params = array() ){
        if(isset($this->_items[$index]) && $params) {
            $this->_items[$index] = array_merge($this->_items[$index], $params);
        }
    }

}