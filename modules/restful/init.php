<?php defined('SYSPATH') or die('No direct access allowed.');

/** @noinspection PhpParamsInspection, PhpUndefinedClassInspection */
Route::set('api', 'api(/<controller>.<action>)')
    ->filter(
        function( $route, $params, $request ) {
            if(isset($params['controller'])) {
                $nameParts = explode('_', $params['controller']);
                $nameParts = array_map('ucfirst', $nameParts);
                $className = 'Api_'.implode('_', $nameParts);

                if(class_exists($className)) {
                    $classMethods = get_class_methods($className);
                    if(!in_array('action_'.$params['action'], $classMethods)) {
                        $params['controller'] = 'Api';
                    } else {
                        $params['prefix'] = 'Api';
                    }
                    unset($classMethods);
                }
            }
            return $params;
        }
    )
    ->defaults(
        array(
            'controller' => 'Api',
            'action' => 'success'
        )
    );


/** @noinspection PhpParamsInspection, PhpUndefinedClassInspection */
Route::set('rest', 'rest/<controller>(<id>)')
    ->filter(
        function( $route, $params, $request ) {
            if(isset($params['controller'])) {
                $nameParts = explode('_', $params['controller']);
                $nameParts = array_map('ucfirst', $nameParts);
                $className = 'Rest_'.implode('_', $nameParts);
                if(class_exists($className)) {
                    $params['prefix'] = 'Rest';
                } else {
                    $params['controller'] = 'Rest';
                }
                unset($nameParts);
            }
            return $params;
        }
    )
    ->defaults(
        array(
            'controller' => 'Rest',
            'action' => 'success'
        )
    );