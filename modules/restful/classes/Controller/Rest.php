<?php defined('SYSPATH') or die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
class Controller_Rest extends Controller {
    /**
     * @var array
     */
    protected $_outputResult = array(
        'status' => array(
            'code' => 200,
            'message' => 'OK'
        ),
        'data' => array(),
    );

    /**
     * @var string
     */
    protected $_outputFormat = 'json';

    /**
     * @var array
     */
    protected $_defaultErrorMessages = array(
        200 => 'OK',
        201  => 'Created',
        304  => 'Not Modified',
        400 => 'Bad Request',
        401  => 'Unauthorized',
        403  => 'Forbidden',
        404  => 'Not Found',
        500 => 'Internal server error',
    );

    /**
     * Default mapping
     * @var array
     */
    protected $_defaultMethodMapping = array(
        HTTP_REQUEST::GET => 'action_get',
        HTTP_REQUEST::POST => 'action_create',
        HTTP_REQUEST::PUT => 'action_update',
        HTTP_REQUEST::DELETE => 'action_delete'
    );

    /**
     * @var array
     */
    protected $_errorMessages = array();

    /**
     * Overrided mapping
     * @var array
     */
    protected $_methodMapping = array();

    /**
     * Current http method
     * @var
     */
    protected $_requestMethod = HTTP_REQUEST::GET;

    /**
     * @return mixed
     */
    public function getData(){
        return $this->_outputResult['data'];
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data){
        if(is_array($data)) {
            $this->_outputResult['data'] = $data;
        }
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function addData($data) {
        if ( is_array($data) ) {
            foreach($data as $_key => $_value) {
                $this->_outputResult['data'][$_key] = $_value;
            }
        }
        return $this;
    }

    /**
     * Set error status
     * @param $code
     * @param $message
     * @param bool $break
     */
    public function error($code, $message = NULL, $break = FALSE){
        $this->_outputResult['status']['code'] = $code;
        if(!is_null($message)) {
            $this->_outputResult['status']['message'] = $message;
        } else {
            $this->_outputResult['status']['message'] = Arr::get($this->_errorMessages, $code, 'Unknown error');
        }

        // Break function call
        if($break) {
            $this->action_error();
            $this->after();
            exit;
        }
    }

    /**
     * Set success status
     * @param bool $break
     */
    public function success($break = FALSE){
        $this->_outputResult['status']['code'] = 200;
        $this->_outputResult['status']['message'] = $this->_errorMessages[200];

        // Break function call
        if($break) {
            $this->action_success();
            $this->after();
            exit;
        }
    }

    /**
     * Before handler
     */
    public function before() {
        // Get method mapping
        foreach($this->_defaultMethodMapping as $_method => $_name) {
            if(!isset($this->_methodMapping[$_method])) {
                $this->_methodMapping[$_method] = $_name;
            }
        }

        // Get error messages
        foreach($this->_defaultErrorMessages as $_code => $_message) {
            if(!isset($this->_errorMessages[$_code])) {
                $this->_errorMessages[$_code] = $_message;
            }
        }

        // Get current http method
        $this->_requestMethod = $this->request->method();

        // Override current method
        if(isset($this->_methodMapping[$this->_requestMethod]) && !empty($this->_methodMapping[$this->_requestMethod]) ) {
             if(!method_exists($this, $this->_methodMapping[$this->_requestMethod])) {
                 $this->error(400, NULL, TRUE);
             }
        } else {
            $this->error(400, NULL, TRUE);
        }
    }

    /**
     *  Error handler
     */
    public function action_error() { }

    /**
     *  Success handler
     */
    public function action_success() { }

    /**
     *  After handler
     */
    public function after() {

        switch ($this->_outputFormat) {
            case 'array':
                echo '<pre>';
                print_r($this->_outputResult);
                echo '</pre>';
                break;

            case 'json':
            default:
                echo json_encode($this->_outputResult);
                break;
        }
    }
}