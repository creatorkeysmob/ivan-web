<?php defined('SYSPATH') or die('No direct access allowed.');

include 'init/init_main.php';
include 'init/init_page.php';


// Load custom modules
$moduleManager = Registry::get(Registry::moduleManager);

include 'init/init_creator.php';
include 'init/init_modules.php';

unset($moduleManager);

$LastModified_unix = strtotime(date("D, d M Y H:i:s", filectime($_SERVER['SCRIPT_FILENAME'])));
$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
$IfModifiedSince = false;
if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
    exit;
}

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));
header('Last-Modified: '. $LastModified);
