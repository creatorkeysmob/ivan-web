<?
/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
Route::set('public_main', '')
    ->filter(
        function( $route, $params, $request) {
            Registry::get(Registry::documentManager);

            $uri = Helper_Text::uri($request->uri());
            $query = $request->query();
            unset($query['page']);

            if($query) {
                $uri .= '?'. urldecode( http_build_query($query) );
            }

            /** @var $redirect Model_Redirect */
            $redirect = ORM::factory('Redirect')
                ->where('from','=', $uri)
                ->and_where('hide','=',0)
                ->order_by('id', 'DESC')
                ->find();

            if($redirect->loaded()) {
                header('Location: /'.$redirect->to);
                exit;
            }
            return $params;
        }
    )
    ->defaults(
        array(
            'controller' => 'Main',
            'action'     => 'index',
            'prefix'     => 'Public_Controller'
        )
    );
