<?
/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
Route::set('public_page', '(<fullpath>)',
    array(
        'fullpath' => '.*'
    )
)->filter(
        function( $route, $params, $request) {
            $uri = Helper_Text::uri($request->uri());
            $query = $request->query();
            unset($query['page']);

            if($query) {
                $uri .= '?'. urldecode( http_build_query($query) );
            }

            /** @var $redirect Model_Redirect */
            $redirect = ORM::factory('Redirect')
                ->where('from','=', $uri)
                ->and_where('hide','=',0)
                ->order_by('id', 'DESC')
                ->find();

            if($redirect->loaded()) {
                header('Location: /'.$redirect->to, true, 301);
                exit;
            }
            // Load inner page
            if(!empty($params['fullpath'])) {
                $modules = Kohana::$config->load('admin/main.modules.public');
                if( !$modules ) {
                    throw new Exception('Engine: modules does not exists');
                }

                $uriArray = explode('/', $request->uri());
                $uriArray = array_filter($uriArray);
                $uriArray = array_values($uriArray);

                if( !$uriArray ) {
                    return false;
                }

                // Load page candidates
                $candidates = ORM::factory('Pages')
                    ->where('alias','IN',$uriArray)
                    ->and_where('hide','=',0)
                    ->find_all();

                if( !$candidates->count() ) {
                    return false;
                }

                // Process candidates
                $processed = array();

                /** @var $_candidate Model_Pages */
                foreach ( $candidates as $_candidate ) {
                    $processed[$_candidate->alias][$_candidate->parent_id] = $_candidate;
                }
                unset($candidates);

                // Get page object
                $parentId      = 1;
                $matchesArray  = array();
                $fullpathArray = $uriArray;
                $modpathArray  = array();

                $document = NULL;

                for ($i = 0, $length = count($uriArray); $i < $length; $i++) {
                    $modpathArray[] = $uriArray[$i];
                    $matchesArray[] = $alias = array_shift($uriArray);
                    if( isset($processed[$alias][$parentId]) ) {

                        /** @var $document Model_Pages */
                        $document = $processed[$alias][$parentId];
                        $parentId = $document->id;

                        // Add breadcrumbs
                        Breadcrumb::instance()->addItem(
                            $document->head,
                            '/' . implode('/', array_slice($fullpathArray, 0, $i+1))
                        );

                        // If custom module
                        if( $document->module !== 'Text' && ( !$uriArray OR !isset($processed[$uriArray[0]][$parentId])) ) {
                            $matchesArray = array_values($uriArray);
                            break;
                        }
                    } else {
                        return false;
                    }
                }

                /** @var $documentManager Component_Document */
                $documentManager = Registry::get(Registry::documentManager);
                $documentManager->setModel($document);
                $documentManager->setMatches( $matchesArray );
                $documentManager->setModule( $document->module );
                $documentManager->setFullpath( '/'. implode('/',$modpathArray) );
                $documentManager->setUri( '/'. implode('/',$fullpathArray) );

                // Save current document instance
                Registry::set(Registry::documentManager, $documentManager);

                // Clear memory
                unset($standartContainer);
                unset($documentManager);
                unset($fullpathArray);
                unset($matchesArray);
                unset($modpathArray);

                if( isset($modules[$document->module]['controller']) ) {
                    $params['controller'] = $modules[$document->module]['controller'];
                    $page_modules = array('Projects', 'About');
                    if ( in_array($modules[$document->module]['controller'], $page_modules) ) return $params;
                } else {
                    // Module not found
                    return false;
                }

                // Only text pages
                if($params['controller'] && $params['controller'] !== 'Text') {
                    return false;
                }
            }
            return $params;
        }
    )->defaults(
        array(
            'controller' => 'Text',
            'action'     => 'index',
            'prefix'     => 'Public_Controller'
        )
    );
