<?

Route::set('public_catalog', $moduleManager->getConfig('catalog.fullpath').'(/<alias>)',
    array(
        'alias' => '[a-zA-Z0-9_/\-]*'
    )
)
->filter(
    function( $route, $params, $request) {
        if($params['alias']) {
            $params['action'] = 'inner';
        }
        return $params;
    }
)
->defaults(
    array(
        'controller' => $moduleManager->getConfig('catalog.controller'),
        'template'   => $moduleManager->getConfig('catalog.template'),
        'action'     => 'index',
        'prefix'     => 'Public_Controller'
    )
);

/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
Route::set('public_search', $moduleManager->getConfig('search.fullpath'))
->defaults(
    array(
        'controller' => $moduleManager->getConfig('search.controller'),
        'action'     => 'index',
        'prefix'     => 'Public_Controller'
    )
);


/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
Route::set('public_sitemap', $moduleManager->getConfig('sitemap.fullpath'))
->defaults(
    array(
        'controller' => $moduleManager->getConfig('sitemap.controller'),
        'action'     => 'index',
        'prefix'     => 'Public_Controller'
    )
);

/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
    Route::set('public_contacts', $moduleManager->getConfig('contacts.fullpath').'(/<alias>)',
        array(
            'alias' => '[a-zA-Z0-9_/\-]*'
        )
    )
    ->defaults(
        array(
            'controller' => $moduleManager->getConfig('contacts.controller'),
            'template'   => $moduleManager->getConfig('contacts.template'),
            'action'     => 'index',
            'prefix'     => 'Public_Controller'
        )
    );

/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
    Route::set('public_news', $moduleManager->getConfig('news.fullpath').'(/<alias>)',
        array(
            'alias' => '[a-zA-Z0-9_/\-]*'
        )
    )
    ->filter(
        function( $route, $params, $request) {
            if($params['alias']) {
                $params['action'] = 'inner';
            }
            return $params;
        }
    )
    ->defaults(
        array(
            'controller' => $moduleManager->getConfig('news.controller'),
            'template'   => $moduleManager->getConfig('news.template'),
            'action'     => 'index',
            'prefix'     => 'Public_Controller'
        )
    );

/** @noinspection PhpUndefinedClassInspection, PhpParamsInspection */
    Route::set('public_events', $moduleManager->getConfig('events.fullpath').'(/<alias>)',
        array(
            'alias' => '[a-zA-Z0-9_/\-]*'
        )
    )
    ->defaults(
        array(
            'controller' => $moduleManager->getConfig('events.controller'),
            'template'   => $moduleManager->getConfig('events.template'),
            'action'     => 'index',
            'prefix'     => 'Public_Controller'
        )
    );
