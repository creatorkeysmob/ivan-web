<?php defined('SYSPATH') or die('No direct script access.');

class Api_Public_Forms extends Controller_Api {
    const EMAIL_FROM     = 'noreply@zubarev.pro';
    const EMAIL_FALLBACK = 'seo-directory@yandex.ru'; // используется если пусто поле в админке
    const ADMIN_MAIL     = 'check@web-ar.ru'; // почта для сообщений из админки нам


    public function action_sendForm(){
         $form = Former_Form::factory('Public_Form_Feedback');
         if( $this->request->method() === Kohana_Request::POST ) {
             $form->setData($this->request->post());
             if( $form->validate() ) {

                 $data = $form->save();

                 $data['date'] = date("Y-m-d H:i:s");

                 $table_data = array(
                     'text' => $data['text'],
                     'date' => $data['date'],
                 );

                 $data['text'] = htmlspecialchars_decode($data['text']);
                 $table = $data['table'];
                 if ( $table != 'not_save' ) {
                     $result = DB::insert($data['table'])
                         ->columns(array_keys($table_data))
                         ->values(array_values($table_data))
                         ->execute($this->_db);
                 }

                 if ( $table == 'not_save' ) $email = array(self::ADMIN_MAIL);

                 // Send mail
                 $body = Twig::factory('Email/Feedback', array('data' => $data))->render();
                 self::SendMail($body, $data['title'], $email);


             } else {
                 $this->error('101', 'Invalid fields value');
                 $this->addData(
                     array(
                         'errors' => $form->getErrors()
                     )
                 );
             }
         } else {
             $this->error('102', 'Only POST method available');
         }
     }


    public function SendMail ($body, $subject, $email = false) {
        // Send mail
        $settingsManager = Registry::get(Registry::settingsManager);
        $vars = $settingsManager->getVars();

        if ( !$email ) {
            $email = Arr::get($vars,'email_feedback');
            $email = preg_split("/[;, ]+/", $email);
            $email = array_filter($email);

            if(empty($email)) {
                $email = array(self::EMAIL_FALLBACK);
            }
        } else {
            $email = explode(',', trim($email));
        }

        $mailer = Email::instance();
        $mailer->from(self::EMAIL_FROM);

        foreach($email as $_email) {
            $mailer->to($_email);
        }


        $mailer->subject($subject);
        $mailer->message($body, TRUE);
        $mailer->send();
        $mailer->reset();

        $this->addData(
            array(
                'redirect' => Arr::get($vars,'redirect_order')
            )
        );
    }

    public function action_Catalog () {
        $catalog = Widget::getUltraModule('Catalog', true);
        //var_dump(json_encode($catalog));
        echo json_encode($catalog);exit;
    }



    /* Life Edit */

    public function action_saveLifeEdit(){
        $data = $_POST;
    //    var_dump($data);exit;
        $field = array($data['field'] => htmlspecialchars($data['text']));
        DB::update($data['table'])
            ->set($field)
            ->where('id', '=', $data['id'])
            ->execute($this->_db);
        echo 1;exit;
    }

    public function action_switchLifeEdit(){
        $data = $_POST;
    //    var_dump($data);exit;
        $field = array('value' => $data['on']);
        DB::update('settings')
            ->set($field)
            ->where('id', '=', 12)
            ->execute($this->_db);
        echo 1;exit;
    }

    public function action_fileUpload(){
        $data = $_POST;
    //    var_dump($data);exit;
        $field = array('value' => $data['on']);
        DB::update('settings')
            ->set($field)
            ->where('id', '=', 12)
            ->execute($this->_db);
        echo 1;exit;
    }

}
