<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Public_Form_Feedback.
*/
class Public_Form_Feedback extends Former_Form {
    public function initElements() {
        $this->_elements += array(

            'text' => array(
                'type'  => 'text',
                'label' => 'Текст',
                'rules' => array(
                    array('not_empty')
                ),
            ),
            'table' => array(
                'type'  => 'text',
                'label' => 'Текст',
                'rules' => array(
                    array('not_empty')
                ),
            ),
            'title' => array(
                'type'  => 'text',
                'label' => 'Текст',
                'rules' => array(
                    array('not_empty')
                ),
            ),

        );
        parent::initElements();
    }
}
