<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Pressa extends Public_Controller_Abstract {

    public function action_index() {

        $pressa = Widget::getModule('Pressa');
        foreach ($pressa as $value) {
            $day  = date_format(date_create($pressa['date']), ("d"));
            $mout = Widget::getRusMonth(date_format(date_create($pressa['date']), ("n")));
            $year = date_format(date_create($pressa['date']), ("Y"));
            $value->date = $day.' '.$mout.' '.$year;
        }

        $this->render('Pressa/Index', array(
                'pressa' => $pressa,
                'index'  => true,
            )
        );
    }
}
