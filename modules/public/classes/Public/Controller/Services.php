<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Services extends Public_Controller_Abstract {
    const CONFIG_PATH = 'public/modules/services';
    const MODEL_NAME = 'Service';
    const INDEX_TEMPLATE = 'Services/Index';

    public function action_index() {
        $submenu = Widget::getServices();
        $this->render(self::INDEX_TEMPLATE, array(
                'services' => $submenu,
                'index' => true,
                'articleshide' =>true,
                'all_blogs'  => Widget::getBlogList(),
            )
        );
    }

    public function action_inner() {
        $alias = HTML::chars($this->request->param('alias'));

        /** @var $item Model_News */
        $item = ORM::factory(self::MODEL_NAME)
            ->where('alias', '=', $alias)
            ->and_where('hide', '=', 0)
            ->find();

        if (!$item->loaded()) {
            /** @noinspection PhpUndefinedClassInspection */
            throw new HTTP_Exception_404();
        }
        Breadcrumb::instance()->addItem($item->head);
        $submenu = Widget::getServices($item->id);
        $album = Widget::getAlbum($item->album);
      //  $tariff = Widget::getTariff($item->tariff);

        $this->render(self::INDEX_TEMPLATE, array(
                'item'         => $item,
                'submenu'      => $submenu,
                'services'     => $submenu,
                'articleshide' => true,
                'all_blogs'    => Widget::getBlogList(),
                'album'        => $album,
        //        'tariff'       => $tariff,
            )
        );
    }
}