<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Projects extends Public_Controller_Abstract {
    const MODEL_NAME = 'Projects';
    const TEMPLATE   = 'Projects/Index';

    public function action_index() {

        //$all_item = Widget::getModule('Projects');
        $documentManager = Registry::get(Registry::documentManager);
        $document = $documentManager->getModel();
        $page_id = $document->id;
        $item      = ORM::factory('Projects')->where('page', '=', $page_id)->find()->as_array();

        $item['block1_title'] = html_entity_decode($item['block1_title']);
        $item['block1_body'] = html_entity_decode($item['block1_body']);

        $item['info_in_head'] = unserialize($item['info_in_head']);
        $item['block3'] = unserialize($item['block3']);
        //$item['block2_body'] = unserialize($item['block2_body']);
        $item['block4_body'] = unserialize($item['block4_body']);
        $item['block5_body'] = unserialize($item['block5_body']);
        //var_dump($item['block5_body']);exit();

        $this->render(self::TEMPLATE, array(
                'item' => $item,
                'table' => 'projects',
            )
        );
    }

}
