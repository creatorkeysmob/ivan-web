<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Catalog extends Public_Controller_Abstract {
    const MODEL_NAME = 'Catalog';
    const TEMPLATE   = 'Catalog/Index';


    public function action_index() {
        $this->render(self::TEMPLATE, array(
                'index'      => true,
                'table'      => 'catalog',
            )
        );
    }

    public function action_inner() {
        $alias = HTML::chars($this->request->param('alias'));
        $item = Widget::getItem(self::MODEL_NAME, self::TEMPLATE, array('alias', '=', $alias));
    }




}
