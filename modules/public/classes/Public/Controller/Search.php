<?php defined('SYSPATH') or die('No direct script access.');

/** @noinspection PhpIncludeInspection */
require_once Kohana::find_file('vendor','Stemmer/En');

/** @noinspection PhpIncludeInspection */
require_once Kohana::find_file('vendor','Stemmer/Ru');

class Public_Controller_Search extends Public_Controller_Abstract {
    // Configs
    const SEARCH_CONFIG = 'public/modules/search';
    const SEARCH_HIGHLIGHT_PATTERN = '<b>$0</b>';
    const SEARCH_MIN_WORD_LENGTH = 3;

    // Models
    const SEARCH_MODEL = 'Search';

    protected $_models = array();
    protected $_errors = array();

    public function action_index(){
        $original_query = HTML::chars($this->request->query('q'));
        $prepared_query = $this->_prepareSearchQuery($original_query);
        $hash = md5($prepared_query);

        if(empty($prepared_query)) $this->_errors['short'] = true;

        // Load search model
        $search_model = ORM::factory(self::SEARCH_MODEL);

        // Remove from cache
        Db::delete($search_model->table_name())
            ->where('sdate','<=',DB::expr('NOW() - INTERVAL 1 HOUR'))
            ->execute();

        // Search in cached data
        $cached_count = $search_model->where('hash','=',$hash)->count_all();
        if(!sizeof($this->_errors)) {
            if(empty($cached_count)) {
                $this->_initModelsIfNeeded();

                $items = array();
                /** @var $_model Model_Abstract */
                foreach($this->_models as $_model) {
                    $items = array_merge($items, $_model->search($prepared_query));
                }

                // Cache found results
                $cached_count = count($items);
                if(!empty($cached_count)) {
                    foreach($items as $_item) {
                        $search_result = $this->_processingSearchResult($_item, $prepared_query);
                        $search_data = array(
                            'related'   => $search_result['id'],
                            'hash'      => $hash,
                            'sdate'     => date("Y-m-d H:i:s"),
                            'module'    => isset($search_result['module']) ? $search_result['module'] : 'text',
                            'head'      => $search_result['head'],
                            'body'      => $search_result['body'],
                            'score'     => $search_result['score'],
                            'fullpath'  => isset($search_result['fullpath']) ? $search_result['fullpath'] : '#',
                        );

                        // Insert to database cache
                        DB::insert($search_model->table_name(), array_keys($search_data))
                            ->values($search_data)
                            ->execute();
                    }
                }
                unset($items);
            }
        }

        // Get cached results if exists
        $items = array();
        $pagination = false;

        if(!empty($cached_count)) {
            // Load config
            $config = Kohana::$config->load(self::SEARCH_CONFIG)->as_array();

            // Apply Pagination
            $pagination = Pagination::factory(
                array(
                    'source'      => 'query',
                    'key'         => 'page',
                    'perpage'     => Arr::path($config, 'pagination.perpage'),
                    'total_items' => $cached_count,
                    'view'        => 'Block/Pagination'
                ),
                'dynamic'
            );
            $pagination->execute();

            // Get items for current page
            $items = $search_model
                ->where('hash','=', $hash)
                ->limit($pagination->getPerpage())
                ->offset($pagination->getOffset())
                ->find_all()
                ->as_array();

            $pagination = $pagination->render();
        }

        if(!$items) {
            $this->_errors['empty'] = true;
        }

        $this->render( 'Search/List', array(
                'scount'     => $cached_count,
                'squery'     => $original_query,
                'items'      => $items,
                'errors'     => $this->_errors,
                'pagination' => $pagination
            )
        );
    }

    protected function _initModelsIfNeeded() {
        if(!sizeof($this->_models)) {
            $config = Kohana::$config->load(self::SEARCH_CONFIG)->as_array();
            if(isset($config['models']) && is_array($config['models'])){
                foreach($config['models'] as $_model) {
                    $orm = ORM::factory($_model);
                    if(method_exists($orm, 'search')) {
                        $this->_models[strtolower($_model)] = $orm;
                    } else {
                        throw new Exception('Search: Model_'.$_model.' doesn\'t implement method search');
                    }
                }
                if(!sizeof($this->_models)) {
                    throw new Exception('Search: models empty');
                }
            } else {
                throw new Exception('Search: models does not exists');
            }
        }
    }

    protected function _prepareSearchQuery($query){
        $query = strip_tags(urldecode($query));
        $query = str_replace(
            array('@', '"', '\'', '#', '&', '%', '^', '?', '*', '\\', '/', '-', '=', '+', '|', '(', ')', '«', '»', '“', '”', '„', '.', ',', ';', ':', '[', ']', '{', '}'),
            '',
            $query
        );
        $query = preg_replace('/[^\w\x7F-\xFF\s]/', '', $query);
        $query = str_replace(
            array('Ё','ё'),
            array('Е','е'),
            $query
        );

        $words = preg_split('/\s+/', $query);
        $words = array_unique($words);
        $words = array_filter($words);

        // Stemm words
        if (!empty($words)) {
            $min_word_length = self::SEARCH_MIN_WORD_LENGTH;
            $words = array_map(
                function($element) use($min_word_length){
                    if (strlen($element) >= $min_word_length) {
                        if (preg_match('/[А-Я]+/si', $element)) {
                            $stemm = Stemmer_Ru::stem($element);
                        } else {
                            $stemm = Stemmer_En::stem($element);
                        }
                        return $stemm . '*';
                    }
                    return false;
                },
                $words
            );
        }
        return implode(' ', $words);
    }

    protected function _processingSearchResult($_item, $query) {
        $_item = array_map('strip_tags', $_item);

        // Prepare patterns
        $grep_pattern = '/' . str_replace('*', '\w*|', $query) . '/i';
        $grep_pattern = str_replace(' ', '', $grep_pattern);
        $grep_pattern = str_replace('|/i', '/i', $grep_pattern);

        $highlight_pattern = self::SEARCH_HIGHLIGHT_PATTERN;

        // Explode text into sentence array
        $sentence_array = preg_split("#(?<=[\.\?\!])\s+(?=[А-ЯA-Z-])#s", $_item['body'], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $sentence_array = array_map(
            function($element){
                $element = trim($element);
                if(strlen($element) > 10) {
                    return $element;
                }
                return false;
            },
            $sentence_array
        );

        if(count($sentence_array)) {
            $search_content = preg_grep($grep_pattern, $sentence_array);
            $search_count = count($search_content);

            if($search_count > 2) {
                $search_content = array_slice($search_content, 0, 2);
            } elseif(empty($search_count)) {
                $search_content = array( $sentence_array[0] );
            }

            $_item['body'] = implode('&hellip; ', $search_content);
            if (!empty($search_content)) {
                $_item['body'] = preg_replace($grep_pattern, $highlight_pattern, $_item['body']);
            }
        } else {
            $_item['body'] = '';
        }
        return $_item;
    }
}
