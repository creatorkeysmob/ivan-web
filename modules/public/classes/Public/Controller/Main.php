<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Main extends Public_Controller_Abstract {
    public function action_index(){
        $admin = ( $_COOKIE['wa_admin'] == 1 && $vars['life_edit'] ) ? true : false;
        $this->render('Index', array(
                'mainpage'  => true,
                'slider'    => Widget::getModule('Slider'),
                'admin'     => $admin,
                'wa_admin'  => $_COOKIE['wa_admin'],
                'modules'   => Widget::getModule('Modules'),
                'news'      => Widget::getBigModule('News'),
                /****************** Modules by Admin The Creator ******************/
                #################################
                #################################
                /******************************************************************/
            )
        );
    }
}
