<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_About extends Public_Controller_Abstract {
    const MODEL_NAME = 'About';
    const TEMPLATE   = 'About/Index';

    public function action_index() {

        //$all_item = Widget::getModule('About');
        $documentManager = Registry::get(Registry::documentManager);
        $document = $documentManager->getModel();
        $page_id = $document->id;

        $item      = ORM::factory('About')->where('page', '=', $page_id)->find()->as_array();

        $item['logo_menu'] = html_entity_decode($item['logo_menu']);
        $item['block1_body'] = html_entity_decode($item['block1_body']);
        $item['block2_body'] = html_entity_decode($item['block2_body']);
        //$item['block3_body'] = html_entity_decode($item['block3_body']);
        $item['block5_body'] = html_entity_decode($item['block5_body']);
        $item['block6_body'] = html_entity_decode($item['block6_body']);
        $item['logo_menu'] = unserialize($item['logo_menu']);
        $item['block3_body'] = unserialize($item['block3_body']);
        $item['block5_body'] = unserialize($item['block5_body']);

        $this->render(self::TEMPLATE, array(
                'item' => $item,
            )
        );

    }

}
