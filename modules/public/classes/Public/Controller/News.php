<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_News extends Public_Controller_Abstract {
    const MODEL_NAME = 'News';
    const TEMPLATE   = 'News/Index';

    public function action_index() {

        $all_item = Widget::getModule('News');

        $elementsCount = 5;
        // $all_news   = Widget::getNewsList();
        $count        = count($all_item);
        $pager_count  = ceil($count/$elementsCount);
        $page         = isset($_GET['pn']) ? ($_GET['pn'] - 1) : 0;
        $limitPage    = $page * $elementsCount. "," .$elementsCount;
        $itembypage   = Widget::getBigModule('News', $limitPage);

        $this->render(self::TEMPLATE, array(
                'arraylist'   => $itembypage,
                'index'       => true,
                'pager_count' => (int)$pager_count,
                'page'        => $page,
                'table'       => 'news',
            )
        );
    }

    public function action_inner() {
        $alias = HTML::chars($this->request->param('alias'));
        $item = Widget::getItem(self::MODEL_NAME, self::TEMPLATE, array('alias', '=', $alias));
    }

}
