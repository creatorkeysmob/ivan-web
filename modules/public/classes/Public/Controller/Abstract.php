<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Public_Controller_Abstract
 */
abstract class Public_Controller_Abstract extends Controller_View {
    public function before() {
        parent::before();
    }

    public function after() {
        parent::after();
    }

    public function beforeRender() {
        /** @var $documentManager Component_Document */
        $documentManager = Registry::get(Registry::documentManager);

        /** @var $settingsManager Component_Settings */
        $settingsManager = Registry::get(Registry::settingsManager);
        $isUtmDetected = Session::instance()->get('utm');
        if(!$isUtmDetected) {
            // Detect utm
            $query = $this->request->query();
            $utm = array(
                'utm_source',
                'utm_medium',
                'utm_campaign',
                'utm_term',
                'utm_content'
            );
            $data = array_intersect_key($query, array_flip($utm));
            if($data) {
                Session::instance()->set('utm', true);
                $isUtmDetected = true;
            }
        }

        $vars = $settingsManager->getVars();
        $vars = array_map(function($el){
            $el = html_entity_decode($el);
            return $el;
        }, $vars);

        $document = $documentManager->getModel();
        if($document){
            $document->head = html_entity_decode($document->head);
            $document->body = html_entity_decode($document->body);
        }

        $admin = ( $_COOKIE['wa_admin'] == 1 && $vars['life_edit'] ) ? true : false;

        $this->addTemplateVariable('css',        '/assets/public/css');
        $this->addTemplateVariable('js',         '/assets/public/js');

        $this->addTemplateVariable('site',       $_SERVER["HTTP_HOST"]);
        $this->addTemplateVariable('url',        $_SERVER["REQUEST_URI"]);
        $this->addTemplateVariable('document',   $document);
        $this->addTemplateVariable('vars',       $vars);
        $this->addTemplateVariable('breadcrumb', Widget::getBreadcrumbs());
        $this->addTemplateVariable('menu',       Widget::getMenu(1,1));
        $this->addTemplateVariable('footermenu', Widget::getMenu(1,1, 'bottomshow'));
        $this->addTemplateVariable('form',       Widget::getForm());
        $this->addTemplateVariable('admin',      $admin);
        $this->addTemplateVariable('wa_admin',   $_COOKIE['wa_admin']);
        $this->addTemplateVariable('news_url',   '/'.Route::get("public_news")->uri());

        // Clear memory
        unset($documentManager);
        unset($settingsManager);

        // Trigger parent beforeRender
        parent::beforeRender();
    }

    public function afterRender() {
        parent::afterRender();
    }

    /**
     * Index action
     */
    public function action_index() { }
}
