<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Contacts extends Public_Controller_Abstract {
    const INDEX_TEMPLATE = 'Contact/Index';

    public function action_index() {
        $this->render(self::INDEX_TEMPLATE, array(
            'index' => true,
            'table' => 'catalog'
        ));
    }
}
