<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Sitemap extends Public_Controller_Abstract {
    const INDEX_TEMPLATE = 'Sitemap/Index';

    public function action_index() {
        global $sitemap;
        self::getSiteMapLvl(0);
        $this->render(self::INDEX_TEMPLATE, array(
            'sitemap' => $sitemap,
        ));
    }


    private function getSiteMapLvl($parent_id) {
        global $sitemap;
        $pages  = DB::select('id', 'head', 'fullpath', 'parent_id', 'module')
                    ->from('pages')
                    ->where('parent_id', '=', $parent_id)
                    ->and_where('sitemapshow', '=', 1)
                    ->order_by('priority', 'ASC')
                    ->execute()
                    ->as_array();

        if (!$pages) return false;
        $sitemap .= '<ul>';
        foreach ($pages as $key => $page) {
            $sitemap .= '<li><a href="'.$page['fullpath'].'">'.$page['head'].'</a></li>';
            $modules = Kohana::$config->load('admin/main.sitemap');
            if ( in_array($page['module'], $modules) ) {
                self::getSiteMapModule($page['module']);
            }
            self::getSiteMapLvl($page['id']);
        }
        $sitemap .= '</ul>';
    }

    private function getSiteMapModule($module) {
        global $sitemap;
        $pages  = DB::select('id', 'head', 'alias')
                    ->from($module)
                    ->where('sitemapshow', '=', 1)
                    ->order_by('priority', 'ASC')
                    ->execute()
                    ->as_array();

        if (!$pages) return false;
        $sitemap .= '<ul>';
        foreach ($pages as $key => $page) {
            $url = '/'.Route::get("public_$module")->uri(
                array(
                    'alias' => $page['alias']
                )
            );
            $sitemap .= '<li><a href="'.$url.'">'.$page['head'].'</a></li>';
        }
        $sitemap .= '</ul>';
    }

}
