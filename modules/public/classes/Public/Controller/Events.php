<?php defined('SYSPATH') or die('No direct script access.');

class Public_Controller_Events extends Public_Controller_Abstract {
    const MODEL_NAME = 'Events';
    const TEMPLATE   = 'Events/Index';

    public function action_index() {

        $all_item = Widget::getModule('Events');

        //$elementsCount = 5;
        // $all_events   = Widget::getEventsList();
        // $count        = count($all_item);
        // $pager_count  = ceil($count/$elementsCount);
        // $page         = isset($_GET['pn']) ? ($_GET['pn'] - 1) : 0;
        // $limitPage    = $page * $elementsCount. "," .$elementsCount;
        // $itembypage   = Widget::getBigModule('Events', $limitPage);

        $post_events = array();
        $future_events = array();

        foreach ($all_item as $event) {
          $event = $event->as_array();
          //var_dump(date('',$event['date']));
          $event['day']   = date('d', strtotime($event['date']));
          $event['month'] = Widget::getRusMonth(date('n', strtotime($event['date'])));
          $event['year']   = date('Y', strtotime($event['date']));
          if ( date('Y-m-d H:i:s', time()) > $event['date'] ){
            //var_dump($event['date']);
            $post_events[] = $event;
          } else {
            $future_events[] = $event;
          }

        }

        $this->render(self::TEMPLATE, array(
                //'arraylist'   => $itembypage,
                'index'       => true,
                //'pager_count' => (int)$pager_count,
                //'page'        => $page,
                'table'       => 'events',
                'post_events' => $post_events,
                'future_events' => $future_events,
            )
        );
    }

}
