<?php

/**
 * Class Helper_Trees
 */
class Helper_Tree {
    public static function transformArrayToTree($items, $child_key = 'childs', $id_key = 'id', $parent_key = 'parent_id')
    {
        $children = array(); // children of each ID
        $ids = array();
        foreach ($items as $i=>$r) {
            $row =& $items[$i];
            $id = $row[$id_key];
            $pid = $row[$parent_key];
            $children[$pid][$id] =& $row;
            if (!isset($children[$id])) $children[$id] = array();
            $row[$child_key] =& $children[$id];
            $ids[$row[$id_key]] = true;
        }
        // Root elements are elements with non-found PIDs.
        $forest = array();
        foreach ($items as $i=>$r) {
            $row =& $items[$i];
            if (!isset($ids[$row[$parent_key]])) {
                $forest[$row[$id_key]] =& $row;
            }
            unset($row[$id_key]); unset($row[$parent_key]);
        }
        return $forest;
    }
}