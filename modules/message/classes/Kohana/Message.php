<?php

/**
 * Class Message
 */
class Kohana_Message {
    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';
    const SESSION_KEY = 'flash';

    protected $_priority = array(
        self::TYPE_ERROR, self::TYPE_WARNING, self::TYPE_INFO, self::TYPE_SUCCESS
    );

    protected $_messages = array();
    protected static $_instance = null;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    /**
     * Get message instance
     * @return Message
     */
    public static function instance() {
        if( is_null(self::$_instance) ) {
            $instance = new Kohana_Message();

            /* Load from session */
            $instance->_messages = (array) Session::instance()->get(self::SESSION_KEY);
            self::$_instance = $instance;
        }
        return self::$_instance;
    }

    /**
     * Add message in queue
     * @param $key
     * @param $value
     * @param string $type
     * @return $this
     */
    public function add( $key, $value, $type = self::TYPE_ERROR ) {
        $this->_messages[$type][$key] = $value;
        Session::instance()->set(self::SESSION_KEY, $this->_messages);
        return $this;
    }

    /**
     * Remove message from queue
     * @param $key
     * @param string $type
     * @return $this
     */
    public function remove( $key, $type = self::TYPE_ERROR ) {
        if( isset($this->_messages[$type][$key]) ) {
            unset($this->_messages[$type][$key]);
        }
        Session::instance()->set(self::SESSION_KEY, $this->_messages);
        return $this;
    }

    /**
     * Set multiple message into queue
     * @param array $messages
     * @param string $type
     * @return $this
     */
    public function set($messages, $type = self::TYPE_ERROR) {
        foreach($messages as $_key => $_value) {
            $this->add($_key, $_value, $type);
        }
        return $this;
    }

    /**
     * Get all messages
     * @param bool $plain
     * @param bool $once
     * @param $type
     * @return array
     */
    public function get($plain = true, $once = true, $type = null) {
        $messages = array();
        if( $plain ) {
            foreach( $this->_priority as $_priority ) {
                if(isset($this->_messages[$_priority]) && is_array($this->_messages[$_priority])) {
                    foreach($this->_messages[$_priority] as $_key => $_value) {
                        $messages[] = array(
                            'type' => $_priority,
                            'text' => $_value
                        );
                    }
                }
            }
            if( $once ) {
                $this->clear();
            }
            return $messages;
        }

        if( !is_null($type) ) {
            $messages = ( isset($this->_messages[$type]) ? $this->_messages[$type] : array() );
            if( $once ) {
                $this->_messages[$type] = array();
                Session::instance()->set(self::SESSION_KEY, $this->_messages);
            }
            return $messages;
        }

        $messages = $this->_messages;
        if( $once ) {
            $this->clear();
        }
        return $messages;
    }

    /**
     * Delete all messages
     */
    public function clear() {
        $this->_messages = array();
        Session::instance()->set(self::SESSION_KEY, $this->_messages);
    }
}