<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @property mixed user_id
 */
class Model_Role extends Model_Auth_Role {

	// This class can be replaced or extended

} // End Role Model