<?php

defined('SYSPATH') or die('No direct script access.');

use Symfony\Component\Filesystem\Filesystem;

/**
 * Twig view
 */
class Kohana_Twig extends View
{

    /**
     * Twig environment
     */
    protected static $_environment = NULL;

    /**
     * Initialize the Twig module
     */
    public static function init()
    {
        Twig_Autoloader::register();
        $cache = Kohana::$config->load('twig.environment.cache');
        if($cache) {
            if (!is_dir($cache)) {
                $filesystem = new Filesystem();
                $filesystem->mkdir($cache);
            }

            if (!is_writable($cache)) {
                throw new Kohana_Exception('Directory :dir must be writable', array(
                    ':dir' => Debug::path($cache),
                ));
            }
        }
    }

    /**
     * Create a Twig view instance
     *
     * @param   string $file Name of Twig template
     * @param   array $data Data to be passed to template
     * @return  Twig    Twig view instance
     */
    public static function factory($file = NULL, array $data = NULL)
    {
        return new Twig($file, $data);
    }

    /**
     * Create a new Twig environment
     *
     * @return  Twig_Environment  Twig environment
     */
    protected static function twig()
    {

        $twig_config = Kohana::$config->load('twig');
        $environment_config = $twig_config->get('environment');

        $loader = new Twig_Loader_CFS($twig_config->get('loader'));
        $environment = new Twig_Environment($loader, $environment_config);

        // Inject global vars
        $globals = $twig_config->get('globals');
        $environment->addGlobal('global', $globals);

        //inject extensions to environment
        $extensions = $twig_config->get('extensions');
        if (is_array($extensions) && !empty($extensions)) {
            foreach ($extensions as $_extension) {
                $environment->addExtension($_extension);
            }
        }
        return $environment;
    }

    /**
     * Get the Twig environment (or create it on first call)
     *
     * @return  Twig_Environment  Twig environment
     */
    protected static function environment()
    {
        if (static::$_environment === NULL) {
            static::$_environment = static::twig();
        }
        return static::$_environment;
    }

    /**
     * Set the filename for the Twig view
     *
     * @param   string $file Base name of template
     * @return  Twig    This Twig instance
     */
    function set_filename($file)
    {
        $this->_file = $file;
        return $this;
    }

    /**
     * Render Twig template as string
     *
     * @param   string $file Base name of template
     * @return  string  Rendered Twig template
     */
    public function render($file = NULL)
    {
        if ($file !== NULL) {
            $this->set_filename($file);
        }
        return static::environment()->render($this->_file, $this->_data);
    }

} // End Twig
