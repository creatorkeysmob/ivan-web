<?php
/**
 * Provides integration of the Config component with Twig.
 */
class Twig_Extension_Config extends Twig_Extension {

    public function __construct() { }

    /**
     * Register function
     * @return array
     */
    public function getFunctions() {
        return array(
            'get_config_param' => new \Twig_Function_Method( $this, 'getConfigParam' ),
        );
    }

    /**
     * Functions
     * @param $group
     * @param $key
     * @return string
     */

    public function getConfigParam( $group, $key = null ) {
        $config = Kohana::$config->load($group);
        if( !is_null($key) ) {
            return $config->get($key);
        }
        return $config;
    }

    /**
     * Get extension name
     * @return string
     */
    public function getName() {
        return 'config';
    }
}