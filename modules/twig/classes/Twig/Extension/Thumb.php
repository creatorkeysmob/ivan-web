<?php
/**
 * Provides integration of the Routing component with Twig.
 */
class Twig_Extension_Thumb extends Twig_Extension {

    public function __construct() { }

    /**
     * Returns a list of functions to add to the existing list.
     * @return array An array of functions
     */
    public function getFunctions() {
        return array(
            'thumb' => new \Twig_Function_Method( $this, 'getThumb' ),
        );
    }
    public function getThumb( $name, $config = array() ) {
        return Thumb::factory($name,$config)->render();
    }
    /**
     * Возвращает имя расширения
     * @return string The extension name
     */
    public function getName() {
        return 'thumb';
    }
}