<?php

class Twig_Extension_Unserializer extends Twig_Extension { 
    
//здесь мы и определяем нужную нам функцию
    public function getFunctions() { 
        return array('unserializer' => new Twig_Function_Function('unserialize', array())); 
    } 

//обязательно
    public function getName() {
        return 'unserializer';
    }   
}