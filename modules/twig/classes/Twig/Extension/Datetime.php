<?php

class Twig_Extension_Datetime extends Twig_Extension
{
    public function getFilters()
    {
        return array(
            'datetime' => new Twig_Filter_Method($this, 'datetime')
        );
    }

    public function datetime($d, $format = "%d.%m.%Y %H:%M")
    {
        if ($d instanceof \DateTime) {
            $d = $d->getTimestamp();
        } else {
            $d = strtotime($d);
        }

        // Replace russian months
        $current_month = intval(date('m', $d)) - 1;
        if( strpos($format,'%B_R') !== FALSE ) {
            $month_array = array(
                'январь', 'февраль', 'март', 'апрель',
                'май', 'июнь', 'июль', 'август',
                'сентябрь', 'октябрь', 'ноябрь', 'декабрь'
            );

            if(isset($month_array[$current_month])) {
                $format = str_replace('%B_R',$month_array[$current_month],$format);
            }
        }

        if( strpos($format,'%BR_R') !== FALSE ) {
            $month_array = array(
                'января', 'февраля', 'марта', 'апреля',
                'мая', 'июня', 'июля', 'августа',
                'сентября', 'октября', 'ноября', 'декабря'
            );

            if(isset($month_array[$current_month])) {
                $format = str_replace('%BR_R',$month_array[$current_month],$format);
            }
        }

        if( strpos($format,'%b_r') !== FALSE ) {
            $month_array = array(
                'янв', 'фев', 'мар', 'апр',
                'май', 'июн', 'июл', 'авг',
                'сен', 'окт', 'ноя', 'дек'
            );

            if(isset($month_array[$current_month])) {
                $format = str_replace('%b_r',$month_array[$current_month],$format);
            }
        }

        return strftime($format, $d);
    }

    public function getName()
    {
        return 'Datetime';
    }
}