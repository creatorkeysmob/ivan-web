<?php

class Twig_Extension_Helpers extends Twig_Extension {
    public function getFilters()
    {
        return array(
            // Translation
            'translate' => new Twig_Filter_Function('__'),
            'trans' => new Twig_Filter_Function('__'),
            'tr' => new Twig_Filter_Function('__'),

            //Date and time
            'timestamp' => new Twig_Filter_Function('strtotime'),

            //Strings
            'plural' => new Twig_Filter_Function('Inflector::plural'),
            'singular' => new Twig_Filter_Function('Inflector::singular'),
            'humanize' => new Twig_Filter_Function('Inflector::humanize'),

            //HTML
            'obfuscate' => new Twig_Filter_Function('Html::obfuscate'),

            //Numbers
            'num_format' => new Twig_Filter_Function('Num::format'),

            //Text
            'limit_words' => new Twig_Filter_Function('Text::limit_words'),
            'limit_chars' => new Twig_Filter_Function('Text::limit_chars'),
            'unescape' => new Twig_Filter_Function('html_entity_decode'),
            'bytes' => new Twig_Filter_Function('Text::bytes'),
            'unescape' => new Twig_Filter_Function('html_entity_decode'),
            'breakless' => new Twig_Filter_Method($this,'removeBreaks'),

            //Url
            'urltitle' => new Twig_Filter_Function('Url::title'),
        );
    }

    public function getFunctions() {
        return array(
            'price_format' => new \Twig_Function_Method( $this, 'priceFormat' ),
            'integer_to_string' => new \Twig_Function_Method( $this, 'integerToString' ),
        );
    }

    /**
     * Remove breaks from string
     * @param $string
     * @return string
     */
    public function removeBreaks( $string ) {
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        $string = trim(preg_replace('/\s{2,}/', ' ', $string));
        return $string;
    }

    /**
     * Get formated money
     * @param $price
     * @param int $decimals
     * @return string
     */
    public function priceFormat( $price, $decimals = 0 ) {
        return number_format( $price, $decimals, ',', ' ' );
    }

    /**
     * @param $int
     * @param $str1
     * @param $str2
     * @param $str3
     * @param bool $showint
     * @return string
     */
    public function integerToString( $int, $str1, $str2, $str3, $showint = false ) {
        $int = (string)$int;
        $last = substr( $int, -1 );
        $prelast = substr( $int, -2 );
        if ( $showint !== false ) {
            $str1 = $int . ' ' . $str1;
            $str2 = $int . ' ' . $str2;
            $str3 = $int . ' ' . $str3;
        }
        if ( $last == 0 || ( $last >= 5 && $last <= 9 ) || $prelast >= 11 && $prelast <= 14 )
            return $str3;
        elseif ( $last >= 2 && $last <= 4 )
            return $str2;
        else return $str1;
    }

    public function getName()
    {
        return 'helpers';
    }
}