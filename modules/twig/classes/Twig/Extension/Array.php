<?php

class Twig_Extension_Array extends Twig_Extension {

    public function getFunctions() {
        return array(
            'array_diff' => new \Twig_Function_Method( $this, 'arrayDiff' ),
        );
    }

    /**
     * Array diff
     * @param $first
     * @param $second
     * @return array|bool
     */
    public function arrayDiff( $first, $second ) {
        if(!is_array($first) or !is_array($second)) return false;
        return array_diff($first, $second);
    }

    public function getName()
    {
        return 'array';
    }
}