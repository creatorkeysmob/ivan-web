<?php
/**
 * Provides integration of the Routing component with Twig.
 */
class Twig_Extension_Route extends Twig_Extension {

    public function __construct() { }

    /**
     * Returns a list of functions to add to the existing list.
     * @return array An array of functions
     */
    public function getFunctions() {
        return array(
            'get_route_uri' => new \Twig_Function_Method( $this, 'getRouteUri' ),
        );
    }

    /**
     * Аналог функции Route->get('name')->uri(array('action'=>'...'))
     * @param $name - название роута
     * @param array $parameters - параметры роута
     * @param array $get - get параметры
     * @return string - сгенерированный uri
     */
    public function getRouteUri( $name, $parameters = array(), $get = array() ) {
        /** @noinspection PhpUndefinedClassInspection */
        $uri = '/'.strtolower( Route::get( $name )->uri( $parameters ) );
        $get = array_filter($get);

        if($get) {
            $uri .= '?'.http_build_query($get);
        }
        return $uri;
    }

    /**
     * Возвращает имя расширения
     * @return string The extension name
     */
    public function getName() {
        return 'route';
    }
}