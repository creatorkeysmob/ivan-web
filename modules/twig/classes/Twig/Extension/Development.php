<?php
/**
 * Kohana development features
 */
class Twig_Extension_Development extends Twig_Extension {

    public function __construct() { }

    /**
     * Returns a list of functions to add to the existing list.
     * @return array An array of functions
     */
    public function getFunctions() {
        return array(
            'development_toolbar' => new \Twig_Function_Method( $this, 'getDevelopmentToolbar' ),
        );
    }


    public function getDevelopmentToolbar( ) {
        if(Kohana::$environment == Kohana::DEVELOPMENT) {
            return Debugtoolbar::render();
        }
        return false;
    }

    /**
     * Возвращает имя расширения
     * @return string The extension name
     */
    public function getName() {
        return 'development';
    }
}