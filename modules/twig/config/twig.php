<?php defined('SYSPATH') or die('No direct script access.');

return array(

    /**
     * Twig Loader options
     */
    'loader' => array(
        'extension' => 'twig',  // Extension for Twig files
        'path'      => 'views', // Path within cascading filesystem for Twig files
    ),

    /**
     * Twig Environment options
     *
     * http://twig.sensiolabs.org/doc/api.html#environment-options
     */
    'environment' => array(
        'debug'               => TRUE,
        'auto_reload'         => (Kohana::$environment == Kohana::DEVELOPMENT),
        'autoescape'          => FALSE,
        'base_template_class' => 'Twig_Template',
        //'cache'               => APPPATH.'cache/twig',
		'cache'               => FALSE,
        'charset'             => 'utf-8',
        'optimizations'       => -1,
        'strict_variables'    => FALSE,
    ),

    'globals' => array( ),
    'extensions' => array(
        new Twig_Extension_Debug(),
        new Twig_Extension_Development(),
        new Twig_Extension_Route(),
        new Twig_Extension_Config(),
        new Twig_Extension_Helpers(),
        new Twig_Extension_Thumb(),
        new Twig_Extension_Datetime(),
        new Twig_Extension_Text(),
        new Twig_Extension_Array(),
        new Twig_Extension_Unserializer()
    )

);