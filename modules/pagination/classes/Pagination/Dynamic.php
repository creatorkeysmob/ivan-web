<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Pagination_Dynamic
 */
class Pagination_Dynamic extends Pagination {
    /**
     * @var array
     */
    protected $_first_pages = array();

    /**
     * @var array
     */
    protected $_middle_pages = array();

    /**
     * @var array
     */
    protected $_last_pages = array();

    /**
     * @var int
     */
    protected $_offset_count = 3;

    /**
     * @var int
     */
    protected $_offset_start = 3;

    /**
     * @var int
     */
    protected $_offset_end = 3;

    /**
     * @param int $offset_count
     * @return $this
     */
    public function setOffsetCount($offset_count) {
        $this->_offset_count = $offset_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffsetCount() {
        return $this->_offset_count;
    }

    /**
     * @param int $offset_end
     * @return $this
     */
    public function setOffsetEnd($offset_end) {
        $this->_offset_end = $offset_end;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffsetEnd() {
        return $this->_offset_end;
    }

    /**
     * @param int $offset_start
     * @return $this
     */
    public function setOffsetStart($offset_start) {
        $this->_offset_start = $offset_start;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOffsetStart() {
        return $this->_offset_start;
    }

    /**
     * @return array
     */
    public function getFirstPages() {
        return $this->_first_pages;
    }

    /**
     * @param array $first_pages
     * @return $this
     */
    public function setFirstPages($first_pages) {
        $this->_first_pages = $first_pages;
        return $this;
    }

    /**
     * @return array
     */
    public function getMiddlePages() {
        return $this->_middle_pages;
    }

    /**
     * @param array $middle_pages
     * @return $this
     */
    public function setMiddlePages($middle_pages) {
        $this->_middle_pages = $middle_pages;
        return $this;
    }

    /**
     * @return array
     */
    public function getLastPages() {
        return $this->_last_pages;
    }

    /**
     * @param array $last_pages
     * @return $this
     */
    public function setLastPages($last_pages) {
        $this->_last_pages = $last_pages;
        return $this;
    }

    /**
     * Calculate pagination
     * @return $this
     */
    public function execute() {
        if ($this->getTotalPages() < 2) {
            return $this;
        }

        if (!$this->getCurrentPage() || $this->getCurrentPage() < 1) {
            $this->setCurrentPage(1);
        }

        if ($this->getCurrentPage() > $this->getTotalPages()) {
            $this->setCurrentPage( $this->getTotalPages() );
        }

        // Get current parameters
        $current_page = $this->getCurrentPage();
        $total_pages = $this->getTotalPages();
        $offset_start = $this->getOffsetStart();
        $offset_count = $this->getOffsetCount();
        $offset_end = $this->getOffsetEnd();

        $first_pages = array();
        $middle_pages = array();
        $last_pages = array();

        // Calculate pages
        for ($i = $current_page - $offset_count - $offset_start; $i < $current_page; $i++) {
           if(
               $i > 0 &&
               (
                   $current_page <= $offset_count + $offset_start + 1 || $i >= $current_page - $offset_count
               )

           ) {
               $middle_pages[$i] = 0;
           }
        }

        $middle_pages[$current_page] = 1;

        for ($i = $current_page + 1; $i < $current_page + $offset_count + $offset_end + 1; $i++) {
            if(
                $i <= $total_pages &&
                (
                    $current_page >= $total_pages - $offset_count - $offset_end || $i <= $current_page + $offset_count
                )
            ){
                $middle_pages[$i] = 0;
            }
        }

        // Calculate first pages
        if ($current_page > $offset_count + $offset_start + 1){
            for($i = 1; $i < $offset_start + 1; $i++){
                $first_pages[] = $i;
            }
        }

        // Calculate end pages
        if ($current_page < $total_pages - $offset_end - $offset_count){
            for($i = $total_pages - $offset_end + 1; $i <= $total_pages; $i++){
                $last_pages[] = $i;
            }
        }

        // Set next/prev and first/last pages
        ($current_page < $total_pages) ? $this->setNextPage($current_page + 1) : $this->setNextPage($total_pages);
        ($current_page > 1) ? $this->setPreviousPage($current_page - 1) : $this->setPreviousPage(1);

        $this->setFirstPage(1);
        $this->setLastPage($total_pages);

        // Set pages
        $this->setFirstPages($first_pages);
        $this->setMiddlePages($middle_pages);
        $this->setLastPages($last_pages);

        // Unset vars
        unset($first_pages);
        unset($middle_pages);
        unset($last_pages);

        return $this;
    }

    /**
     * Render view
     * @param null $view
     * @return string
     */
    public function render($view = null) {
        if(!is_null($view)) $this->setView($view);
        $template = Twig::factory(
            $this->getView(),
            array(
                'pagination' => $this,
                'first_pages' => $this->getFirstPages(),
                'middle_pages' => $this->getMiddlePages(),
                'last_pages' => $this->getLastPages(),
                'previous_page' => $this->getPreviousPage(),
                'next_page' => $this->getNextPage(),
                'first_page' => $this->getFirstPage(),
                'last_page' => $this->getLastPage()
            )
        );

        return $template->render();
    }
}