<?php defined('SYSPATH') or die('No direct script access.');

class Pagination_Simple extends Pagination {
    /**
     * Calculate pagination
     * @return $this
     */
    public function execute() {
        if ($this->getTotalPages() < 2) {
            return $this;
        }

        if (!$this->getCurrentPage() || $this->getCurrentPage() < 1) {
            $this->setCurrentPage(1);
        }

        if ($this->getCurrentPage() > $this->getTotalPages()) {
            $this->setCurrentPage( $this->getTotalPages() );
        }

        // Get current parameters
        $current_page = $this->getCurrentPage();
        $total_pages = $this->getTotalPages();

        // Set next/prev and first/last pages
        ($current_page < $total_pages) ? $this->setNextPage($current_page + 1) : $this->setNextPage($total_pages);
        ($current_page > 1) ? $this->setPreviousPage($current_page - 1) : $this->setPreviousPage(1);

        $this->setFirstPage(1);
        $this->setLastPage($total_pages);

        return $this;
    }

    /**
     * Render view
     * @param null $view
     * @return string
     */
    public function render($view = null) {
        if(!is_null($view)) $this->setView($view);
        $template = Twig::factory(
            $this->getView(),
            array(
                'pagination' => $this,
                'total_pages' => $this->getTotalPages(),
                'current_page' => $this->getCurrentPage(),
                'previous_page' => $this->getPreviousPage(),
                'next_page' => $this->getNextPage(),
                'first_page' => $this->getFirstPage(),
                'last_page' => $this->getLastPage()
            )
        );

        return $template->render();
    }
}