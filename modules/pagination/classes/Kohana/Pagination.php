<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Kohana_Pagination
 */
abstract class Kohana_Pagination {
    /**
     * @var string
     */
    protected $_type = 'simple';

    /**
     * @var string
     */
    protected $_source = 'route';

    /**
     * @var string
     */
    protected $_key = 'page';

    /**
     * @var string
     */
    protected $_view = 'Pagination';

    /**
     * @var Request
     */
    protected $_request;

    /**
     * @var Route
     */
    protected $_route;

    /**
     * @var array
     */
    protected $_params;

    /**
     * @var array
     */
    protected $_query;

    /**
     * @var int
     */
    protected $_current_page;

    /**
     * @var int
     */
    protected $_total_pages = 1;

    /**
     * @var int
     */
    protected $_total_items = 0;

    /**
     * @var int
     */
    protected $_perpage = 10;

    /**
     * @var int
     */
    protected $_offset = 0;

    /**
     * @var int
     */
    protected $_previous_page;

    /**
     * @var int
     */
    protected $_next_page;

    /**
     * @var int
     */
    protected $_first_page;

    /**
     * @var int
     */
    protected $_last_page;

    /**
     * @param int $current_page
     * @return $this
     */
    public function setCurrentPage($current_page) {
        $this->_current_page = ($current_page) ? $current_page : 1;

        $offset = ($current_page - 1) * $this->_perpage;
        $this->setOffset($offset);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage() {
        return $this->_current_page;
    }

    /**
     * @param int $first_page
     * @return $this
     */
    public function setFirstPage($first_page) {
        $this->_first_page = $first_page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstPage() {
        return $this->_first_page;
    }

    /**
     * @param int $last_page
     * @return $this
     */
    public function setLastPage($last_page) {
        $this->_last_page = $last_page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastPage() {
        return $this->_last_page;
    }

    /**
     * @param int $next_page
     * @return $this
     */
    public function setNextPage($next_page) {
        $this->_next_page = $next_page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextPage() {
        return $this->_next_page;
    }

    /**
     * @param int $perpage
     * @throws Exception
     * @return $this
     */
    public function setPerpage($perpage) {
        $this->_perpage = $perpage;
        if(!empty($this->_perpage)) {
            $total = ceil($this->_total_items / $this->_perpage);
            if(!empty($total)) {
                $this->setTotalPages($total);
            }
        } else {
            throw new Exception('Pagination: perpage cannot be empty');
        }

        // Set offset
        if($this->_current_page) {
            $offset = ($this->_current_page - 1) * $this->_perpage;
            $this->setOffset($offset);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerpage() {
        return $this->_perpage;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset) {
        $this->_offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset() {
        return $this->_offset;
    }

    /**
     * @param int $previous_page
     * @return $this
     */
    public function setPreviousPage($previous_page) {
        $this->_previous_page = $previous_page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousPage() {
        return $this->_previous_page;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource($source) {
        $this->_source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource() {
        return $this->_source;
    }

    /**
     * @param string $key
     */
    public function setKey($key) {
        $this->_key = $key;
    }

    /**
     * @return string
     */
    public function getKey() {
        return $this->_key;
    }

    /**
     * @param int $total_pages
     * @return $this
     */
    public function setTotalPages($total_pages) {
        $this->_total_pages = $total_pages;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPages() {
        return $this->_total_pages;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * @param string $view
     * @return $this
     */
    public function setView($view) {
        $this->_view = $view;
        return $this;
    }

    /**
     * @return string
     */
    public function getView() {
        return $this->_view;
    }

    /**
     * @param \Request $request
     * @return $this
     */
    public function setRequest($request) {
        $this->_request = $request;
        return $this;
    }

    /**
     * @return \Request
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * @param \Route $route
     */
    public function setRoute($route) {
        $this->_route = $route;
    }

    /**
     * @return \Route
     */
    public function getRoute() {
        return $this->_route;
    }

    /**
     * @param array $param
     */
    public function setParams($param) {
        $this->_params = $param;
    }

    /**
     * @return array
     */
    public function getParams() {
        return $this->_params;
    }

    /**
     * @param array $query
     */
    public function setQuery($query) {
        $this->_query = $query;
    }

    /**
     * @return array
     */
    public function getQuery() {
        return $this->_query;
    }

    /**
     * @param int $total_items
     */
    public function setTotalItems($total_items) {
        $this->_total_items = $total_items;
        $total = ceil($this->_total_items / $this->_perpage);
        if(!empty($total)) {
            $this->setTotalPages($total);
        }
    }

    /**
     * @return int
     */
    public function getTotalItems() {
        return $this->_total_items;
    }

    /**
     * Params setter
     * @param $params
     */
    public function setProperties($params){
        foreach($params as $param => $value){
            $parts = explode('_',$param);
            $parts = array_map('ucfirst', $parts);
            $name = implode('', $parts);

            $setter = "set$name";
            if(method_exists($this, $setter)){
                $this->{$setter}($value);
            }
        }
    }

    /**
     * Params getter
     * @return array
     */
    public function getProperties(){
        $reflect = new ReflectionClass($this);
        $params = array();
        foreach($reflect->getProperties() as $property){
            $param = $property->getName();
            $parts = explode('_',$param);
            $parts = array_map('ucfirst', $parts);
            $name = implode('', $parts);

            $getter = "get$name";

            if(method_exists($this, $getter)){
                $params[$param] = $this->{$getter}();
            }

        }
        return $params;
    }

    public static function factory(array $params = array(), $type = 'simple') {
        $parts = explode('_', $type);
        $parts = array_map('ucfirst', $parts);
        $classname = 'Pagination_'.implode('_', $parts);

        if(!class_exists($classname)) {
            throw new Exception('Pagination: class not exists');
        }

        /** @var $pagination Pagination */
        $pagination = new $classname();

        // Load default config
        $config = Kohana::$config->load('pagination')->get($type);
        if(empty($config)) {
            throw new Exception('Pagination: unknown pagination type');
        } else{
            // Set standart parameters
            $pagination->setProperties($config);
        }

        $pagination->setProperties($params);
        $pagination->setType($type);

        // Set current page and offset
        if($pagination->getCurrentPage() === NULL) {
            $request = $pagination->getRequest();
            $source = $pagination->getSource();

            switch($source) {
                case 'filter':
                    $current_page = Arr::get($request->param('filter'), $pagination->getKey(), 1);
                    break;

                case 'route':
                    $current_page = Arr::get($request->param(), $pagination->getKey(), 1);
                    break;

                case 'query':
                    $current_page = Arr::get($request->query(), $pagination->getKey(), 1);
                    break;
            }
            $pagination->setCurrentPage($current_page);
            unset($request);
        }

        return $pagination;
    }

    public function __construct() {
        if($this->getRequest() === NULL) $this->setRequest(Request::$current);
        if($this->getRoute() === NULL) $this->setRoute(Request::$current->route());
        if($this->getParams() === NULL) {
            $params = Request::$current->param();
            $params['controller'] = Request::$current->controller();
            $params['action'] = Request::$current->action();
            $params['page_col'] = (isset($_COOKIE['pagination']) ? $_COOKIE['pagination'] : 10);

            $this->setParams($params);
            unset($params);
        }
        if($this->getQuery() === NULL) {
            $query = Request::$current->query();
            $this->setQuery($query);
            unset($query);
        }
    }

    public function url($page = 1){
        if($page > $this->getTotalPages()) $page = $this->getTotalPages();
        if($page < 1) $page = 1;

        $request = $this->getRequest();
        $route = $this->getRoute();
        $key = $this->getKey();

        // Get route params
        $params = $this->getParams();

        // Get query params
        $query = $this->getQuery();
        switch($this->getSource())
        {
            case 'filter': {
                $params['filter'][$key] = $page;
                break;
            }

            case 'route': {
                $params[$key] = $page;
                break;
            }

            case 'query': {
                $query[$key] = $page;
                break;
            }
        }

        $uri = $route->uri($params);
        if($query){
            $uri .= '?'.http_build_query($query);
        }
        return '/'.$uri;
    }

    abstract public function execute();
    abstract public function render($view = null);
}