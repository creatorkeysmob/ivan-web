
/**
* <?php echo ucfirst($class_type), ' ', $name ?>.
*/
<?php
if ( ! empty($abstract)) {echo 'abstract ';}
echo $class_type, ' ', $name;
if ( ! empty($extends)) {echo ' extends ', $extends;}
if ( ! empty($implements)) {echo ' implements ', $implements;}
if ( ! empty($blank)) {echo ' {}';} else { ?> {
    public function initElements() {
        $this->_elements += array(
            <? foreach($columns as $_name => $_property): ?>
            '<?=$_name?>' => <?=var_export($_property, true)?>,
            <? endforeach;?>
        );
        parent::initElements();
    }
}
<?php } ?> 
