
/**
* <?php echo ucfirst($class_type), ' ', $name ?>.
*/
<?php
if ( ! empty($abstract)) {echo 'abstract ';}
echo $class_type, ' ', $name;
if ( ! empty($extends)) {echo ' extends ', $extends;}
if ( ! empty($implements)) {echo ' implements ', $implements;}
if ( ! empty($blank)) {echo ' {}';} else { ?> {
    public function initElements() {
        $this->_elements += array(
            array(
                'type'      => 'section',
                'label'     => 'Основная информация',
                'view'      => 'open'
            ),
            <? foreach($columns as $_name => $_property) { ?>
                '<?=$_name?>' => <?=var_export($_property, true)?>,
            <?php } ?>
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            <? if(is_array($seo) && !empty($seo)) { ?>
            array(
                'type'      => 'divider',
            ),
            array(
                'type'      => 'section',
                'label'     => 'Продвижение',
                'view'      => 'open'
            ),
            <? foreach($seo as $_name => $_property){ ?>
                '<?=$_name?>' => <?=var_export($_property, true)?>,
            <?php } ?>
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            <?php } ?>
        );
        parent::initElements();
    }
}
<?php } ?> 
