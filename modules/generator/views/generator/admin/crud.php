
/**
* <?php echo ucfirst($class_type), ' ', $name ?>.
*/
<?php
if ( ! empty($abstract)) {echo 'abstract ';}
echo $class_type, ' ', $name;
if ( ! empty($extends)) {echo ' extends ', $extends;}
if ( ! empty($implements)) {echo ' implements ', $implements;}
if ( ! empty($blank)) {echo ' {}';} else { ?> {
    public $moduleName = '<?php echo $self;?>';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('<?php echo $model;?>');
        $this->_crud_form = Former_Form::factory('Admin_Form_<?php echo $form;?>');

        // List properties
        $this->_list_properties = array(
            'sort' => array( 'head' => 'ASC' ),
            'fields' => array(
                array( 'name' => 'head', 'label' => 'Название' ),
            ),
            'custom' => array(
                'sortable' => true,
                'pagination' => false,
                'drop' => true,
                'hide' => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/<?php echo $self;?>');
    }
}
<?php } ?> 
