
/**
* <?php echo ucfirst($class_type), ' ', $name ?>.
<?php foreach($property as $_prop) { ?>
* @property <?=$_prop['type']?> <?=$_prop['name']."\n"?>
<?php } ?>
*/
<?php
echo $class_type, ' ', $name;
if ( ! empty($extends)) {echo ' extends ', $extends;}
if ( ! empty($implements)) {echo ' implements ', $implements;}
if ( ! empty($blank)) {echo ' {}';} else { ?> {
    protected $_table_name = '<?=$table?>';
    <?php if($alias) { ?>

    /**
     * Generate alias
     * @return string
     */
    protected function _alias()
    {
        $alias = '';
        if ($this->head OR $this->alias) {
            if ($this->alias) {
                $alias = $this->alias;
            } elseif ($this->head) {
                $alias = $this->head;
            }

            // Generate unique
            $alias = trim(Helper_Text::translit(mb_strtolower($alias)));
            $alias = substr($alias, 0, 60);
            $alias = preg_replace('/[^a-z0-9_]+/', '', $alias);

            $exist = DB::select()->from($this->table_name())
                ->where('<?=$alias?>', '=', $alias)
                ->and_where('id', '<>', intval($this->id))
                ->execute()->count();

            while ($exist) {
                $postfix = substr(md5(uniqid(rand(), 1)), 0, 10);
                $exist = DB::select()->from($this->table_name())
                    ->where('<?=$alias?>', '=', $alias . '_' . $postfix)
                    ->and_where('id', '<>', intval($this->id))
                    ->execute()->count();

                if (!$exist) {
                    $alias .= '_' . $postfix;
                }
            }
        }

        return $alias;
    }

    public function save(Validation $validation = NULL)
    {
        $this->alias = $this->_alias();
        return parent::save($validation);
    }
    <?php } ?>
    <?php if($img) { ?>

    public function delete()
    {
        // Remove images
        if ($this->loaded() && !empty($this-><?=$img?>)) {
            Thumb::delete($this-><?=$img?>, TRUE);
        }
        return parent::delete();
    }

    <?php } ?>

}
<?php } ?>