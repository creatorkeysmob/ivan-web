
/**
 * <?php echo ucfirst($class_type), ' ', $name ?>.
 */
<?php 
	if ( ! empty($abstract)) {echo 'abstract ';}
	echo $class_type, ' ', $name;
	if ( ! empty($extends)) {echo ' extends ', $extends;}
	if ( ! empty($implements)) {echo ' implements ', $implements;}
	if ( ! empty($blank)) {echo ' {}';} else { ?> 
{
<?php if ( ! empty($traits)): foreach ($traits as $trait): ?>
	// Trait: <?php echo $trait ?> 
	<?php echo 'use ', $trait, ';' ?> 

<?php endforeach; ?>
<?php endif; ?>

<?php foreach (array('static', 'public', 'abstract', 'other') as $group): ?>
<?php if (isset($methods[$group])) foreach ($methods[$group] as $method => $m): ?>
	<?php echo $m['doccomment'] ?> 
	<?php echo $m['signature'] ?>
<?php if ($m['abstract']): echo ';', PHP_EOL, PHP_EOL; else: ?> 
	{
		<?php echo $m['body'] ?> 
	}

<?php endif; ?>
<?php endforeach; ?>
<?php endforeach; ?>
} // End <?php echo $name ?>
<?php } ?> 
