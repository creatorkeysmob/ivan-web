<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Generator_Task_Generate_Admin_Entity
 */
class Generator_Task_Generate_Admin_Entities extends Task_Generate
{
	/**
	 * @var  array  The task options
	 */
	protected $_options = array(
        'extended' => true
    );

	/**
	 * @var  array  Arguments mapped to options
	 */
	protected $_arguments = array(
		1 => 'extended',
	);

	/**
	 * Validates the task options.
	 *
	 * @param   Validation  $validation  The validation object to add rules to
	 * @return  Validation
	 */
	public function build_validation(Validation $validation)
	{
		return parent::build_validation($validation);
	}

	/**
	 * Creates a generator builder with the given configuration options.
	 *
	 * @param   array  $options  The selected task options
	 * @return  Generator_Builder
	 */
	public function get_builder(array $options) {
        $tablePrefix = Database::instance()->table_prefix();
        $tablesList = DB::query(
            Database::SELECT, 'SHOW TABLES'
        )->execute()->as_array(null);

        // Process values, remove prefix
        $tablesList = array_map(
            function($e) use($tablePrefix){
                $tableName = reset($e);
                if($tablePrefix && (substr($tableName, 0, strlen($tablePrefix)) == $tablePrefix)) {
                    $tableName = substr($tableName, strlen($tablePrefix));
                }
                return $tableName;
            },
            $tablesList
        );

        function depluralize($word){
            // Here is the list of rules. To add a scenario,
            // Add the plural ending as the key and the singular
            // ending as the value for that key. This could be
            // turned into a preg_replace and probably will be
            // eventually, but for now, this is what it is.
            //
            // Note: The first rule has a value of false since
            // we don't want to mess with words that end with
            // double 's'. We normally wouldn't have to create
            // rules for words we don't want to mess with, but
            // the last rule (s) would catch double (ss) words
            // if we didn't stop before it got to that rule.
            $rules = array(
                'ss' => false,
                'os' => 'o',
                'ies' => 'y',
                'xes' => 'x',
                'oes' => 'o',
                'ies' => 'y',
                'ves' => 'f',
                's' => '');
            // Loop through all the rules and do the replacement.
            foreach(array_keys($rules) as $key){
                // If the end of the word doesn't match the key,
                // it's not a candidate for replacement. Move on
                // to the next plural ending.
                if(substr($word, (strlen($key) * -1)) != $key)
                    continue;
                // If the value of the key is false, stop looping
                // and return the original version of the word.
                if($key === false)
                    return $word;
                // We've made it this far, so we can do the
                // replacement.
                return substr($word, 0, strlen($word) - strlen($key)) . $rules[$key];
            }
            return $word;
        }

        // Generate model names
        $tableNames = array_map(
            function($e) {
                $nameParts = explode('_', $e);
                $nameParts = array_map(
                    function($t){
                        if(!in_array($t, array('news'))) {
                            $t = depluralize($t);
                        }
                        return ucfirst($t);
                    },
                    $nameParts
                );

                return implode('_', $nameParts);
            },
            $tablesList
        );

        // Run minion subtasks
        foreach($tablesList as $_key => $_table) {
            $generateImageMethod = FALSE;
            $generateAliasMethod = FALSE;

            if($options['extended']) {
                $tableColumns = DB::query(
                    Database::SELECT, 'SHOW COLUMNS FROM :table'
                )->parameters(
                    array(
                        ':table' => DB::expr($tablePrefix.$_table)
                    )
                )->execute()->as_array('Field','Type');

                if(array_key_exists('alias', $tableColumns)) $generateAliasMethod = TRUE;
                if(array_key_exists('img', $tableColumns)) $generateImageMethod = TRUE;
                unset($tableColumns);
            }

            // Run minion task
            Minion_Task::factory(array(
                'task' => 'generate:admin:entity',
                'name' => $tableNames[$_key],
                'table' => $tablesList[$_key],
                'no-ask' => TRUE,
                'alias' => ($generateAliasMethod) ? 'alias' : '',
                'img' => ($generateImageMethod) ? 'img' : '',
            ))->execute();
        }

        /** @noinspection PhpUndefinedClassInspection */
        $builder = Generator::build()->builder();
        return $builder
            ->with_pretend($options['pretend'])
            ->with_force($options['force']);
	}

} // End Task_Generate_Crud
