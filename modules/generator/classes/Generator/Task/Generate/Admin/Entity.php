<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Generator_Task_Generate_Admin_Entity
 */
class Generator_Task_Generate_Admin_Entity extends Task_Generate
{
	/**
	 * @var  array  The task options
	 */
	protected $_options = array(
		'name' => '',
		'table' => '',
		'alias' => '',
		'img' => '',
		'extend' => 'Model_Abstract',
	);

	/**
	 * @var  array  Arguments mapped to options
	 */
	protected $_arguments = array(
		1 => 'name',
		2 => 'table',
		3 => 'alias',
		4 => 'img',
	);

	/**
	 * Validates the task options.
	 *
	 * @param   Validation  $validation  The validation object to add rules to
	 * @return  Validation
	 */
	public function build_validation(Validation $validation)
	{
		return parent::build_validation($validation)
			->rule('name', 'not_empty')
			->rule('table', 'not_empty');
	}

	/**
	 * Creates a generator builder with the given configuration options.
	 *
	 * @param   array  $options  The selected task options
	 * @return  Generator_Builder
	 */
	public function get_builder(array $options){

        $table = $options['table'];
        $alias = $options['alias'];
        $img = $options['img'];

        $property = array();

        // Get table columns
        $columns = DB::query(
            Database::SELECT, 'SHOW COLUMNS FROM :table'
        )->parameters(
            array(
                ':table' => DB::expr(Database::instance()->table_prefix().$table)
            )
        )->execute()->as_array('Field','Type');

        // Process table columns
        foreach($columns as $_field => $_type) {
            $type = preg_replace('/(\(\d+\))/suiU','',$_type);
            $type = trim($type);
            switch($type){
                case 'int':
                case 'smallint':
                case 'tinyint':
                    $type = 'int';
                    break;

                case 'text':
                case 'varchar':
                case 'datetime':
                case 'timestamp':
                    $type = 'string';
                    break;

                default:
                    $type = 'mixed';
            }

            $property[] = array(
                'name' => $_field,
                'type' => $type
            );
        }

        /** @noinspection PhpUndefinedClassInspection */
        $builder = Generator::build()
            ->add_model($options['name'])
                ->extend($options['extend'])
                ->template('generator/admin/entity')
                ->set('property', $property)
                ->set('table', $table)
                ->set('alias', $alias)
                ->set('img', $img)
            ->builder();

        return $builder
            ->with_pretend($options['pretend'])
            ->with_force($options['force']);
	}

} // End Task_Generate_Crud
