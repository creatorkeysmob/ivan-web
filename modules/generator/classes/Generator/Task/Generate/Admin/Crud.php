<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Generator_Task_Generate_Admin_Crud
 */
class Generator_Task_Generate_Admin_Crud extends Task_Generate
{
    /**
     * @var  array  The task options
     */
    protected $_options = array(
        'name' => '',
        'model' => '',
        'form' => '',
    );

    /**
     * @var  array  Arguments mapped to options
     */
    protected $_arguments = array(
        1 => 'name',
        2 => 'model',
        3 => 'form',
    );

    /**
     * Validates the task options.
     *
     * @param   Validation  $validation  The validation object to add rules to
     * @return  Validation
     */
    public function build_validation(Validation $validation)
    {
        return parent::build_validation($validation)
            ->rule('name', 'not_empty')
            ->rule('model', 'not_empty')
            ->rule('form', 'not_empty');
    }

    /**
     * Creates a generator builder with the given configuration options.
     *
     * @param   array  $options  The selected task options
     * @return  Generator_Builder
     */
    public function get_builder(array $options){
        $self = strtolower($options['name']);

        $classNameOriginal = $options['name'];
        $modelNameOriginal = $options['model'];
        $formNameOriginal = $options['form'];

        $classNameDirectory = explode('_', $classNameOriginal);
        $classNameDirectory = array_map('ucfirst', $classNameDirectory);
        $classNameDirectory = implode(DIRECTORY_SEPARATOR, $classNameDirectory);

        // Config content
        $config = "template:\n";
        $config .= "    text:\n";
        $config .= "        add: Добавить элемент\n";
        $config .= "        create: Новый элемент\n";
        $config .= "        empty: Элементы отсутствуют\n";
        $config .= "        remove: Удалить элемент\n";
        $config .= "        drop: Удалить выбранные элементы\n";

        // List template
        $list = "{% extends \"Admin/Crud/List\" %}\n\n";
        $list .= "{% block title %}Администрирование | ".$classNameOriginal."{% endblock %}";

        // Edit template
        $edit = "{% extends \"Admin/Crud/Edit\" %}\n\n";
        $edit .= "{% block title %}Администрирование | ".$classNameOriginal."{% endblock %}";

        Minion_Task::factory(array(
            'task' => 'generate:admin:former',
            'name' => $formNameOriginal,
            'model' => $modelNameOriginal,
            'no-ask' => TRUE,
        ))->execute();

        /** @noinspection PhpUndefinedClassInspection */
        $builder = Generator::build()
            ->add_class('Admin_Controller_'.$classNameOriginal)
                ->extend('Admin_Controller_Crud')
                ->module('admin')
                ->template('generator/admin/crud')
                ->set('self', $self)
                ->set('model', $modelNameOriginal)
                ->set('form', $formNameOriginal)
            ->add_file('config/admin/'.strtolower($classNameDirectory).'.yml')
                ->module('admin')
                ->content($config)
            ->add_file('views/Admin/'.$classNameDirectory.'/List.twig')
                ->module('admin')
                ->content($list)
            ->add_file('views/Admin/'.$classNameDirectory.'/Edit.twig')
                ->module('admin')
                ->content($edit)
            ->builder();

        return $builder
            ->with_pretend($options['pretend'])
            ->with_force($options['force']);
    }

} // End Task_Generate_Crud
