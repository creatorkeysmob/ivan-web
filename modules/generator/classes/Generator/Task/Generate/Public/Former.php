<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Generator_Task_Generate_Public_Former
 */
class Generator_Task_Generate_Public_Former extends Task_Generate
{
    /**
     * @var  array  The task options
     */
    protected $_options = array(
        'name' => '',
        'model' => ''
    );

    /**
     * @var  array  Arguments mapped to options
     */
    protected $_arguments = array(
        1 => 'name',
        2 => 'model'
    );

    /**
     * Validates the task options.
     *
     * @param   Validation  $validation  The validation object to add rules to
     * @return  Validation
     */
    public function build_validation(Validation $validation)
    {
        return parent::build_validation($validation)
            ->rule('name', 'not_empty');
    }

    /**
     * Creates a generator builder with the given configuration options.
     *
     * @param   array  $options  The selected task options
     * @return  Generator_Builder
     */
    public function get_builder(array $options){
        $skipColumnNames = array('id', 'hide', 'priority');
        $defaultColumnMapping = array(
            'head' => array(
                'label'     => 'Название',
                'type'      => 'text',
                'required'  => true,
                'rules'     => array(
                    array('not_empty'),
                )
            ),
            'alias' => array(
                'label'     => 'Псевдоним',
                'type'      => 'text',
            ),
            'created' => array(
                'label'     => 'Дата публикации',
                'type'      => 'datetime',
            ),
            'body' => array(
                'label'     => 'Основной текст',
                'type'      => 'wysiwyg',
            ),
            'img' => array(
                'label'     => 'Изображение',
                'type'      => 'image',
            ),
            'title' => array(
                'label'     => 'Title страницы',
                'type'      => 'text',
            ),
            'description' => array(
                'label'     => 'Description страницы',
                'type'      => 'textarea',
            ),
            'keywords' => array(
                'label'     => 'Keywords страницы',
                'type'      => 'textarea',
            ),
        );

        $columnMapping = array();
        if($options['model']) {
            $modelInstance = ORM::factory($options['model']);
            $modelColumns = $modelInstance->table_columns();
            foreach($modelColumns as $_key => $_property) {
                if(in_array($_key, $skipColumnNames)) continue;
                if(isset($defaultColumnMapping[$_key])) {
                    $columnMapping[$_key] = $defaultColumnMapping[$_key];
                    continue;
                }

                $labelParts = preg_split('/[\_\-\s]+/suiU', $_key);
                $labelParts = array_map('ucfirst', $labelParts);

                $columnMapping[$_key] = array(
                    'label'     => implode(' ', $labelParts),
                    'type'      => 'text',
                );

                switch($_property['data_type']) {
                    case 'bigint':
                    case 'tinyint':
                    case 'smallint':
                    case 'mediumint':
                    case 'integer':
                    case 'int':
                        $columnMapping[$_key]['type'] = 'integer';
                        break;

                    case 'float':
                    case 'double':
                        $columnMapping[$_key]['type'] = 'double';
                        break;

                    case 'date':
                    case 'datetime':
                    case 'timestamp':
                        $columnMapping[$_key]['type'] = 'datetime';
                        break;

                    case 'tinytext':
                    case 'mediumtext':
                    case 'text':
                    case 'longtext':
                        $columnMapping[$_key]['type'] = 'textarea';
                        break;

                    case 'varchar':
                    default:
                        $columnMapping[$_key]['type'] = 'text';
                        break;
                }
            }
        }

        /** @noinspection PhpUndefinedClassInspection */
        $builder = Generator::build()
            ->add_class('Public_Form_'.$options['name'])
            ->extend('Former_Form')
            ->module('public')
            ->template('generator/public/form')
            ->set('columns', $columnMapping)
            ->builder();

        return $builder
            ->with_pretend($options['pretend'])
            ->with_force($options['force']);
    }

} // End Task_Generate_Crud
