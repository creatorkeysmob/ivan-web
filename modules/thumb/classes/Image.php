<?php defined('SYSPATH') OR die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
abstract class Image extends Kohana_Image {
    /**
     * Apply filter to image
     *
     *     // Grayscale image
     *     $image->filter(IMG_FILTER_GRAYSCALE);
     *
     * @param $name - PHP Filter Constants
     * @param array $params
     * @return  $this
     * @uses    Image::_do_filter
     */
    public function filter($name, $params = array())
    {
        $this->_do_filter($name, $params);
        return $this;
    }

    /**
     * Execute a filter.
     *
     * @param $name
     * @param array $params
     * @return  void
     */
    protected function _do_filter($name, $params = array()){}
}
