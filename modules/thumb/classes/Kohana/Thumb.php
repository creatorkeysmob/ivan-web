<?php defined('SYSPATH') OR die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;

class Kohana_Thumb {
    // Constants
    const THUMB_CONFIG_ACTIONS = 'actions';
    const THUMB_CONFIG_FILTERS = 'filters';

    // Actions
    CONST THUMB_RESIZE_ACTION = 'resize';
    CONST THUMB_CROP_ACTION = 'crop';
    CONST THUMB_WATERMARK_ACTION = 'watermark';

    // Filters
    CONST THUMB_GRAYSCALE_FILTER = 'grayscale';

    /**
     * Kohana image instance
     * @var Image
     */
    protected $_image;

    /**
     * Thumb config
     * @var array
     */
    protected $_config = array();


    /**
     * New flag
     * @var bool
     */
    protected $_new = FALSE;

    /**
     * Original image path
     * @var
     */
    protected $_original_path;

    /**
     * Watermark image path
     * @var
     */
    protected $_watermark_path;

    /**
     * Thumb image path
     * @var
     */
    protected $_thumb_path;

    /**
     * Allowed image actions
     * @var array
     */
    protected $_allowed_actions = array(
        self::THUMB_RESIZE_ACTION,
        self::THUMB_CROP_ACTION,
        self::THUMB_WATERMARK_ACTION
    );

    /**
     * Allowed image filters
     * @var array
     */
    protected $_allowed_filters = array(
        self::THUMB_GRAYSCALE_FILTER
    );

    /**
     * Skip attributes from render
     * @var array
     */
    protected $_skip_attributes = array(
        'link', 'image', 'absolute'
    );

    /**
     * @return array
     */
    public function getConfig() {
        return $this->_config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config) {
        $actions = (is_array($config['actions']) && !empty($config['actions'])) ? $config['actions'] : array();
        $attributes = (is_array($config['attributes']) && !empty($config['attributes'])) ? $config['attributes'] : array();
        $filters = (is_array($config['filters']) && !empty($config['filters'])) ? $config['filters'] : array();

        // Process image actions
        $result = array();
        foreach ( $actions as $data ) {
            $params = explode( '_', $data );
            $action = array_shift( $params );

            if(!in_array($action, $this->_allowed_actions)) continue;

            $classname = 'Thumb_Action_' . ucfirst($action);
            if(class_exists($classname)) {
                /** @var $object Thumb_Action */
                $object = new $classname( $params, $attributes );
                $result[$action] = $object->getParams();
                unset($object);
            }
        }

        // Clear reserved attributes
        if ( isset( $attributes['image'] ) ) unset( $attributes['image'] );

        // Set config
        $this->_config = array(
            'actions' => $result,
            'attributes' => $attributes,
            'filters' => $filters
        );
    }

    /**
     * @return Image
     */
    public function getImage() {
        return $this->_image;
    }

    /**
     * @param Image $image
     * @return $this
     */
    public function setImage($image) {
        $this->_image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbPath() {
        return $this->_thumb_path;
    }

    /**
     * @param mixed $path
     * @return $this
     */
    public function setThumbPath($path) {
        $this->_thumb_path = $path;
        return $this;
    }

    /**
     * Generate thumbnail path
     * @return mixed
     */
    public function generateThumbPath() {
        $filesystem = new Filesystem();
        $fileinfo = pathinfo($this->getOriginalPath());

        $original_path = $fileinfo['dirname'];
        $thumb_path = str_replace(
            DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR,
            DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR,
            $original_path
        );

        if( !$filesystem->exists($thumb_path) ) {
            $filesystem->mkdir($thumb_path);
        }

        // Apply actions
        $prefix = '';
        foreach ($this->_config[self::THUMB_CONFIG_ACTIONS] as $_action => $_params) {
            if(!in_array($_action, $this->_allowed_actions)) continue;

            switch ($_action) {
                case self::THUMB_RESIZE_ACTION:
                {
                    $prefix .= $_params['width'] . 'x' . $_params['height'] . '_';
                    break;
                }
                case self::THUMB_CROP_ACTION:
                {
                    $prefix .= $_params['width'] . 'xx' . $_params['height'] . '_' . $_params['offset_x'] . 'x' . $_params['offset_y'] . '_';
                    break;
                }
                case self::THUMB_WATERMARK_ACTION:
                {
                    break;
                }
            }
        }

        // Apply filters
        foreach ($this->_config[self::THUMB_CONFIG_FILTERS] as $_filter) {
            if(!in_array($_filter, $this->_allowed_filters)) continue;

            switch ($_filter) {
                case self::THUMB_GRAYSCALE_FILTER:
                {
                    $prefix .=  self::THUMB_GRAYSCALE_FILTER.'_';
                    break;
                }
            }
        }


        $thumb_path = $thumb_path . '/' . $prefix . '_' . $fileinfo['basename'];
        if( !$filesystem->exists($thumb_path) ) {
            $this->setNew(TRUE);
        }
        return $thumb_path;
    }

    /**
     * @return mixed
     */
    public function getOriginalPath() {
        return $this->_original_path;
    }

    /**
     * @param $path
     * @return $this
     */
    public function setOriginalPath($path) {
        $filesystem = new Filesystem();
        $path = realpath(DOCROOT . $path);

        if( $filesystem->exists($path) ) {
            $this->_original_path = $path;
        } else {
            $path_info = pathinfo($path);
            $directory = $path_info['dirname'];

            if( !$filesystem->exists($directory) ) {
                $filesystem->mkdir($directory);
            }
            $this->_original_path = NULL;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWatermarkPath() {
        return $this->_watermark_path;
    }

    /**
     * @param $path
     * @return $this
     */
    public function setWatermarkPath($path) {
        $filesystem = new Filesystem();
        $path = realpath(DOCROOT . $path);
        if( $filesystem->exists($path) ) {
            $this->_watermark_path = $path;
        } else {
            $path_info = pathinfo($path);
            $directory = $path_info['dirname'];

            if( !$filesystem->exists($directory) ) {
                $filesystem->mkdir($directory);
            }
            $this->_watermark_path = NULL;
        }
        return $this;
    }

    /**
     * @param boolean $new
     * @return $this
     */
    public function setNew($new) {
        $this->_new = $new;
        return $this;

    }

    /**
     * @return boolean
     */
    public function getNew() {
        return $this->_new;
    }

    /**
     * @return bool
     */
    public function isNew(){
        return ($this->_new) ? TRUE : FALSE;
    }

    /**
     * Base factory
     * @param string $file
     * @param array $config
     * @param string $driver
     * @return Model_Thumb
     */
    public static function factory($file, $config = array(), $driver = 'GD') {
        $thumb = new Thumb();

        // Set parameters
        $thumb->setOriginalPath($file);
        $thumb->setConfig($config);

        // Generate thumb path
        $path = $thumb->generateThumbPath();
        $thumb->setThumbPath($path);

        // Create image instance
        $image = Image::factory($thumb->getOriginalPath(), $driver);
        $thumb->setImage($image);
        unset($image);

        if ($thumb->isNew()) {
            $thumb->create();
        }

        return $thumb;
    }

    /**
     * Render thumbnail
     * @return mixed
     */
    public function render() {
        // Get thumbnail path
        $filesystem = new Filesystem();
        $filename = $filesystem->makePathRelative( $this->getThumbPath(), DOCROOT );
        $filename = '/' . trim($filename,'/');

        if(Arr::path($this->_config, 'attributes.absolute') == true) {
            $filename = URL::site($filename,'http');
        }

        if ( Arr::path($this->_config, 'attributes.link') == true ) {
            return $filename;
        } else {
            $attributes = '';
            if (isset($this->_config['attributes']) && is_array($this->_config)) {
                foreach ($this->_config['attributes'] as $k => $v) {
                    if(!in_array($k, $this->_skip_attributes)) {
                        $attributes .= "$k = \"$v\" ";
                    }
                }
            }
            return "<img src=\"" . $filename . "\"" . $attributes . "/>";
        }
    }

    /**
     * Create thumbnail
     */
    protected function create() {
        // Apply actions
        foreach ($this->_config [self::THUMB_CONFIG_ACTIONS] as $_action => $_params) {
            if(!in_array($_action, $this->_allowed_actions)) continue;

            switch ($_action) {
                case self::THUMB_RESIZE_ACTION:
                {
                    if( $this->_image->width <= $_params['width'] && $this->_image->height <= $_params['height'] ) continue;
                    $this->_image->resize($_params['width'], $_params['height']);
                    break;
                }
                case self::THUMB_CROP_ACTION:
                {
                    if( $this->_image->width <= $_params['width'] && $this->_image->height <= $_params['height'] ) continue;

                    $ratio = $this->_image->width / $this->_image->height;
                    $original_ratio = $_params['width'] / $_params['height'];

                    $crop_width = $this->_image->width ;
                    $crop_height = $this->_image->height;

                    if($ratio > $original_ratio) {
                        // Width crop
                        $crop_width = round($original_ratio * $crop_height);
                    }  else {
                        // Height crop
                        $crop_height = round($crop_width / $original_ratio);
                    }

                    $this->_image->crop($crop_width, $crop_height,$_params['offset_x'], $_params['offset_y'])
                        ->resize($_params['width'], $_params['height'], Image::NONE);

                    break;
                }
                case self::THUMB_WATERMARK_ACTION:
                {
                    $this->setWatermarkFile($_params['image']);
                    $this->_image->watermark(
                        Image::factory($this->getWatermarkPath()),
                        $_params['offset_x'],
                        $_params['offset_y'],
                        $_params['opacity']
                    );
                    break;
                }
            }
        }

        // Apply filters
        foreach ($this->_config[self::THUMB_CONFIG_FILTERS] as $_filter) {
            if(!in_array($_filter, $this->_allowed_filters)) continue;

            switch ($_filter) {
                case self::THUMB_GRAYSCALE_FILTER:
                {
                    $this->_image->filter(IMG_FILTER_GRAYSCALE);
                    break;
                }
            }
        }

        $this->getImage()->render();
        $this->save();
    }

    /**
     * Remove image
     * @param $filename
     * @param bool $self
     */
    public static function delete($filename, $self = FALSE) {
        $fullpath = realpath(DOCROOT.$filename);
        if (file_exists($fullpath)) {
            $original_path = $fullpath;
            $thumb_path = str_replace(
                DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR,
                DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR,
                $fullpath
            );

            $pathinfo = pathinfo($thumb_path);
            foreach (glob($pathinfo['dirname'] . '/*_' . $pathinfo['basename']) as $file) {
                unlink($file);
            }

            // Remove original image
            if($self) {
                unlink($original_path);
            }
        }
    }

    /**
     * Save image instance
     */
    public function save() {
        $path = $this->getThumbPath();
        $this->_image->save($path, 100);
    }
}