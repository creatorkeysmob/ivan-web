<?php defined('SYSPATH') OR die('No direct access allowed.');

class Thumb_Action_Watermark extends Thumb_Action {

    protected $_default = array(
        'offset_x' => true,
        'offset_y' => true,
        'opacity' => 30
    );

    public function setParams($params, $attributes) {
        if ($params != null && !empty($params)) {

            $params_arr = array();
            if (isset($params[0])) $params_arr ['offset_x'] = $params[0];
            if (isset($params[1])) $params_arr ['offset_y'] = $params[1];
            if (isset($params[2])) $params_arr ['opacity'] = $params[2];

            $this->params = array_merge($this->getDefault(), $params_arr);
        } else {
            $this->params = array_merge($this->getDefault(), $this->params);
        }
        $this->params['image'] = $attributes['image'];
    }
}