<?php defined('SYSPATH') OR die('No direct access allowed.');

class Thumb_Action_Resize extends Thumb_Action {

    protected $_default = array(
        'width' => 300,
        'height' => 300
    );

    public function setParams($params, $attributes) {
        if ($params != null && !empty($params)) {
            $params_arr = array();
            if (isset($params[0])) $params_arr ['width'] = $params[0];
            if (isset($params[1])) $params_arr ['height'] = $params[1];

            $this->params = array_merge($this->getDefault(), $params_arr);
        } else {
            $this->params = $this->getDefault();
        }
    }
}