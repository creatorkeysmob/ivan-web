<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Thumb_Action {
    /**
     * Входящие параметры для функции
     * @var
     */
    protected $params = array();

    /**
     * Стандартные входящие  параметры функции
     * @var array
     */
    protected $_default = array();

    public function __construct($params, $attributes) {
        $this->setParams($params, $attributes);
    }

    public function getDefault() {
        return $this->_default;
    }

    public function getParams() {
        return $this->params;
    }

    /**
     * Установка новых параметров для вызываемой функции
     * @param $params
     * @param $attributes
     */
    abstract public function setParams($params, $attributes);

}