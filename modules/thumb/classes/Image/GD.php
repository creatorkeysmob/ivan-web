<?php defined('SYSPATH') OR die('No direct script access.');

/** @noinspection PhpUndefinedClassInspection */
class Image_GD extends Kohana_Image_GD {
    protected function _do_filter($name, $params = array())
    {
        // Loads image if not yet loaded
        $this->_load_image();
        imagefilter($this->_image, $name);
    }
}
