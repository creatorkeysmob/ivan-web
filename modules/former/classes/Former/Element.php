<?php defined('SYSPATH') or die('No direct access allowed.');

// TODO: add model binding support for validation rules

/**
 * Abstract form element
 * Class Former_Element
 */
abstract class Former_Element extends Former_Abstract_Container {

    /**
     * Form instance
     * @var Former_Form
     */
    protected $_form = null;

    /**
     * Element name
     * @var
     */
    protected $_name;

    /**
     * Element label
     * @var
     */
    protected $_label;

    /**
     * Element theme
     * @var
     */
    protected $_type;

    /**
     * Element value
     * @var
     */
    protected $_value;

    /**
     * Element required
     * @var
     */
    protected $_required;

    /**
     * Element attributes
     * @var
     */
    protected $_attributes = array();

    /**
     * Element validate rules
     * @var array
     */
    protected $_rules = array();

    /**
     * Element theme
     * @var string
     */
    protected $_theme = 'default';

    /**
     * Element errors
     * @var array
     */
    protected $_errors = array();

    /**
     * Render element
     * @var bool
     */
    protected $_render = true;

    /**
     * Element help text
     * @var string
     */
    protected $_help;

    /**
     * Element skip save
     * @var bool
     */
    protected $_skip = FALSE;

    /**
     * @param \Former_Form $form
     * @return $this
     */
    public function setForm($form) {
        $this->_form = $form;
        return $this;
    }

    /**
     * @param $form
     * @return $this
     */
    public function bindForm(&$form) {
        $this->_form = $form;
        return $this;
    }

    /**
     * @return \Former_Form
     */
    public function getForm() {
        return $this->_form;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getAttributes($key = null) {
        if( !is_null($key) ){
            return ( isset($this->_attributes[$key])) ? $this->_attributes[$key] : '';
        }
        return $this->_attributes;
    }

    /**
     * @param mixed $attributes
     * @return $this
     */
    public function setAttributes( $attributes ) {
        $skip = array( 'type', 'value', 'name' );
        foreach( $attributes as $key => $value ) {
            if( !in_array($key, $skip) ) {
                $this->addAttribute($key, $value);
            }
        }
        return $this;
    }

    /**
     * Add attribute
     * @param $key
     * @param $value
     * @return $this
     */

    public function addAttribute( $key, $value ){
        $this->_attributes[$key] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->_errors;
    }

    /**
     * @param array $error
     * @return $this
     */
    public function setErrors( $error ) {
        $this->_errors = $error;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel() {
        return $this->_label;
    }

    /**
     * @param mixed $label
     * @return $this
     */
    public function setLabel( $label ) {
        $this->_label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequired() {
        return $this->_required;
    }

    /**
     * @param mixed $required
     * @return $this
     */
    public function setRequired( $required ) {
        $this->_required = $required;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName( $name ) {
        $this->_name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue() {
        return $this->_value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue( $value ) {
        $this->_value = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getRules() {
        return $this->_rules;
    }

    /**
     * @param array $rules
     * @return $this
     */
    public function setRules($rules) {
        $this->_rules = $rules;
        return $this;
    }

    /**
     * @param $rule
     * @return $this
     */
    public function addRule($rule){
        $this->_rules[] = $rule;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * @return string
     */
    public function getTheme() {
        return $this->_theme;
    }

    /**
     * @param string $theme
     * @return $this
     */
    public function setTheme( $theme ) {
        $this->_theme = $theme;
        return $this;
    }

    /**
     * @param boolean $render
     * @return $this
     */
    public function setRender($render)
    {
        $this->_render = $render;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRender()
    {
        return $this->_render;
    }

    /**
     * @param mixed $help
     * @return $this
     */
    public function setHelp($help) {
        $this->_help = $help;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHelp() {
        return $this->_help;
    }

    /**
     * @param boolean $skip
     * @return $this
     */
    public function setSkip($skip) {
        $this->_skip = $skip;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSkip() {
        return $this->_skip;
    }

    /**
     * Base factory
     * @param $name
     * @param array $params
     * @throws Exception
     * @return Former_Abstract_Container
     */
    public static function factory($name,array $params = array()){
        $parts = explode('_',$name);
        $parts = array_map('ucfirst', $parts);

        $class_name = 'Former_Element_' . implode('_', $parts);
        if( !class_exists($class_name) )  {
            throw new Exception('Class \''.$class_name.'\' not found');
        }

        /** @var $element Former_Element */
        $element = new $class_name();
        $element->setParams($params);
        return $element;
    }

    /**
     * Validate the data
     * @return bool
     */
    public function validate() {
        $validation = new Validation( array( $this->getName() => $this->getValue() ) );
        $validation->bind(':model', $this->_form->getModel());

        $rules = $this->getRules();
        if( is_array( $rules ) ) {
            foreach ( $rules AS $_rule) {
                list($_rule_name, $_rule_params) = $_rule;

                // Check if the named function is actually a method on this object.
                if (!function_exists($_rule_name) && method_exists($this, $_rule_name)) {
                    $_rule_name = array($this, $_rule_name);
                }

                $validation->rule( $this->getName(), $_rule_name, $_rule_params);
                $validation->label( $this->getName(), $this->getLabel());
            }

            if ($validation->check()) {
                $this->setErrors( array() );
                return true;
            } else {
                $this->setErrors( $validation->errors(get_class($this)) );
                return false;
            }

        }
        return true;
    }

    /**
     * Render element
     * @throws Exception
     * @return mixed
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams()
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams()
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }
        return $element->render();
    }

    /**
     * Process value before save
     * @return mixed
     */
    public function save() {
        return htmlspecialchars($this->getValue());
    }


    public function reArrayFiles(&$file_post) {
        $file_ary   = array();
        $file_count = count($file_post['name']);
        $file_keys  = array_keys($file_post);
        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }

    public function prepareFilename($file_name = '') {
                    $tr_table = array (
                    'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
                    'И' => 'I', 'Й' => "'", 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
                    'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H','Ц' => 'C', 'Ч' => 'Ch', 'Щ' => 'Sh', 'Ш' => 'Sh',
                    'Ь' => '', 'Ы' => 'Y', 'Ъ' => '', 'Э' => 'E', 'Ю' => 'Yu','Я' => 'Ya',
                    'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
                    'и' => 'i', 'й' => 'i', 'к' => 'k','л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
                    'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch','ш' => 'sh', 'щ' => 'sh',
                    'ь' => '', 'ы' => 'y', 'ъ' => '', 'э' => 'e', 'ю' => 'yu','я' => 'ya',
                    ' ' => '_', "'" => '', '"' => '', '&' => 'and');
                    return strtr($file_name, $tr_table);
    }

}
