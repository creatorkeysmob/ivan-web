<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Form
 */
class Former_Form extends Former_Abstract_Container{

    /**
     * @var Model
     */
    public $_model = null;

    /**
     * Form action
     * @var
     */
    public $_action = '';

    /**
     * Form method
     * @var
     */
    public $_method = Kohana_Request::POST;

    /**
     * Form attributes
     * @var
     */
    public $_attributes = array();

    /**
     * Form theme
     * @var string
     */
    public $_theme = 'bootstrap';

    /**
     * Form errors
     * @var array
     */
    public $_errors = array();

    /**
     * Form values
     * @var array
     */
    public $_data = array();

    /**
     * Loaded elements
     * @var bool
     */
    protected $_loaded = FALSE;

    /**
     * @param \Model $model
     * @return $this
     */
    public function setModel($model) {
        $this->_model = $model;
        return $this;
    }

    /**
     * @param \Model $model
     * @return $this
     */
    public function bindModel(&$model) {
        $this->_model = $model;
        return $this;
    }

    /**
     * @return \Model
     */
    public function getModel() {
        return $this->_model;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->_data;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData($data) {
        $this->_data = $data;

        /* Update values for loaded elements */
        if( $this->isLoaded() ) {
            /** @var $_element Former_Element */
            foreach( $this->_elements as $_element ) {
                $_name = $_element->getName();
                $_value = (isset($this->_data[$_name])) ? $this->_data[$_name] : null;

                if( !is_null($_value) ) {
                    $_element->setValue($_value);
                }
                unset($_value);
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction() {
        return $this->_action;
    }

    /**
     * @param mixed $action
     * @return $this
     */
    public function setAction( $action ) {
        $this->_action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributes() {
        return $this->_attributes;
    }

    /**
     * @param mixed $attributes
     * @return $this
     */
    public function setAttributes( $attributes ) {
        $this->_attributes = $attributes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod() {
        return $this->_method;
    }

    /**
     * @param mixed $method
     * @return $this
     */
    public function setMethod( $method ) {
        $this->_method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getTheme() {
        return $this->_theme;
    }

    /**
     * @param string $theme
     * @return $this
     */
    public function setTheme( $theme ) {
        $this->_theme = $theme;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getLoaded() {
        return $this->_loaded;
    }

    /**
     * @param boolean $loaded
     * @return $this
     */
    public function setLoaded( $loaded ) {
        $this->_loaded = $loaded;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoaded(){
        return ($this->_loaded) ? true : false;
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->_errors;
    }

    /**
     * @param array $errors
     * @return $this
     */
    public function setErrors($errors) {
        /** @var $_element Former_Element */
        foreach( $this->_elements as $_element ) {
            if(isset($errors[$_element->getName()])) {
                $_element->setErrors($errors[$_element->getName()]);
            }
        }
        $this->_errors = $errors;
        return $this;
    }

    public function __construct($data = array()) {
        if($data) {
            $this->setData($data);
        }
        $this->initElements();
    }

    /**
     * Init form elements
     */
    public function initElements( ) {
        $fields = $this->getElements();
        if($fields) {
            $elements = array();
            foreach( $fields as $_key => $_options ){
                if( is_array($_options) ) {
                    $name = Arr::get($_options, 'name', $_key);
                    $label = Arr::get($_options, 'label', $_key);
                    $type = Arr::get($_options, 'type', 'text');

                    // Set value
                    if ( ($value = Arr::get( $this->_data, $name, FALSE) ) === FALSE) {
                        $value = Arr::get($_options, 'default', '');
                    }

                    // Create element
                    $element = $this->createElement( $name, $label, $type, $value, $_options );
                    $element->bindForm($this);
                    $elements[$name] = $element;
                }
            }
            $this->setElements($elements);
            $this->setLoaded( TRUE );
        }
    }

    /**
     * Create form element
     * @param $name
     * @param $label
     * @param $type
     * @param $value
     * @param array $options
     * @return Former_Element
     */
    public function createElement( $name, $label, $type, $value, $options = array() ) {
        /** @var $element Former_Element */
        $element = Former_Element::factory( $type, $options);

        //set fields
        $element->setName( $name );
        $element->setLabel( $label );
        $element->setValue( $value );

        if( !isset($options['theme']) ) {
            $element->setTheme( $this->_theme );
        }

        //set errors
        if (array_key_exists($element->getName(), $this->_errors)) {
            $class = $element->getAttributes('class');
            $element->setErrors( $this->_errors[$element->getName()] );
            $element->addAttribute( 'class', trim($class) );
        }
        return $element;
    }

    /**
     * Add element into container
     * @param $name
     * @param $label
     * @param $type
     * @param $value
     * @param array $options
     * @return $this
     */
    public function addElement( $name, $label, $type, $value, $options = array() ) {
        if( isset($this->_elements[$name]) ) return $this;

        $element = $this->createElement( $name, $label, $type, $value, $options );
        $element->bindForm($this);
        $this->_elements[$name] = $element;

        return $this;
    }

    /**
     * Base factory
     * @param $class
     * @param array $data
     * @param array $params
     * @throws Exception
     * @return Former_Form
     */
    public static function factory($class = null, array $data = array(), array $params = array()){
        if(!is_null($class)) {
            $classname = explode('_', $class);
            $classname = array_map('ucfirst',$classname);
            $class = implode('_',$classname);
        }

        /** @var $form Former_Form */
        if( !is_null($class) && class_exists($class)) {
            $form = new $class($data);
            if( !($form instanceof Former_Form) ) {
                throw new Exception('[Former] Class is not valid form');
            }
        } else {
            $form = new Former_Form($data);
        }
        $form->setParams($params);
        return $form;
    }

    /**
     * Validate the data
     * @return bool
     */
    public function validate() {
        $errors = array();
        $elements = $this->getElements();

        /** @var $_element Former_Element */
        foreach( $elements as $_element ) {
            if( !$_element->validate() ) {
                $errors = array_merge( $errors, $_element->getErrors() );
            }
        }

        if( sizeof($errors) ) {
            $this->setErrors($errors);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Save form
     * @return array
     */
    public function save() {
        $data = array();
        /** @var $_element Former_Element */
        foreach($this->_elements as $_element) {
            $value = $_element->save();
            if(!$_element->getSkip()) {
                $data[$_element->getName()] = $value;
            }
        }
        $this->setData($data);
        return $data;
    }

    /**
     * Render form
     * @return $this
     */
    public function render() {
        return $this;
    }
}