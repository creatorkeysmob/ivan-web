<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Abstract_Container
 */
abstract class Former_Abstract_Container{

    /**
     * Container elements
     * @var Former_Element[]
     */
    public $_elements = array();

    /**
     * Params setter
     * @param $params
     * @return $this
     */
    public function setParams($params){
        foreach($params as $param => $value){
            $parts = explode('_',$param);
            $parts = array_map('ucfirst', $parts);
            $name = implode('', $parts);

            $setter = "set$name";
            if(method_exists($this, $setter)){
                $this->{$setter}($value);
            }
        }
        return $this;
    }

    /**
     * Params getter
     * @return array
     */
    public function getParams(){
        $reflect = new ReflectionClass($this);
        $params = array();
        foreach($reflect->getProperties() as $property){
            $param = $property->getName();
            $parts = explode('_',$param);
            $parts = array_map('ucfirst', $parts);
            $name = implode('', $parts);

            $getter = "get$name";

            if(method_exists($this, $getter)){
                $params[$param] = $this->{$getter}();
            }

        }
        return $params;
    }

    /**
     * Get container elements
     * @return array
     */
    public function getElements() {
        return $this->_elements;
    }

    /**
     * Set container elements
     * @param array $elements
     * @return $this
     */
    public function setElements( $elements ) {
        $this->_elements = $elements;
        return $this;
    }

    /**
     * @return mixed
     */
    abstract public function render();

}