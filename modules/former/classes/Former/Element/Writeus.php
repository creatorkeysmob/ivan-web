<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Writeus
 */
class Former_Element_Writeus extends Former_Element {
    /**
     * @var bool
     */
    static protected $_rendered = false;

    /**
     * @var string
     */
    protected $_type = 'writeus';

    /**
     * Element value
     * @var
     */
    protected $_value = array();

    /**
     * @return mixed
     */
    public function getValue() {
        return $this->_value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        } elseif(is_string($value)){
            $this->_value = json_decode($value, true);
        }
        return $this;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }

        $view = $element->render();
        self::$_rendered = true;

        return $view;
    }

    /**
     * @return string
     */
    public function save() {
        $value = $this->getValue();
        $writeuss = array();
        if ($_POST['writeus'] || $_POST['writeus_text']) {
            foreach( $_POST['writeus'] as $key => $value ) {
                foreach( $_POST['writeus_text'] as $k => $val ) {
                    if( $key == $k ) {
                        $writeuss[] = array(
                            'name' => $value,
                            'text' => $val,
                        );
                    }
                }
            }
        }

        return serialize($writeuss);
    }
}