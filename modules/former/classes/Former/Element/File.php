<?php

defined('SYSPATH') or die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Former_Element_File
 */
class Former_Element_File extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'file';

    /**
     * @var array
     */
    protected $_allowed = array('doc', 'docx', 'xls', 'xlsx','csv', 'txt', 'pdf', 'jpg', 'png');

    /**
     * @param array $allowed
     * @return $this
     */
    public function setAllowed( $allowed ) {
        $this->_allowed = $allowed;
        return $this;
    }

    /**
     * @return array
     */
    public function getAllowed() {
        return $this->_allowed;
    }
    public function getForm() {
        return $this->_form;
    }
    /**
     * @return mixed|string
     */
    public function save() {
        /** @noinspection PhpUndefinedClassInspection */
        $request = Request::$current;
        $value   = $this->getValue();
        $name    = $this->getName();
        $module  = str_replace('Admin_Form_', '', get_class($this->getForm()));

     //   var_dump($this);

        if( isset($_FILES[$this->getName()]['tmp_name']) && !empty($_FILES[$this->getName()]['tmp_name']) ) {
            $file = $_FILES[$this->getName()];
            if (
                !Upload::valid($file) ||
                !Upload::not_empty($file) ||
                !Upload::type($file, $this->getAllowed())
            ) {
                return $value;
            }

            $date = date('Y/m/d');
            $directory = DOCROOT.'upload/modules/'.$module.'/files/'.$name.'/';
            $link      = '/upload/modules/'.$module.'/files/'.$name;

            if( !is_dir($directory) ) {
                $filesystem = new Filesystem();
                $filesystem->mkdir($directory);
            }

            $filename =  Helper_Text::translit($file['name']);
            $extension =  strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

            if( is_file($directory . $filename. '.' . $extension) ) {
                $counter = 1;
                while( is_file($directory . $filename . '_'. $counter . '.' . $extension) ) {
                    $counter++;
                }
                $filename .= '_'.$counter;
            }

            //$filename .= '.' . $extension;
            $upload = Upload::save($file,$filename,$directory);

            // Delete existed image
            if( !empty($value) && $upload ) {
                $path = realpath(DOCROOT.$value);
                if( is_file( $path ) ) {
                    unlink($path);
                }
            }
            $value = (string) $link . '/' . $filename;
        } else if($request->post($this->getName().'_delete')) {
            // Delete existed image
            $path = realpath(DOCROOT.$value);
            if( is_file( $path ) ) {
                unlink($path);
            }
            $value = '';
        }
        return $value;
    }
}
