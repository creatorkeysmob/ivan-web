<?php defined('SYSPATH') or die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;
/**
 * Class Former_Element_Tariff
 */
class Former_Element_Groups extends Former_Element {
    /**
     * @var bool
     */
    static protected $_rendered = false;

    /**
     * @var string
     */
    protected $_type = 'groups';

    /**
     * Element value
     * @var
     */
    protected $_counter;
    protected $_param;
    protected $_value = array();
    protected $_id;

    public function getCounter() {
        return $this->_counter;
    }
    public function setCounter( $counter ) {
        $this->_counter = $counter;
        return $this;
    }
    public function getParam() {
        return $this->_param;
    }
    public function setParam( $param ) {
        $this->_param = $param;
        return $this;
    }
    public function getValue() {
        return $this->_value;
    }
    public function getForm() {
        return $this->_form;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        } elseif(is_string($value)){
            $this->_value = unserialize($value);
            $value = unserialize($value);
        }
        return $this;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }

        $view = $element->render();
        self::$_rendered = true;

        return $view;
    }

    /**
     * @return string
     */
    public function save() {
        $name   = $this->getName();
        $groups = array();
        $module = str_replace('Admin_Form_', '', get_class($this->getForm()));

    //    var_dump($_POST);exit();

        $files_array = ( $_FILES[$name] ) ? $this->reArrayFiles($_FILES[$name]) : array();
       //         var_dump($files_array);exit;
        foreach($files_array as $key => $file){
            if ( isset($file['tmp_name']) && !empty($file['tmp_name']) ){

                if (
                    !Upload::valid($file) ||
                    !Upload::not_empty($file)
                ) {
                    continue;
                }

                $directory = DOCROOT.'upload/modules/'.$module.'/images/'.$name.'/';
                $link      = '/upload/modules/'.$module.'/images/'.$name;
                if( !is_dir($directory) ) {
                    $filesystem = new Filesystem();
                    $filesystem->mkdir($directory);
                }

                $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
               // $filename  = md5(microtime() . $file['name']) . '.' . $extension;
                $filename  = $this->prepareFilename($file['name']);
                $upload    = Upload::save($file,$filename,$directory);
                $value     = (string) $link . '/' . $filename;

              //  array_unshift($pic_arr[$key], $value);
            }
        }

      // var_dump($_POST);exit();

        $param_count = substr_count( implode(' ', array_keys($_POST)), $name.'_' );

        $keys = array();
        foreach ($_POST as $key => $value) {
            if ( strpos($key, $name.'_') === 0 ){
                $keys[] = str_replace($name.'_', '', $key);
            }
        }
        //exit;

        //var_dump($keys);exit;
        if ( $param_count > 0 && !is_null($_POST[$name]) ) {

            foreach( $_POST[$name] as $key => $value ) {
                $array = array();
                //var_dump($key);exit;
                // for( $i = 1; $i <= $param_count; $i ++ ){
                //     $array[$i] = $_POST[$name.'_'.$i][$key];
                // }
                foreach ($keys as $k => $v) {
                    $array[$v] = $_POST[$name.'_'.$v][$key];
                }
                $groups[$key] = $array;
            }
        }
        //exit;
        // var_dump($groups);exit();

        return serialize($groups);
    }
}
