<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Textarea_Disabled 
 */
class Former_Element_Textareadisabled extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'textarea_disabled';

    /**
     * @return string
     */
    
    public function getValue() {
        return htmlspecialchars_decode($this->_value);
    }   
    
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}