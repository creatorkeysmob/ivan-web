<?php
defined('SYSPATH') or die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Former_Element_Albumv2
 */
class Former_Element_Albumv2 extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'album_v2';
    protected $_id;

    /**
     * @var array
     */
    protected $_allowed = array('jpg', 'jpeg', 'png', 'gif');

    public function getForm() {
        return $this->_form;
    }
    /**
     * @param array $allowed
     */
    public function setAllowed( $allowed ) {
        $this->_allowed = $allowed;
    }

    /**
     * @return array
     */
    public function getAllowed() {
        return $this->_allowed;
    }

    /**
     * @return mixed|string
     */
    public function save() {

        /** @noinspection PhpUndefinedClassInspection */
        $request = Request::$current;

        $name   = $this->getName();
        $module = str_replace('Admin_Form_', '', get_class($this->getForm()));

        $old_pic_arr = ( $_POST[$name.'_old'] ) ? array_chunk($_POST[$name.'_old'], 5) : array();
    //    $del_pic = array_diff($_POST['delited_old_pic'], array(''));



        if ( count($old_pic_arr) > 0 ){
            foreach($old_pic_arr as $key => $pic){
                if ( in_array($pic[0], $_POST['delited_old_pic']) ){
                    unset($old_pic_arr[$key]);
                    Thumb::delete($pic[0], TRUE);
                }
            }
        }

        if ( $_POST[$name] ){
            $pic_arr  = array_chunk($_POST[$name], 4);
        } else {
            $ser_pics = serialize($old_pic_arr);
            return $ser_pics;
        }

        if ( count($old_pic_arr) > 0 ){
            $re_keys     = range(count($pic_arr), count($old_pic_arr)+count($pic_arr)-1);
            $old_pic_arr = array_combine($re_keys, $old_pic_arr);
        }

        $files_array = $this->reArrayFiles($_FILES[$name]);
        foreach($files_array as $key => $file){
            if ( in_array($file['name'], $_POST['delited_pic']) ){
                unset($files_array[$key]);
                unset($pic_arr[$key]);
                continue;
            }
            if ( isset($file['tmp_name']) && !empty($file['tmp_name']) ){
                if (
                    !Upload::valid($file) ||
                    !Upload::not_empty($file) ||
                    !Upload::type($file, $this->getAllowed())
                ) {
                    continue;
                }
                $date      = date('Y/m/d');
                $directory = DOCROOT.'upload/modules/'.$module.'/images/'.$name.'/';
                $link      = '/upload/modules/'.$module.'/images/'.$name;
                if( !is_dir($directory) ) {
                    $filesystem = new Filesystem();
                    $filesystem->mkdir($directory);
                }
                $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                $filename  = md5(microtime() . $file['name']) . '.' . $extension;
                $upload    = Upload::save($file,$filename,$directory);
                $value     = (string) $link . '/' . $filename;

                array_unshift($pic_arr[$key], $value);
            }
        }
        $pic_all = $pic_arr + $old_pic_arr;
        $ser_pics = serialize(array_reverse($pic_all));
        return $ser_pics;

    }

}
