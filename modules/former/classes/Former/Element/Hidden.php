<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Hidden
 */
class Former_Element_Hidden extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'hidden';

    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}