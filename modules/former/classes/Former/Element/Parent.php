<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Select
 */
class Former_Element_Parent extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'parent';
    /**
     * @var array
     */
    protected $_choices = array();
    
    protected $_id;
    protected $_child_list;

    /**
     * @param array $choices
     * @return $this
     */
    public function setChoices($choices) {
        $this->_choices = $choices;
        return $this;
    }

    /**
     * @return array
     */
    public function getChoices() {
        return $this->_choices;
    }
    
    public function setId($id) {
        $this->_id = $id;
    }    
    public function getId() {
        return $this->_id;
    }    
    public function setChildList($child_list) {
        $this->_child_list = $child_list;
    }    
    public function getChildList() {
        return $this->_child_list;
    }

    
    
    
}