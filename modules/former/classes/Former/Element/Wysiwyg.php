<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Textarea
 */
class Former_Element_Wysiwyg extends Former_Element {
    /**
     * @var bool
     */
    static protected $_rendered = false;

    /**
     * @var string
     */
    protected $_type = 'wysiwyg';

    /**
     * Wysiwyg properties
     * @var array
     */
    protected $_properties = array(
        'width' => '100%'
    );

    /**
     * Toolbar style Default|Basic|Full
     * @var string
     */
    protected $_toolbar = 'Default';

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties( $properties ) {
        $this->_properties = $properties;
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties() {
        return $this->_properties;
    }

    /**
     * @param string $toolbar
     * @return $this
     */
    public function setToolbar($toolbar) {
        $this->_toolbar = $toolbar;
        return $this;
    }

    /**
     * @return string
     */
    public function getToolbar() {
        return $this->_toolbar;
    }

    /**
     * Render element
     * @throws Exception
     * @return mixed
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }

        $view = $element->render();
        self::$_rendered = true;

        return $view;
    }

}