<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Select
 */
class Former_Element_Select2lvl extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'select2lvl';
    /**
     * @var array
     */
    protected $_choices = array();

    /**
     * @param array $choices
     * @return $this
     */
    public function setChoices($choices) {
        $this->_choices = $choices;
        return $this;
    }

    /**
     * @return array
     */
    public function getChoices() {
        return $this->_choices;
    }
}
