<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Date
 */
class Former_Element_Date extends Former_Element {
    static protected $_rendered = false;

    /**
     * @var string
     */
    protected $_type = 'date';

    /**
     * @return bool|string
     * @throws Exception
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }

        $view = $element->render();
        self::$_rendered = true;

        return $view;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value) {
        if(empty($value) or $value == '0000-00-00 00:00:00') {
            $value = 'now';
        }

        $date = new DateTime( $value );
        $value    = $date->format('Y-m-d H:i:s');
        unset($date);

        $this->_value = $value;
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function save() {
        $value = $this->getValue();

        if(empty($value)  or $value == '0000-00-00 00:00:00') {
            $value = 'now';
        }
        $date = new DateTime( $value );
        $value = $date->format('Y-m-d H:i:s');
        unset($date);

        return $value;
    }
}
