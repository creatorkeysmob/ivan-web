<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_List
 */
class Former_Element_List extends Former_Element {
    /**
     * @var bool
     */
    static protected $_rendered = false;

    /**
     * @var string
     */
    protected $_type = 'list';

    /**
     * Element value
     * @var
     */
    protected $_value = array();

    /**
     * @return mixed
     */
    public function getValue() {
        return $this->_value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        } elseif(is_string($value)){
            $this->_value = json_decode($value, true);
        }
        return $this;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function render() {
        if( !$this->getRender() ) return false;

        $filename = sprintf( 'former/{theme}/element/%s', $this->getType() );
        if( Kohana::find_file('views', $template = str_replace( '{theme}', $this->getTheme(), $filename ), 'twig') ) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } elseif(Kohana::find_file('views', $template = str_replace( '{theme}', 'default', $filename ), 'twig')) {
            $element = Twig::factory( $template,
                array(
                    'element' => $this->getParams(),
                    'rendered' => self::$_rendered
                )
            );
        } else {
            throw new Exception('Not found template for \''.$this->getType().'\' type');
        }

        $view = $element->render();
        self::$_rendered = true;

        return $view;
    }

    /**
     * @return string
     */
    public function save() {
        $value = $this->getValue();
        if(!is_array($value)) {
            $value = array();
        }

        // Filter empty entities
        $data = array();
        foreach($value as $_value ) {
            $option = trim(HTML::chars($_value));
            if(!empty($option)) {
                $data[] = $option;
            }
        }
        return json_encode($data);
    }
}