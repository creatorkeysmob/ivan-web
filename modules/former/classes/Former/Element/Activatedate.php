<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Activatetext
 */
class Former_Element_Activatedate extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'activatedate';

    protected $_value = array();

    public function getValue() {
        return $this->_value;
    }

    public function setValue( $value ) {
        
        if(is_array($value)){
            $this->_value = $value;
        }else{
            $this->_value = unserialize($value);
        }
        return $this;
    }
    
    /**
     * @return string
     */
    public function save() {
        $value = $this->getValue();
        $activ = $value[1];
        $date  = $value[0];
        if( $activ == 'on' && ( empty($date) || $date == '0000-00-00 00:00:00' ) ) {
            $date = 'now';
        }

        $datetime = new DateTime( $date );
        $value    = array($datetime->format('Y-m-d H:i:s'), $activ);
        unset($datetime);
        return serialize($value);
    }
}