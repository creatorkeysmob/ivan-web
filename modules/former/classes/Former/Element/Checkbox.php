<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Checkbox
 */
class Former_Element_Checkbox extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'checkbox';

    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}