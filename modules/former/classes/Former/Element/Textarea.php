<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Textarea
 */
class Former_Element_Textarea extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'textarea';

    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}