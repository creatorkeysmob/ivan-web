<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Divider
 */
class Former_Element_Divider extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'divider';
}