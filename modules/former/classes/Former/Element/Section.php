<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Section
 */
class Former_Element_Section extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'section';

    /**
     * @var string
     */
    protected $_view = 'open';

    /**
     * @param string $view
     * @return $this
     */
    public function setView($view) {
        $this->_view = $view;
        return $this;
    }

    /**
     * @return string
     */
    public function getView() {
        return $this->_view;
    }
}