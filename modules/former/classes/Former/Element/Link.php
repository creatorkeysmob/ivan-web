<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Text
 */
class Former_Element_Link extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'link';
    protected $_by_who;
    
    public function setBywho($by_who) {
        $this->_by_who = $by_who;
    }    
    public function getBywho() {
        return $this->_by_who;
    }   
    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}