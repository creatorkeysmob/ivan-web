<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Double
 */
class Former_Element_Double extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'double';

    /**
     * @return mixed
     */
    public function save() {
        $number = preg_replace('/[^0-9\.]/', '', str_replace(',', '.',$this->getValue()));
        if($this->getValue() < 0) {
            $number = '-' . $number;
        }
        return $number;
    }

}