<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Checkboxes
 */
class Former_Element_Checkboxes extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'checkboxes';
    /**
     * @var array
     */
    protected $_choices = array();
    
    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        } elseif(is_string($value)){
            $this->_value = unserialize($value);
        }
        return $this;
    }
    /**
     * @param array $choices
     * @return $this
     */
    public function setChoices($choices) {
        $this->_choices = $choices;
        return $this;
    }

    /**
     * @return array
     */
    public function getChoices() {
        return $this->_choices;
    }
    
    
    public function save() {
        $name   = $this->getValue();
        $groups = array();
        foreach ( $name as $v ){
           $groups[$v] = ( is_null($_POST[$v]) ) ? 0 : 1;
        }
        return serialize($groups);
    }
}