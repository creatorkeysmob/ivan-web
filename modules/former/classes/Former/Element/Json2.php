<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Text
 */
class Former_Element_Json2 extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'json2';
    protected $_value;

    public function setValue( $value ) {
        $value = str_replace('&quot;', '"', $value);
        $this->_value = json_decode($value, TRUE);
        $value = json_decode($value);
        return $this;
    }
}
