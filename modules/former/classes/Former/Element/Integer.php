<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Integer
 */
class Former_Element_Integer extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'integer';

    /**
     * @return mixed
     */
    public function save() {
        $number = preg_replace('/[^0-9]/', '', $this->getValue());
        if($this->getValue() < 0) {
            $number = '-' . $number;
        }
        return $number;
    }
}