<?php
defined('SYSPATH') or die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Former_Element_Album
 */
class Former_Element_Album extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'album';

    /**
     * @var array
     */
    protected $_allowed = array('jpg', 'jpeg', 'png', 'gif');

    /**
     * @param array $allowed
     */
    public function setAllowed( $allowed ) {
        $this->_allowed = $allowed;
    }

    /**
     * @return array
     */
    public function getAllowed() {
        return $this->_allowed;
    }

    public function getForm() {
        return $this->_form;
    }
    /**
     * @return mixed|string
     */
    public function save() {
        /** @noinspection PhpUndefinedClassInspection */
        $request = Request::$current;

        $pics   = explode(',', $this->getValue());
        $pics   = array_diff($pics, array(''));
        $value  = $this->getValue();
        $name   = $this->getName();
        $module = str_replace('Admin_Form_', '', get_class($this->getForm()));

        if( isset($_FILES[$this->getName()]['tmp_name']) && !empty($_FILES[$this->getName()]['tmp_name']) ) {
            $image = $_FILES[$this->getName()];
            if (
                !Upload::valid($image) ||
                !Upload::not_empty($image) ||
                !Upload::type($image, $this->getAllowed())
            ) {
                return $value;
            }

            $date = date('Y/m/d');
            $directory = DOCROOT.'upload/modules/'.$module.'/images/'.$name.'/';
            $link      = '/upload/modules/'.$module.'/images/'.$name;

            if( !is_dir($directory) ) {
                $filesystem = new Filesystem();
                $filesystem->mkdir($directory);
            }

            $extension = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
            $filename = md5(microtime() . $image['name']) . '.' . $extension;
            $upload = Upload::save($image,$filename,$directory);

            // Delete existed image
//            if( !empty($value) && $upload ) {
//                Thumb::delete($value, TRUE);
//            }
            $value = (string) $link . '/' . $filename;
        } else if($request->post($this->getName().'_delete')) {
            // Delete existed image
            $clear_pics = array();
            $del_pics_arr = $request->post($this->getName().'_delete');
            foreach( $del_pics_arr as $del_pic ){
                Thumb::delete($del_pic, TRUE);
            }

            $pics = array_diff($pics, $del_pics_arr);

//            Thumb::delete($value, TRUE);
            $value = '';
        }

        $pics[] = $value;
        $pics = array_diff($pics, array(''));
        $ser_pics = serialize($pics);
        return $ser_pics;
    }
}
