<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Text_Disabled
 */
class Former_Element_Textdisabled extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'text_disabled';

    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
}
