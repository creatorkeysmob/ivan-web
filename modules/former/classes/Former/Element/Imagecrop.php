<?php
defined('SYSPATH') or die('No direct access allowed.');

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Former_Element_Imagecrop
 */
class Former_Element_Imagecrop extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'imagecrop';
    protected $_height;
    protected $_width;
    /**
     * @var array
     */
    protected $_allowed = array('jpg', 'jpeg', 'png', 'gif');

    /**
     * @param array $allowed
     */
    public function setAllowed( $allowed ) {
        $this->_allowed = $allowed;
    }

    /**
     * @return array
     */
    public function getAllowed() {
        return $this->_allowed;
    }

    public function getForm() {
        return $this->_form;
    }


    public function setHeight($height) {
        $this->_height = $height;
    }
    public function getHeight() {
        return $this->_height;
    }

    public function setWidth($width) {
        $this->_width = $width;
    }
    public function getWidth() {
        return $this->_width;
    }
    /**
     * @return mixed|string
     */
    public function save() {
        /** @noinspection PhpUndefinedClassInspection */
        $request = Request::$current;
        $value   = $this->getValue();
        $name    = $this->getName();
        $module  = str_replace('Admin_Form_', '', get_class($this->getForm()));

        if( isset($_FILES[$this->getName()]['tmp_name']) && !empty($_FILES[$this->getName()]['tmp_name']) ) {

            $image = $_FILES[$this->getName()];
            $height = $this->getHeight();
            $width = $this->getWidth();

            if (
                !Upload::valid($image) ||
                !Upload::not_empty($image) ||
                !Upload::type($image, $this->getAllowed())
            ) {
                return $value;
            }

            $date = date('Y/m/d');
            $directory = DOCROOT.'upload/modules/'.$module.'/images/'.$name.'/';
            $link      = '/upload/modules/'.$module.'/images/'.$name;

            if( !is_dir($directory) ) {
                $filesystem = new Filesystem();
                $filesystem->mkdir($directory);
            }

            $extension = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
            $filename = md5(microtime() . $image['name']) . '.' . $extension;
            $filename_res = md5(microtime() . $image['name']) . '_crop.' . $extension;
            $upload = Upload::save($image,$filename,$directory);

            $image_type = $this->getName();
            if ($_POST['h_'.$image_type]) {
                list($current_width, $current_height) = getimagesize($directory.'/'.$filename);
                $x1 = $_POST['x1_'.$image_type];
                $y1 = $_POST['y1_'.$image_type];
                $x2 = $_POST['x2_'.$image_type];
                $y2 = $_POST['y2_'.$image_type];
                $w  = $_POST['w_'.$image_type];
                $h  = $_POST['h_'.$image_type];
                $quality = 100;

                // This will be the final size of the image
                $crop_width = $w;
                $crop_height = $h;

                // Create our small image
                $new = imagecreatetruecolor($crop_width, $crop_height);
                // Create original image

                switch ($extension)
		{
			case 'jpg':
			case 'jpeg':
                            $current_image = imagecreatefromjpeg($directory.'/'.$filename);
			break;
			case 'gif':
                            $current_image = imagecreatefromgif($directory.'/'.$filename);
			break;
			case 'png':
                            $current_image = imagecreatefrompng($directory.'/'.$filename);
			break;
		}

                // resamling (actual cropping)
                imagecopyresampled($new, $current_image, 0, 0, $x1, $y1, $crop_width, $crop_height, $w, $h);
                // creating our new image

                switch (strtolower($extension))
		{
			case 'jpg':
			case 'jpeg':
				imagejpeg($new, $directory.'/'.$filename, $quality);
			break;
			case 'gif':
                            $quality = NULL;
                            imagegif($new, $directory.'/'.$filename, $quality);
			break;
			case 'png':
                            $quality = 9;
                            imagepng($new, $directory.'/'.$filename, $quality);
			break;
		}
            }

            // Delete existed image
            if( !empty($value) && $upload ) {
                Thumb::delete($value, TRUE);
            }
            $value = (string) $link . '/' . $filename;
        } else if($request->post($this->getName().'_delete')) {
            // Delete existed image
            Thumb::delete($value, TRUE);
            $value = '';
        }
        return $value;
    }



}
