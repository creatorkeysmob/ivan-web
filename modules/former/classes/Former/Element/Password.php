<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Password
 */
class Former_Element_Password extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'password';

    /**
     * @return mixed|null
     */
    public function save() {
        $value = $this->getValue();
        if(empty($value)) {
            $this->setSkip(TRUE);
            $value = NULL;
        }
        return $value;
    }
}