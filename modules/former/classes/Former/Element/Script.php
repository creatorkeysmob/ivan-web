<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Script
 */
class Former_Element_Script extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'script';

    /**
     * @return mixed
     */
    public function save() {
        return $this->getValue();
    }
}