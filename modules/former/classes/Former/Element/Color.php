<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Text
 */
class Former_Element_Color extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'color';
    protected $_class;

    /**
     * @return string
     */
    public function save() {
        return HTML::chars( $this->getValue() );
    }
    public function setClass($class) {
        $this->_class = $class;
    }
    public function getClass() {
        return $this->_class;
    }
}
