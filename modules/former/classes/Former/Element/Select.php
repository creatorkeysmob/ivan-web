<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Select
 */
class Former_Element_Select extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'select';
    /**
     * @var array
     */
    protected $_choices = array();

    /**
     * @param array $choices
     * @return $this
     */
    public function setChoices($choices) {
        $this->_choices = $choices;
        return $this;
    }

    /**
     * @return array
     */
    public function getChoices() {
        return $this->_choices;
    }
}