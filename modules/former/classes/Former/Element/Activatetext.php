<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Activatetext
 */
class Former_Element_Activatetext extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'activate_text';

    protected $_value = array();

    public function getValue() {
        return $this->_value;
    }

    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        }else{
            $this->_value = unserialize(base64_decode($value));
        }
        return $this;
    }
    
    /**
     * @return string
     */
    public function save() {
       // var_dump($this->getValue());exit;
        return HTML::chars( base64_encode(serialize($this->getValue())) );
    }
}