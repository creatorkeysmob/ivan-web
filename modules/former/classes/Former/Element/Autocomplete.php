<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Former_Element_Autocomplete
 */
class Former_Element_Autocomplete extends Former_Element {
    /**
     * @var string
     */
    protected $_type = 'autocomplete';

    /**
     * @var array
     */
    protected $_choices = array();

    /**
     * Element value
     * @var
     */
    protected $_value = array();

    /**
     * @return mixed
     */
    public function getValue() {
        return $this->_value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue( $value ) {
        if(is_array($value)){
            $this->_value = $value;
        } elseif(is_string($value)){
            $this->_value = json_decode($value, true);
        }
        return $this;
    }

    /**
     * @param array $choices
     * @return $this
     */
    public function setChoices($choices) {
        $this->_choices = $choices;
        return $this;
    }

    /**
     * @return array
     */
    public function getChoices() {
        return $this->_choices;
    }

    /**
     * @return string
     */
    public function save() {
        $value = $this->getValue();
        if(!is_array($value)) {
            $value = array();
        }
        return json_encode($value);
    }
}