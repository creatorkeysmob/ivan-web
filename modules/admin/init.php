<?php defined('SYSPATH') or die('No direct access allowed.');

/** @noinspection PhpUndefinedClassInspection */
Route::set('admin_auth', 'admin/<action>',
    array(
        'action' => 'login|logout'
    )
)
    ->defaults(
        array(
            'controller' => 'Main',
            'prefix'     => 'Admin_Controller'
        )
    );

/** @noinspection PhpParamsInspection, PhpUndefinedClassInspection */
Route::set('admin_base', 'admin(/<controller>(/<action>(/<filter>)))',
    array(
        'filter' => '.*'
    )
)
    ->filter(
        function( $route, $params, $request ) {
            //get prefix
            $params['prefix'] = 'Admin_Controller';

            //get controller name
            if( isset($params['controller']) ) {
                if( strpos($params['controller'],'Admin') === FALSE ) {
                    $params['controller'] = $params['controller'];
                }
            } else {
                $params['controller'] = 'Main';
            }

            //get filter vars
            if( isset($params['filter']) ) {
                $matches = array();
                $filter = explode('/',$params['filter']);
                for( $i = 0, $length = count($filter); $i < $length; $i += 2 ) {
                    if( isset($filter[$i]) && isset($filter[$i+1]) ) {
                        $matches[$filter[$i]] = $filter[$i+1];
                    } else if( isset($filter[$i]) && !isset($filter[$i+1]) ) {
                        $matches[$filter[$i]] = null;
                    } else {
                        continue;
                    }
                }
                $params['filter'] = $matches;
            } else {
                $params['filter'] = array();
            }
            return $params;
        }
    );