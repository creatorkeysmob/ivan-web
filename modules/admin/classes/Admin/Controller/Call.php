<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Call.
*/
class Admin_Controller_Call extends Admin_Controller_Crud {
    public $moduleName = 'call';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Call');
        $this->_crud_form = Former_Form::factory('Admin_Form_Call');

        // List properties
        $this->_list_properties = array(
            'sort'   => array('id' => 'DESC'),
            'fields' => array(
                array( 'name' => 'date', 'label' => 'Дата' ),
                array( 'name' => 'text', 'label' => 'Сообщение' ),
            ),
            'custom' => array(
                'format_html' => true,
                'pagination'  => false,
                'drop'        => true,
                'only_list'   => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/modules/call');
    }
}
