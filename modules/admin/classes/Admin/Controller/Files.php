<?php defined( 'SYSPATH' ) or die( 'No direct access allowed.' );

class Admin_Controller_Files extends Admin_Controller_Abstract {
    public $moduleName = 'files';

    public function action_index() {
        $this->render('Admin/Files', array());
    }
}