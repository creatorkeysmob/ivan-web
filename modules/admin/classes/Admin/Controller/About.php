<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Controller_About.
 */
class Admin_Controller_About extends Admin_Controller_Crud {
    public $moduleName = 'about';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('About');
        $this->_crud_form = Former_Form::factory('Admin_Form_About');

        //List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                array('name' => 'head',    'label' => 'Название'),
                //array('name' => 'post', 'label' => 'Превью'),
            ),
            'custom' => array(
                'sortable'   => true,
                'pagination' => true,
                'drop'       => true,
                'hide'       => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/about');
    }
}
