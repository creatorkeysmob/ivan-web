<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Writeus.
*/
class Admin_Controller_Writeus extends Admin_Controller_Crud {
    public $moduleName = 'writeus';


    // Models
    const WRITEUS_MODEL = 'Writeus';

    // Forms
    const WRITEUS_FORM = 'Admin_Form_Writeus';

    // Views
    const WRITEUS_TEMPLATE = 'Admin/Writeus';

    /**
     *  Index action
     */
    public function action_index(){
        $form = Former_Form::factory(self::WRITEUS_FORM);
        if( $this->request->method() === Kohana_Request::POST ) {
            $form->setData($this->request->post());

            // Merge post and form data
            $data = array();
            foreach($form->save() as $_key => $_value) {
                if(!is_numeric($_key)) {
                    $data[$_key] = $_value;
                }
            }



            Message::instance()->add(
                'success', 'Успешно сохранено', Message::TYPE_SUCCESS
            );

        } else {
            
        }
        $this->render( self::WRITEUS_TEMPLATE,
            array(
                'form' => $form->render(),
            )
        );
    }
}

