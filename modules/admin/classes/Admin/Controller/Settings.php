<?php defined('SYSPATH') or die('No direct access allowed.');


class Admin_Controller_Settings extends Admin_Controller_Abstract {
    public $moduleName = 'settings';

    // Models
    const SETTINGS_MODEL = 'Settings';

    // Forms
    const SETTINGS_FORM = 'Admin_Form_Settings';

    // Views
    const SETTINGS_TEMPLATE = 'Admin/Settings';

    /**
     *  Index action
     */
    public function action_index(){
        $form = Former_Form::factory(self::SETTINGS_FORM);
        if( $this->request->method() === Kohana_Request::POST ) {

            $form->setData($this->request->post());

            // Merge post and form data
            $data = array();
            foreach($form->save() as $_key => $_value) {
                if(!is_numeric($_key)) {
                    $data[$_key] = $_value;
                }
            }


            // Save data
            foreach($data as $_key => $_value) {
                /** @var $model Model_Settings */
                $model = ORM::factory(self::SETTINGS_MODEL)
                            ->where('key','=',$_key)
                            ->find();

                $model->key = $_key;
                $model->value = $_value;
                $model->save();

                unset($model);
            }

            // exit;
            Message::instance()->add(
                'success', 'Успешно сохранено', Message::TYPE_SUCCESS
            );

        } else {
            $data = ORM::factory(self::SETTINGS_MODEL)
                        ->find_all()
                        ->as_array('key','value');

            $form->setData($data);
        }
        $this->render( self::SETTINGS_TEMPLATE,
            array(
                'form' => $form->render(),
            )
        );
    }
}
