<?php defined('SYSPATH') or die('No direct access allowed.');


class Admin_Controller_Colorpicker extends Admin_Controller_Abstract
{
    public $moduleName = 'colorpicker';
    const CP_TMPL      = 'Admin/Colorpicker/Colorpicker';
    const CP_CSS       = 'Admin/Colorpicker/Css';
    const CP_REAL_CSS  = 'assets/public/css/colors.css';


    public function action_index()
    {
        $form = Former_Form::factory('Admin_Form_Colorpicker');
        if( $this->request->method() === Kohana_Request::POST ) {
            $data = $_POST;
            $field = array('color' => $data['color']);
            // var_dump($field);exit;
            DB::update('colorpicker')
                ->set($field)
                ->where('id', '=', 1)
                ->execute($this->_db);

            $color_css = Twig::factory(self::CP_CSS, array('color' => $data['color']))->render();
            file_put_contents($_SERVER['DOCUMENT_ROOT'].self::CP_REAL_CSS, trim($color_css));
        }


        $data = ORM::factory('Colorpicker')
                        ->find_all()
                        ->as_array('key', 'color');

        $form->setData($data);

        $this->render(
            self::CP_TMPL,
            array(
                'form' => $form->render(),
                'data' => $data,
            )
        );
    }
}
