<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Creator.
*/
class Admin_Controller_Creator extends Admin_Controller_Crud {
    public $moduleName = 'creator';

    // Forms
    const CREATOR_FORM = 'Admin_Form_Creator';

    // Views
    const CREATOR_TEMPLATE = 'Admin/Creator/Creator';



    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Creator');
        $this->_crud_form = Former_Form::factory(self::CREATOR_FORM);

        // List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                array('name' => 'head',    'label' => 'Название'),
                array('name' => 'alias',   'label' => 'Название (En)'),
            ),
            'custom' => array(
                'not_edit'  => true,
                'creator'   => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/creator');
    }



    /**
     *  Index action
     */
    public function action_index() {

        $form = Former_Form::factory(self::CREATOR_FORM);

        // ajax удоление модуля
        if(
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $lvl        = $_POST['lvl'];
            $main       = $_POST['main'];
            $name_big   = $_POST['name'];
            $search     = $_POST['search'];
            $img        = $_POST['img'];
            $name_small = strtolower($_POST['name']);
            $delete     = "DROP TABLE `wa_$name_small`";
            Database::instance()->query(NULL, $delete);

            $module = ORM::factory('creator')
            	->where('alias', '=', $name_big)
            	->find()
                ->delete();

                $file_arr = array(
                    0 => array(
                        'file' => APPPATH."classes/Model/".$name_big.".php",
                    ),
                    1 => array(
                        'file' => MODPATH."admin/classes/Admin/Controller/".$name_big.".php",
                    ),
                    2 => array(
                        'file' => MODPATH."admin/classes/Admin/Form/".$name_big.".php",
                    ),
                    3 => array(
                        'file' => MODPATH."public/classes/Public/Controller/".$name_big.".php",
                    ),
                    4 => array(
                        'file' => MODPATH."public/views/".$name_big."/Index.twig",
                    ),
                    5 => array(
                        'path' => MODPATH."public/views/".$name_big."/",
                        'file' => MODPATH."public/views/".$name_big."/List.twig",
                    ),
                );

                if ( $main ){
                    $file_arr[] = array(
                        'file' => MODPATH."public/views/MainModules/".$name_big.".twig",
                    );


                    $modules_form_file    = MODPATH.'admin/classes/Admin/Form/Modules.php';
                    $main_controller_file = MODPATH.'public/classes/Public/Controller/Main.php';

                    $main_array = array(
                        0 => array(
                            'file'    => $modules_form_file,
                        ),
                        1 => array(
                            'file'    => $main_controller_file,
                        ),
                    );

                    foreach ($main_array as $file) {
                        $file_read   = file_get_contents($file['file']);
                        $file_array  = explode('/*********************************/', $file_read);
                        foreach ($file_array as $key => $value) {
                            if ( stristr($value, "'alias'  => '$name_big',") ){
                                unset($file_array[$key]);
                            }
                            if ( stristr($value, "Module('$name_big'") ){
                                unset($file_array[$key]);
                            }
                            if ( trim($value) == '' ) {
                                unset($file_array[$key]);
                            }
                        }
                        if ( count($file_array) == 2 ) {
                            file_put_contents($file['file'], implode($file_array));
                        } else {
                            file_put_contents($file['file'], implode('/*********************************/', $file_array));
                        }
                    }


                }

            foreach ($file_arr as $key => $file) {
                unlink($file['file']);
                if ( $file['path'] ) {
                    rmdir($file['path']);
                }
            }

            if ( $search ){
                // удалим из конфига поиска
                $search_conf = Kohana::$config->load('public/modules/search');
                $models      = Kohana::$config->load('public/modules/search.models');
                $models      = array_flip($models); //Меняем местами ключи и значения
                unset ($models[$name_big]);         //Удаляем элемент массива
                $models      = array_flip($models); //Меняем местами ключи и значения
                $search_conf->set('models', $models);
            }


            $main    = Kohana::$config->load('admin/main');
            $modules = Kohana::$config->load('admin/main.modules');
            $menu    = Kohana::$config->load('admin/main.menu');

            unset($modules['admin'][$name_small]);
            unset($modules['public'][$name_small]);
            unset($menu['creator']['items'][$name_small]);
            if ( count($menu['creator']['items']) == 0 ){
                unset($menu['creator']);
            }
            $main->set('modules',  $modules);
            $main->set('menu',     $menu);
            if ( $lvl ) {
                $sitemap = Kohana::$config->load('admin/main.sitemap');
                $sitemap = array_flip($sitemap); //Меняем местами ключи и значения
                unset ($sitemap[$name_small]);   //Удаляем элемент массива
                $sitemap = array_flip($sitemap); //Меняем местами ключи и значения
                $main->set('sitemap', $sitemap);
            }

            $init        = MODPATH.'public/init/init_creator.php';
            $init_read   = file_get_contents($init);
            $init_arr    = explode('/*********************************/', $init_read);
            foreach ($init_arr as $key => $value) {
                if ( stristr($value, 'public_'.$name_small) ){
                    unset($init_arr[$key]);
                }
                if ( trim($value) == '' ) {
                    unset($init_arr[$key]);
                }
            }
            if ( count($init_arr) == 2 ) {
                file_put_contents($init, implode($init_arr));
            } else {
                file_put_contents($init, implode('/*********************************/', $init_arr));
            }

            // прибрать в загруженных файлах
            $directory = DOCROOT.'upload/modules/'.$name_big;
            if( is_dir($directory) ) {
                Widget::dirDel($directory);
            }

            echo 1;exit;
        }

        header("Refresh:0; url=/admin/creator/list");
    }

    public function action_edit() {
        // редиркетим к списку, так как редактировать ничо нельзя
        header("Refresh:0; url=/admin/creator/list");
    }

}
