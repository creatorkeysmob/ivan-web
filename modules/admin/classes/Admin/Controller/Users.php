<?php defined('SYSPATH') or die('No direct access allowed.');

class Admin_Controller_Users extends Admin_Controller_Abstract {
    public $moduleName = 'users';

    const USERS_MODEL = 'User';
    const USERS_FORM = 'Admin_Form_Users';
    const USERS_CONFIG = 'admin/modules/users';
    const USERS_TEMPLATE_LIST = 'Admin/Users/List';
    const USERS_TEMPLATE_EDIT = 'Admin/Users/Edit';

    /**
     *  Index action
     */
    public function action_index() {
        /** @noinspection PhpUndefinedClassInspection */

        $this->redirect(
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => 'list',
                )
            )
        );
    }

    /**
     *  List action
     */
    public function action_list() {
        // Load config
        $config = Kohana::$config->load(self::USERS_CONFIG)->as_array();
        $items =  ORM::factory(self::USERS_MODEL)->where('exclude','=',0);
        if($perpage = Arr::path($config, 'pagination.perpage')) {
            // Apply pagination
            $count = $items->count_all();
            $pagination = Pagination::factory(
                array(
                    'source' => 'filter',
                    'key' => 'page',
                    'perpage' => Arr::path($config, 'pagination.perpage'),
                    'total_items' => $count
                ),
                'dynamic'
            );
            $pagination->execute();

            $items->limit($pagination->getPerpage())
                  ->offset($pagination->getOffset());

            $pagination = $pagination->render();
        }

        // Get items
        $items = $items->where('exclude','=',0)
                       ->order_by('id', 'ASC')
                       ->find_all();

        $this->render(self::USERS_TEMPLATE_LIST,
            array(
                'items' => $items,
                'pagination' => $pagination
            )
        );
    }

    /**
     *  Create action
     */
    public function action_create() {
        /** @var $model Model_User */
        $model = ORM::factory(self::USERS_MODEL);
        $form = Former_Form::factory(self::USERS_FORM);

        // Add password rules
        $form->_elements['password']
            ->setRequired(TRUE)
            ->addRule( array('not_empty') )
            ->addRule( array('min_length', array(':value','6')) );

        if ($this->request->method() === Kohana_Request::POST) {
            $form->setData($this->request->post());

            if ($form->validate()) {
                // Save form
                $data = $form->save();

                // Check unique
                $errors = $model->is_unique($data['username'],$data['email']);
                if(!$errors) {
                    // Save model
                    $model->values($data);
                    $model->save();

                    // Add roles
                    $model->add('roles', Model_User::ROLE_LOGIN);
                    if( $data['role'] == Model_User::TYPE_ADMIN ) {
                        $model->add('roles', Model_User::ROLE_ADMIN);
                    }

                    Message::instance()->add(
                        'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                    );

                    /** @noinspection PhpUndefinedClassInspection */
                    $this->redirect(
                        Route::get('admin_base')->uri(
                            array(
                                'controller' => $this->moduleName,
                                'action' => 'edit',
                                'filter' => array(
                                    'id' => $model->id
                                )
                            )
                        )
                    );
                } else {
                    $form->setErrors($errors);
                    Message::instance()->set(
                        $errors, Message::TYPE_ERROR
                    );
                }
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        }

        // Generate breadcrumbs
        Breadcrumb::instance()->addItem(
            'Новый пользователь',
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => 'create',
                )
            ), false
        );

        $this->render(self::USERS_TEMPLATE_EDIT,
            array(
                'item' => $model,
                'form' => $form->render(),
            )
        );
    }

    /**
     *  Edit action
     */
    public function action_edit() {
        $filter = $this->request->param('filter');
        $id = (isset($filter['id'])) ? intval($filter['id']) : 0;

        if (empty($id)) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                    )
                )
            );
        }

        /** @var $model Model_User */
        $model = ORM::factory(self::USERS_MODEL)
            ->where('id','=',$id)
            ->and_where('exclude','=',0)
            ->find();

        if (!$model->loaded()) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                    )
                )
            );
        }

        $form = Former_Form::factory(self::USERS_FORM);
        if ($this->request->method() === Kohana_Request::POST) {
            $form->setData($this->request->post());
            if($form->_elements['password']->getValue()) {
                $form->_elements['password']->addRule( array('min_length', array(':value','6')) );
            }

            if ($form->validate()) {
                // Save form
                $data = $form->save();

                // Check unique
                $errors = $model->is_unique($data['username'],$data['email']);
                if(!$errors) {
                    // Save model
                    $model->values($data);
                    $model->save();

                    // Add user roles
                    if($data['role'] == Model_User::TYPE_ADMIN && !$model->has_role(Model_User::ROLE_ADMIN)) {
                        $model->add('roles', Model_User::ROLE_ADMIN);
                    } elseif($data['role'] == Model_User::TYPE_USER && $model->has_role(Model_User::ROLE_ADMIN)) {
                        $model->remove('roles', Model_User::ROLE_ADMIN);
                    }

                    Message::instance()->add(
                        'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                    );

                    /** @noinspection PhpUndefinedClassInspection */
                    $this->redirect(
                        Route::get('admin_base')->uri(
                            array(
                                'controller' => $this->moduleName,
                                'action' => 'edit',
                                'filter' => array(
                                    'id' => $model->id
                                )
                            )
                        )
                    );
                }  else {
                    $form->setErrors($errors);
                    Message::instance()->set(
                        $errors, Message::TYPE_ERROR
                    );
                }
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        } else {
            $data = $model->original_values();
            $data['role'] = $model->has_role(Model_User::ROLE_ADMIN) ? 2 : 1;
            $form->setData($data);
        }

        // Generate breadcrumbs
        Breadcrumb::instance()->addItem(
            ($model->username) ? $model->username : 'Редактировать пользователя', false
        );

        $this->render(self::USERS_TEMPLATE_EDIT,
            array(
                'item' => $model,
                'form' => $form->render(),
            )
        );
    }

    /**
     * Ajax drop
     * @throws HTTP_Exception_403
     */
    public function action_drop() {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $answer = array(
                'items' => array()
            );

            foreach ($this->request->post() as $field => $delete) {
                if (preg_match('/item_([0-9]+)/', $field, $match) && $delete) {
                    $item_id = intval($match[1]);

                    /** @var $model Model_User */
                    $model = ORM::factory(self::USERS_MODEL)
                        ->where('id','=',$item_id)
                        ->and_where('exclude','=',0)
                        ->find();

                    if ($model->loaded()) {
                        $answer['items'][$item_id] = true;
                        $model->delete();
                    }

                    unset($model);
                }
            }

            echo json_encode(array('data' => $answer));
            exit;
        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    /**
     * Ajax hide
     * @throws HTTP_Exception_403
     */
    public function action_hide() {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $id = $this->request->post('id');
            $answer = array('id' => $id);

            if ($id) {
                /** @var $model Model_User */
                $model = ORM::factory(self::USERS_MODEL)
                    ->where('id','=',$id)
                    ->and_where('exclude','=',0)
                    ->find();

                if ($model->loaded()) {
                    /** @var $model Model_User */
                    $status = ($model->hide) ? 0 : 1;
                    $answer['hide'] = $model->hide = $status;
                    $answer['status_message'] = $status == 1 ? 'Не активен' : 'Активен';
                    $model->save();
                }
            }

            echo json_encode(array('data' => $answer));
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }
}