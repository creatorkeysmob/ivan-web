<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Call.
*/
class Admin_Controller_Formconstruct extends Admin_Controller_Crud {
    public $moduleName = 'formconstruct';
    // Forms
    const FORMCONSTRUCT_FORM = 'Admin_Form_Formconstruct';

    // Views
    const FORMCONSTRUCT_TEMPLATE = 'Admin/Formconstruct/List';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Formconstruct');
        $this->_crud_form = Former_Form::factory('Admin_Form_Formconstruct');

        // List properties
        $this->_list_properties = array(
            'sort' => array('id' => 'DESC'),
            'fields' => array(
                array( 'name' => 'head',  'label' => 'Название' ),
                array( 'name' => 'alias', 'label' => 'Код', 'not_edit' => true, ),
            ),
            'custom' => array(
                'not_edit'  => true,
                'creator'   => true,
            ),
        );
    }



    /**
     *  Index action
     */
    public function action_index() {
        $form = Former_Form::factory(self::FORMCONSTRUCT_FORM);

        // ajax удоление модуля
        if(
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $table    = strtolower($_POST['name']);
            $name_big = ucfirst($table);

            $delete   = "DROP TABLE `wa_feedback_$table`";
            Database::instance()->query(NULL, $delete);

            $module = ORM::factory('formconstruct')
            	->where('alias', '=', $table)
            	->find()
                ->delete();

            $file_arr = array(
                0 => array(
                    'file' => APPPATH."classes/Model/".$name_big.".php",
                ),
                1 => array(
                    'file' => MODPATH."admin/classes/Admin/Controller/".$name_big.".php",
                ),
                2 => array(
                    'file' => MODPATH."admin/classes/Admin/Form/".$name_big.".php",
                ),
            );

            foreach ($file_arr as $key => $file) {
                unlink($file['file']);
            }

            $main    = Kohana::$config->load('admin/main');
            $modules = Kohana::$config->load('admin/main.modules');
            $menu    = Kohana::$config->load('admin/main.menu');

            unset($modules['admin'][$table]);
            unset($modules['public'][$table]);
            unset($menu['msg']['items'][$table]);
            if ( count($menu['msg']['items']) == 0 ){
                unset($menu['msg']);
            }
            $main->set('modules',  $modules);
            $main->set('menu',     $menu);

            echo 1;exit;
        }

        header("Refresh:0; url=/admin/formconstruct/list");

    }

    public function action_edit() {
        // редиркетим к списку, так как редактировать ничо нельзя
        header("Refresh:0; url=/admin/formconstruct/list");
    }

}
