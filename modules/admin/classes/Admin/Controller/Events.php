<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Controller_Events.
 */
class Admin_Controller_Events extends Admin_Controller_Crud {
    public $moduleName = 'events';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Events');
        $this->_crud_form = Former_Form::factory('Admin_Form_Events');

        // List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                array('name' => 'date', 'label' => 'Дата'),
                array('name' => 'head',    'label' => 'Название'),
                //array('name' => 'preview', 'label' => 'Превью'),
            ),
            'custom' => array(
                'sortable'   => true,
                'pagination' => true,
                'drop'       => true,
                'hide'       => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/events');
    }
}
