<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Controller_News.
 */
class Admin_Controller_News extends Admin_Controller_Crud {
    public $moduleName = 'news';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('News');
        $this->_crud_form = Former_Form::factory('Admin_Form_News');

        // List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                array('name' => 'created', 'label' => 'Дата'),
                array('name' => 'head',    'label' => 'Название'),
                array('name' => 'preview', 'label' => 'Превью'),
            ),
            'custom' => array(
                'table'      => 'news',
                'sortable'   => true,
                'pagination' => true,
                'drop'       => true,
                'hide'       => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/news');
    }
}
