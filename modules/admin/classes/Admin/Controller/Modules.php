<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Controller_Modules.
 */
class Admin_Controller_Modules extends Admin_Controller_Crud {
    public $moduleName = 'modules';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Modules');
        $this->_crud_form = Former_Form::factory('Admin_Form_Modules');

        // List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                array('name' => 'head',    'label' => 'Название'),
            ),
            'custom' => array(
                'table'      => 'modules',
                'sortable'   => true,
                'pagination' => true,
                'drop'       => true,
                'hide'       => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/modules');
    }
}
