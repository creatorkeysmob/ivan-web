<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Sitemap.
*/
class Admin_Controller_Sitemap extends Admin_Controller_Crud {
    public $moduleName = 'sitemap';

    // Views
    const SITEMAP_TEMPLATE = 'Admin/Sitemap';

    /**
     *  Index action
     */
    public function action_index(){

        if( $this->request->method() === Kohana_Request::POST ) {
            $data = $_POST;

            $field = array('sitemapshow' => $data['status']);
            DB::update($data['table'])
                ->set($field)
                ->where('id', '=', $data['id'])
                ->execute($this->_db);
            echo 1;exit;

        }
        global $sitemap;
        self::getSiteMapLvl(0, 1);
        $this->render( self::SITEMAP_TEMPLATE,
            array(
                'sitemap' => $sitemap,
            )
        );
    }


    private function getSiteMapLvl($parent_id, $visible) {
        global $sitemap;
        $pages = DB::select('id', 'head', 'parent_id', 'module', 'sitemapshow')
                    ->from('pages')
                    ->where('parent_id', '=', $parent_id)
                    ->order_by('priority', 'ASC')
                    ->execute()
                    ->as_array();

        if (!$pages) return false;

        if ( $visible == 0 ) $gray = 'class="gray"';
        $sitemap .= '<ul '.$gray.'>';
        foreach ($pages as $key => $page) {
            $eye  = ( $page['sitemapshow'] == 1 ) ? '<i title="Скрыть" class="sitemap_status icon-eye-open"></i>' : '<i title="Показать" class="sitemap_status icon-eye-close"></i>';
            $gray = ( $page['sitemapshow'] == 1 ) ? '' : 'gray';
            $sitemap .= '<li class="sitemapshow '.$gray.'" data-id="'.$page['id'].'" data-table="pages">'.$eye.' '.$page['head'].'</li>';

            $modules = Kohana::$config->load('admin/main.sitemap'); // так сделано, что бы отсеитьв всякие 404, поиски и sitemap

            if ( in_array($page['module'], $modules) ) {
                self::getSiteMapModule($page['module'], $page['sitemapshow']);
            }
            self::getSiteMapLvl($page['id'], $page['sitemapshow']);
        }
        $sitemap .= '</ul>';
    }

    private function getSiteMapModule($module, $visible) {
        global $sitemap;
        $pages = DB::select('id', 'head', 'sitemapshow')
                    ->from($module)
                    ->order_by('priority', 'ASC')
                    ->execute()
                    ->as_array();

        if (!$pages) return false;
        if ( $visible == 0 ) $gray = 'class="gray"';
        $sitemap .= '<ul '.$gray.'>';
        foreach ($pages as $key => $page) {
            $eye = ( $page['sitemapshow'] == 1 ) ? '<i title="Скрыть" class="sitemap_status icon-eye-open"></i>' : '<i title="Показать" class="sitemap_status icon-eye-close"></i>';
            $sitemap .= '<li class="sitemapshow" data-id="'.$page['id'].'" data-table="'.$module.'">'.$eye.' '.$page['head'].'</li>';
        }
        $sitemap .= '</ul>';
    }
}
