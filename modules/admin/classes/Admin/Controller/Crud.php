<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Admin_Controller_Crud
 */

class Admin_Controller_Crud extends Admin_Controller_Abstract
{
    // Crud actions
    const CRUD_LIST_ACTION = 'list';
    const CRUD_EDIT_ACTION = 'edit';
    const CRUD_CREATE_ACTION = 'create';
    const CRUD_REMOVE_ACTION = 'remove';

    // Crud templates
    const CRUD_TEMPLATE_LIST = 'list';
    const CRUD_TEMPLATE_EDIT = 'edit';

    /**
     * Crud model
     * @var Model_Abstract
     */
    protected $_crud_model;

    /**
     * Crud form
     * @var Former_Form
     */
    protected $_crud_form;

    /**
     * Crud config
     * @var array
     */
    protected $_crud_config;

    /**
     * Crud list properties
     *
     * filter - default collection filter, format: name => value
     * sort - default collection sort, format: name => sort type (ASC | DESC)
     * fields - displayed fields, format [ 'name' => '', 'label' => '', 'render' => ( text | img | boolean | datetime ), 'default' => '' ]
     * custom - custom actions ['sortable' => true, 'hide' => true, 'drop' => true]
     * @var array
     */
    protected $_list_properties = array();

    /**
     * Get template filename
     * @param $template
     * @throws Exception
     * @return string
     */
    protected function _getTemplate($template)
    {
        $filename = sprintf(
            'Admin/%s/%s', ucfirst($this->moduleName), ucfirst($template)
        );

        if (Kohana::find_file('views', $filename, 'twig')) {
            return $filename;
        } else {
            $filename = sprintf(
                'Admin/Crud/%s', ucfirst($template)
            );
            if (Kohana::find_file('views', $filename, 'twig')) {
                return $filename;
            } else {
                throw new Exception('Crud: template does not exists');
            }
        }
    }

    /**
     * List action handler
     */
    public function action_list()
    {
        $request_filter = $this->request->param('filter');
        $config_filter = array();

        if(isset($this->_list_properties['filter']) && is_array($this->_list_properties['filter'])) {
            $config_filter = $this->_list_properties['filter'];
        }  elseif(isset($this->_crud_config['filter']) && is_array($this->_crud_config['filter'])) {
            $config_filter = $this->_crud_config['filter'];
        }

        // Override request filter and config filter
        if( $config_filter ) {
            $params = $request_filter;
            foreach ($config_filter as $_key => $_value) {
                if (!isset($request_filter[$_key])) $request_filter[$_key] = $_value;
            }

            if (array_diff($params, $request_filter)) {
                /** @noinspection PhpUndefinedClassInspection */
                $this->redirect(
                    Route::get('admin_base')->uri(
                        array(
                            'controller' => $this->moduleName,
                            'action' => self::CRUD_LIST_ACTION,
                            'filter' => $params
                        )
                    )
                );
                exit;
            }
        }

        /** @var $models Model_Abstract */
        $models = $this->_crud_model;
        $columns = $models->table_columns();

        // Apply filter
        if (is_array($request_filter) && !empty($request_filter)) {
            foreach ($request_filter as $_key => $_value) {
                if (isset($columns[$_key])) {
                    $models->where($_key, '=', $_value);
                }
            }
        }

        // count items
        $count = $models->count_all();

        // Apply sort
        if (isset($this->_list_properties['sort']) && is_array($this->_list_properties['sort']) && !empty($this->_list_properties['sort'])) {
            foreach ($this->_list_properties['sort'] as $_key => $_type) {
                if (isset($columns[$_key])) {
                    $models->order_by($_key, $_type);
                }
            }
        }

        // Apply Pagination
        $pagination = '';
        $page_col = (isset($_COOKIE['pagination']) ? ($_COOKIE['pagination'] == 'all' ? 9999999999 : $_COOKIE['pagination']) : 10);

        if (isset($this->_list_properties['custom']['pagination']) && $this->_list_properties['custom']['pagination'] === true) {
            $pagination = Pagination::factory(
                array(
                   'source' => 'filter',
                   'key' => 'page',
                   'perpage' => Arr::path($this->_crud_config, 'pagination.perpage', $page_col),
                   'total_items' => $count
                ),
                'dynamic'
            );
            $pagination->execute();

            $models->limit($pagination->getPerpage())
                   ->offset($pagination->getOffset());

            $pagination = $pagination->render();
        }

        $items = $models->find_all();


        if (isset($this->_list_properties['custom']['hierarchy']) && $this->_list_properties['custom']['hierarchy'] === true) {
            $items = $items->as_array();
            if($items) {
                $items = array_map(
                    function($element) {
                        $element = $element->as_array('id', 'name');
                        return $element;
                    },
                    $items
                );

                $return = array();
                foreach ($items as $value) {
                    $return[$value['parent']][] = $value;
                }
                outTree($return, 0, 0) ;

                global $new_items;
                $items = $new_items; // присваиваем массиву наш порядок
            }

        }

        if (isset($this->_list_properties['custom']['format_html']) && $this->_list_properties['custom']['format_html'] === true) {

            $items = $items->as_array();
            if($items) {
                $items = array_map(
                    function($element) {
                        $text = $element->text;
                        $text = htmlspecialchars_decode($text);
                        $text = strip_tags($text);
                        $text = mb_substr($text, 0, 50,'UTF-8');
                        $text = rtrim($text, "!,.-");
                        if ( strlen ($text) > 50 ) $text = $text.' [...]';
                        $element->text = $text;
                        return $element;
                    },
                    $items
                );
            }
        }


        $vars = Arr::path($this->_crud_config, 'template');
        $template = $this->_getTemplate(self::CRUD_TEMPLATE_LIST);

        $this->render($template,
            array(
                'items' => $items,
                'vars' => $vars,
                'properties' => $this->_list_properties,
                'pagination' => $pagination
            )
        );
    }

    /**
     * Create action handler
     */
    public function action_create()
    {
        /** @var $model Model_Abstract */
        $model = $this->_crud_model;
        $form = $this->_crud_form;

        if ($this->request->method() === Kohana_Request::POST) {
            $form->setData($this->request->post());

            if ($form->validate()) {
                // Save form
                $form->save();

                // Save model
                $model->values($form->getData());
                $model->save();

                Message::instance()->add(
                    'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                );

                /** @noinspection PhpUndefinedClassInspection */
                $this->redirect(
                    Route::get('admin_base')->uri(
                        array(
                            'controller' => $this->moduleName,
                            'action' => self::CRUD_EDIT_ACTION,
                            'filter' => array(
                                'id' => $model->pk()
                            )
                        )
                    )
                );
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        }

        $vars = Arr::path($this->_crud_config, 'template');

        /** @noinspection PhpUndefinedClassInspection */
        Breadcrumb::instance()->addItem(
            Arr::get($vars['text'], 'create', 'Новый элемент'),
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => self::CRUD_CREATE_ACTION
                )
            ), false
        );

        $template = $this->_getTemplate(self::CRUD_TEMPLATE_EDIT);
        $this->render($template,
            array(
                'form' => $form->render(),
                'vars' => $vars
            )
        );
    }

    /**
     * Edit action handler
     */
    public function action_edit()
    {
        $filter = $this->request->param('filter');
        $id = (isset($filter['id'])) ? intval($filter['id']) : 0;

        if (empty($id)) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => self::CRUD_LIST_ACTION
                    )
                )
            );
        }

        /** @var $model Model_Abstract */
        $model = $this->_crud_model;
        $model = $model->where('id', '=', $id)->find();

        if (!$model->loaded()) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => self::CRUD_LIST_ACTION
                    )
                )
            );
        }

        $form = $this->_crud_form;

        if ($this->request->method() === Kohana_Request::POST) {
            $form->setData($this->request->post());

            if ($form->validate()) {
                // Save form
                $data = $form->save();

                // Save model
                $model->values($data);
                $model->save();

                // Update form
                $form->setData($model->as_array());
                Message::instance()->add(
                    'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                );
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        } else {
            $form->setData($model->original_values());
        }

        // Add breadcrumb
        $name = $model->getTitleValue();

        /** @noinspection PhpUndefinedClassInspection */
        Breadcrumb::instance()->addItem(
            ($name) ? $name : 'Редактировать элемент',
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => self::CRUD_EDIT_ACTION,
                    'filter' => array(
                        'id' => $model->pk()
                    )
                )
            ), false
        );

        $template = $this->_getTemplate(self::CRUD_TEMPLATE_EDIT);
        $this->render($template,
            array(
                'item' => $model->original_values(),
                'form' => $form->render()
            )
        );
    }

    /**
     * Remove action handler
     */
    public function action_remove()
    {
        $filter = $this->request->param('filter');
        $id = (isset($filter['id'])) ? intval($filter['id']) : 0;

        if (empty($id)) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => self::CRUD_LIST_ACTION
                    )
                )
            );
        }

        /** @var $model Model_Abstract */
        $model = $this->_crud_model;
        $model = $model->where('id', '=', $id)->find();

        /** @var $model Model_Abstract */
        if ($model->loaded()) {
            $model->delete();
        }

        /** @noinspection PhpUndefinedClassInspection */
        $this->redirect(
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => self::CRUD_LIST_ACTION
                )
            )
        );
    }

    public function action_index()
    {
        /** @noinspection PhpUndefinedClassInspection */
        $this->redirect(
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => self::CRUD_LIST_ACTION
                )
            )
        );
    }

    /* Ajax methods */
    public function action_sort()
    {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $answer = array(
                'items' => array()
            );
            foreach ($this->request->post() as $field => $priority) {
                if (preg_match('/item_([0-9]+)/', $field, $match)) {
                    $item_id = intval($match[1]);
                    $priority = intval($priority);

                    /** @var $model Model_Abstract */
                    $model = clone $this->_crud_model;
                    $model->where($model->primary_key(), '=', $item_id)->find();

                    if ($model->loaded()) {
                        $answer['items'][$item_id] = true;

                        $model->priority = $priority;
                        $model->save();
                    }

                    unset($model);
                }
            }

            echo json_encode(array('data' => $answer));
            exit;
        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    public function action_drop()
    {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $answer = array(
                'items' => array()
            );

            foreach ($this->request->post() as $field => $delete) {
                if (preg_match('/item_([0-9]+)/', $field, $match) && $delete) {
                    $item_id = intval($match[1]);

                    /** @var $model Model_Abstract */
                    $model = clone $this->_crud_model;
                    $model->where($model->primary_key(), '=', $item_id)->find();

                    if ($model->loaded()) {
                        $answer['items'][$item_id] = true;
                        $model->delete();
                    }

                    unset($model);
                }
            }

            echo json_encode(array('data' => $answer));
            exit;
        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    public function action_hide()
    {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $id = $this->request->post('id');
            $answer = array('id' => $id);

            if ($id) {
                $model = $this->_crud_model->where($this->_crud_model->primary_key(), '=', $id)->find();
                if ($model->loaded()) {
                    /** @var $model Model_Abstract */
                    $status = ($model->hide) ? 0 : 1;
                    $answer['hide'] = $model->hide = $status;
                    $model->save();
                }
            }

            echo json_encode(array('data' => $answer));
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }


    public function action_fastedit() {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $id     = $this->request->post('id');
            $field  = $this->request->post('field');
            $text   = $this->request->post('text');
            $answer = array('id' => $id);

            if ($id) {
                $model = $this->_crud_model->where($this->_crud_model->primary_key(), '=', $id)->find();
                if ($model->loaded()) {
                    /** @var $model Model_Abstract */
                    $answer[$field] = $model->$field = $text;
                    $model->save();
                }
            }

            echo json_encode(array('data' => $answer));
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }


}

    function outTree($category_arr, $parent_id, $level, $all = true) {
        global $new_items;
        global $child_list;
        if (isset($category_arr[$parent_id])) { //Если категория с таким parent_id существует
            foreach ($category_arr[$parent_id] as $value) { //Обходим ее
                $value['name'] = "<div style='margin-left:" . ($level * 25) . "px;'>" . $value['name'] . "</div>";
                    $level++; //Увеличиваем уровень вложености
                if ( $all ) {
                    $new_items[]  = $value;
                    outTree($category_arr, $value['id'], $level);
                } else {
                    $child_list[] = $value['id'];
                    outTree($category_arr, $value['id'], $level, false);
                }
                $level--; //Уменьшаем уровень вложености
            }
        }
    }
