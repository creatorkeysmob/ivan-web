<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Class Admin_Controller_Pages
 */
class Admin_Controller_Pages extends Admin_Controller_Abstract {
    public $moduleName = 'pages';

    // Config
    const PAGES_CONFIG = 'admin/modules/pages';

    // Models
    const PAGES_MODEL = 'Pages';

    // Forms
    const PAGES_FORM = 'Admin_Form_Pages';

    // Views
    const PAGES_TEMPLATE_LIST = 'Admin/Pages/List';
    const PAGES_TEMPLATE_EDIT = 'Admin/Pages/Edit';

    /**
     * Generate breadcrumb for current model
     * @param \Model_Pages $current
     * @param bool $with_self
     * @return array
     */
    protected function _getBreadCrumb(Model_Pages $current, $with_self = FALSE) {
        $breadcrumbs = array();
        $parents = $current->parents(FALSE)->find_all();

        /** @var $_parent Model_Pages */
        foreach($parents as $_parent) {
            $breadcrumbs [] = array(
                'head' => $_parent->head,
                'url' => '/' . Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                        'filter' => array(
                            'parent_id' => $_parent->id
                        )
                    )
                )
            );
        }

        if($with_self) {
            $breadcrumbs [] = array(
                'head' => $current->head,
                'url' => '/' . Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                        'filter' => array(
                            'parent_id' => $current->id
                        )
                    )
                )
            );
        }
        return $breadcrumbs;
    }

    /**
     *  Index action
     */
    public function action_index(){
        setcookie("wa_admin", 1, time() + 3600 * 24, '/');

        // if( $this->request->method() === Kohana_Request::POST ) {
        //     // Save form
        //     $data = $_POST;
        //     $form = Former_Form::factory(self::PAGES_FORM);
        //     $page = ORM::factory(self::PAGES_MODEL,$_POST['section_id']);
        //     // Save page
        //     $page->values($data);
        //     $page->save();
        //     var_dump($data);exit;
        //
        //     // Update form after save
        //     $form->setData($page->as_array());
        //
        //     // Update child fullpath
        //     if($page->has_children()) {
        //         $childs = $page->descendants()->find_all();
        //         /** @var $_child Model_Pages */
        //         foreach($childs as $_child) {
        //             $_child->save();
        //         }
        //     }
        // }

        /** @noinspection PhpUndefinedClassInspection */
        $this->redirect(
            Route::get('admin_base')->uri(
                array(
                    'controller' => $this->moduleName,
                    'action' => 'list',
                    'filter' => array(
                        'parent_id' => 1
                    )
                )
            )
        );
    }

    /**
     *  List action
     */
    public function action_list(){
        $filter = $this->request->param('filter');
        if(!isset($filter['parent_id']) OR empty($filter['parent_id']) OR !is_numeric($filter['parent_id'])) {
            $filter['parent_id'] = 1;
            $compact_menu = $_COOKIE['compact_menu'];
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller'   => $this->moduleName,
                        'action'       => 'list',
                        'filter'       => $filter,
                        'compact_menu' => $compact_menu,
                    )
                )
            );
        }

        // Get parent node
        $parent_id = intval($filter['parent_id']);

        /** @var $root Model_Pages */
        $root = ORM::factory(self::PAGES_MODEL, $parent_id);
        if(!$root->loaded()) {
            throw new Exception('Pages: parent node not found');
        }

        // Get items
        $items = $root->children(FALSE,FALSE)->order_by('priority','ASC')->find_all();

        // Get breadcrumbs
        if(!empty($root->parent_id)) {
            $breadcrumb = $this->_getBreadCrumb($root, TRUE);
            foreach($breadcrumb as $_breadcrumb) {
                Breadcrumb::instance()->addItem( $_breadcrumb['head'], $_breadcrumb['url'] );
            }
            unset($breadcrumb);
        }

        // Get template vars
        $config = Kohana::$config->load(self::PAGES_CONFIG);
        $vars = Arr::path($config, 'template');
        unset($config);


        $compact_menu = $_COOKIE['compact_menu'];

        /** @noinspection PhpParamsInspection */
        $this->render( self::PAGES_TEMPLATE_LIST,
            array(
                'items'  => $items,
                'parent_id' => $parent_id,
                'vars' => $vars,
                'compact_menu' => $compact_menu,
            )
        );
    }

    /**
     *  Create action
     */
    public function action_create() {
        $filter = $this->request->param('filter');

        // Get parent node
        $parent_id = (isset($filter['parent_id'])) ? intval($filter['parent_id']) : 1;

        /** @var $root Model_Pages */
        $root = ORM::factory(self::PAGES_MODEL, $parent_id);
        if(!$root->loaded()) {
             throw new Exception('Pages: parent node not found');
        }

        /** @var $page Model_Pages */
        $page = ORM::factory(self::PAGES_MODEL);
        $form = Former_Form::factory(self::PAGES_FORM);

        if( $this->request->method() === Kohana_Request::POST ) {
            $form->setData($this->request->post());

            if( $form->validate() ) {
                // Save form
                $data = $form->save();

                // Insert page node
                $page->values($data);
                if($root->lvl > 1) {
                    $page->section_id = $root->section_id;
                }
                $page->insert_as_last_child($root);
                $page->save();

                // Generate section id
                if($page->lvl == 2) {
                    $page->section_id = $page->id;
                    $page->save();
                }

                Message::instance()->add(
                    'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                );

                /** @noinspection PhpUndefinedClassInspection */
                $this->redirect(
                    Route::get('admin_base')->uri(
                        array(
                            'controller' => $this->moduleName,
                            'action' => 'edit',
                            'filter' => array(
                                'id' => $page->id
                            )
                        )
                    )
                );
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        }

        // Get module config
        $config = Kohana::$config->load(self::PAGES_CONFIG);

        // Get breadcrumbs
        if(!empty($root->parent_id)) {
            $breadcrumb = $this->_getBreadCrumb($root, TRUE);
            foreach($breadcrumb as $_breadcrumb) {
                Breadcrumb::instance()->addItem( $_breadcrumb['head'], $_breadcrumb['url'] );
            }
            unset($breadcrumb);
        }

        /** @noinspection PhpUndefinedClassInspection */
        Breadcrumb::instance()->addItem(
            (isset($config['template']['text']['create'])) ?
                $config['template']['text']['create'] :
                'Новая страница', null, false
        );

        $this->render( self::PAGES_TEMPLATE_EDIT,
            array(
                'item' => $page,
                'form' => $form->render(),
                'parent_id' => $parent_id
            )
        );
    }

    /**
     *  Edit action
     */
    public function action_edit() {
        $filter = $this->request->param('filter');
        $id = (isset($filter['id'])) ? intval($filter['id']) : 0;

        if( empty($id) ) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                        'filter' => array(
                            'parent_id' => 1
                        )
                    )
                )
            );
        }

        /** @var $page Model_Pages */
        $page = ORM::factory(self::PAGES_MODEL,$id);
        if( !$page->loaded() ) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName,
                        'action' => 'list',
                        'filter' => array(
                            'parent_id' => 1
                        )
                    )
                )
            );
        }

        /** @var $root Model_Pages */
        $root = ORM::factory(self::PAGES_MODEL, $page->parent_id);
        if(!$root->loaded()) {
            throw new Exception('Pages: parent node not found');
        }

        $form = Former_Form::factory(self::PAGES_FORM);
        if( $this->request->method() === Kohana_Request::POST ) {
            $form->setData($this->request->post());
            if( $form->validate() ) {
                // Save form
                $data = $form->save();

                // Save page
                $page->values($data);
                $page->save();

                // Update form after save
                $form->setData($page->as_array());

                // Update child fullpath
                if($page->has_children()) {
                    $childs = $page->descendants()->find_all();
                    /** @var $_child Model_Pages */
                    foreach($childs as $_child) {
                        $_child->save();
                    }
                }

                Message::instance()->add(
                    'success', 'Успешно сохранено', Message::TYPE_SUCCESS
                );

                /** @noinspection PhpUndefinedClassInspection */
                $this->redirect(
                    Route::get('admin_base')->uri(
                        array(
                            'controller' => $this->moduleName,
                            'action' => 'edit',
                            'filter' => array(
                                'id' => $page->id
                            )
                        )
                    )
                );
            } else {
                Message::instance()->set(
                    $form->getErrors(), Message::TYPE_ERROR
                );
            }
        } else {
            $form->setData($page->original_values());
        }

        // Get breadcrumbs
        if(!empty($root->parent_id)) {
            /** @var $parents Model_Pages */
            $breadcrumb = $this->_getBreadCrumb($page);
            foreach($breadcrumb as $_breadcrumb) {
                Breadcrumb::instance()->addItem( $_breadcrumb['head'], $_breadcrumb['url'] );
            }
            unset($breadcrumb);
        }

        /** @noinspection PhpUndefinedClassInspection */
        Breadcrumb::instance()->addItem(
            ( $page->head ) ? $page->head : 'Редактировать страницу', null, false
        );

        $this->render( self::PAGES_TEMPLATE_EDIT,
            array(
                'item' => $page,
                'parent_id' => $page->parent_id,
                'section_id' => $page->section_id,
                'form' => $form->render(),
            )
        );
    }

    /**
     * Ajax sort
     * @throws HTTP_Exception_403
     */
    public function action_sort() {
        if( $this->request->is_ajax() OR $this->request->post('is_ajax') ) {
            $answer = array(
                'items' => array()
            );
            foreach($this->request->post() as $field => $priority) {
                if (preg_match('/item_([0-9]+)/', $field, $match)) {
                    $item_id = intval($match[1]);
                    $priority = intval($priority);

                    /** @var $page Model_Pages */
                    $page = ORM::factory(self::PAGES_MODEL, $item_id);

                    if( $page->loaded() ) {
                        $answer['items'][$item_id] = true;

                        $page->priority = $priority;
                        $page->save();
                    }

                    unset($page);
                }
            }

            echo json_encode( array( 'data' => $answer ) );
            exit;
        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    /**
     * Ajax drop
     * @throws HTTP_Exception_403
     */
    public function action_drop() {
        if( $this->request->is_ajax() OR $this->request->post('is_ajax') ) {
            $answer = array(
                'items' => array()
            );

            foreach($this->request->post() as $field => $delete) {
                if (preg_match('/item_([0-9]+)/', $field, $match) && $delete) {
                    $item_id = intval($match[1]);

                    /** @var $page Model_Pages */
                    $page = ORM::factory(self::PAGES_MODEL, $item_id);

                    if( $page->loaded() ) {
                        $answer['items'][$item_id] = true;
                        $page->delete();
                    }

                    unset($page);
                }
            }

            echo json_encode( array( 'data' => $answer ) );
            exit;
        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    /**
     * Ajax hide
     * @throws HTTP_Exception_403
     */
    public function action_hide() {
        if( $this->request->is_ajax() OR $this->request->post('is_ajax') ) {
            $id = $this->request->post('id');
            $answer = array( 'id' => $id );

            if($id) {
                /** @var $page Model_Pages */
                $page = ORM::factory(self::PAGES_MODEL, $id);

                if($page->loaded()) {
                    /** @var $model Model_Pages */
                    $status = ($page->hide) ? 0 : 1;
                    $answer['hide'] = $page->hide = $status;
                    $page->save();
                }
            }

            echo json_encode( array( 'data' => $answer ) );
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    public function action_menushow() {
        if( $this->request->is_ajax() OR $this->request->post('is_ajax') ) {
            $id = $this->request->post('id');

            if($id) {
                /** @var $page Model_Pages */
                $page = ORM::factory(self::PAGES_MODEL, $id);

                if($page->loaded()) {
                    /** @var $model Model_Pages */
                    $status = ($page->menushow) ? 0 : 1;
                    $page->menushow = $page->bottomshow = $status;
                    $page->save();
                }
            }

            echo $status;
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

    public function action_fastedit() {
        if ($this->request->is_ajax() OR $this->request->post('is_ajax')) {
            $id     = $this->request->post('id');
            $field  = $this->request->post('field');
            $text   = $this->request->post('text');
            $answer = array('id' => $id);

            if ($id) {
                $page = ORM::factory(self::PAGES_MODEL, $id);
                if ($page->loaded()) {
                    /** @var $page Model_Abstract */
                    $answer[$field] = $page->$field = $text;
                    $page->save();
                }
            }

            echo json_encode(array('data' => $answer));
            exit;

        } else {
            throw new HTTP_Exception_403('Allow only ajax request');
        }
    }

}
