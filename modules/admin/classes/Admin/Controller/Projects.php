<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Controller_Projects.
 */
class Admin_Controller_Projects extends Admin_Controller_Crud {
    public $moduleName = 'projects';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Projects');
        $this->_crud_form = Former_Form::factory('Admin_Form_Projects');

        // List properties
        $this->_list_properties = array(
            'sort'   => array( 'priority' => 'ASC' ),
            'fields' => array(
                //array('name' => 'created', 'label' => 'Дата'),
                array('name' => 'head',    'label' => 'Название'),
                //array('name' => 'preview', 'label' => 'Превью'),
            ),
            'custom' => array(
                'sortable'   => true,
                'pagination' => true,
                'drop'       => true,
                'hide'       => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/projects');
    }
}
