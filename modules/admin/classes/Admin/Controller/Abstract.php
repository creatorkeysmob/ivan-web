<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Admin_Controller_Abstract
 */
class Admin_Controller_Abstract extends Controller_View
{
    public $moduleName = 'abstract';
    protected $_moduleErrors = array();

    public function before()
    {
        parent::before();

        // Load modules
        $modules = Kohana::$config->load('admin/main.modules.admin');

        // Redirect if not auth
        if (Route::name($this->request->route()) === 'admin_base' AND !Auth::instance()->logged_in()) {
            $this->redirect(
                Route::get('admin_auth')->uri(
                    array(
                        'action' => 'login'
                    )
                )
            );
        } else if (Route::name($this->request->route()) === 'admin_base' && Auth::instance()->logged_in()) {
            /** @var $user_instance Model_User */
            $user_instance = Auth::instance()->get_user();
            $all_modules = array_keys($modules);
            $user_modules = json_decode($user_instance->modules, true);

            // Allow all modules for admin
            if ($user_instance->has_role(Model_User::ROLE_ADMIN)) {
                $user_modules = $all_modules;
            }

            // Deny for users, who hasn't allowed modules
            if (!sizeof($user_modules)) {
                throw new HTTP_Exception_403;
            }

            // Redirect to first allowed module
            if (!in_array($this->moduleName, $user_modules)) {
                $this->redirect(
                    Route::get('admin_base')->uri(
                        array(
                            'controller' => reset($user_modules)
                        )
                    )
                );
            }
            $this->addTemplateVariable('user_modules', $user_modules);
        }

        // Init breadcrumbs
        if (isset($modules[$this->moduleName]['name'])) {
            /** @noinspection PhpUndefinedClassInspection */
            Breadcrumb::instance()->addItem(
                $modules[$this->moduleName]['name'],
                Route::get('admin_base')->uri(
                    array(
                        'controller' => $this->moduleName
                    )
                )
            );
        }
    }

    /**
     * @return array
     */
    public function getModuleErrors()
    {
        $errors = Message::instance()->get(true, true, Message::TYPE_ERROR);
        return Arr::overwrite($this->_moduleErrors, $errors);
    }

    /**
     * @param array $errors
     */
    public function setModuleErrors($errors)
    {
        $this->_moduleErrors = $errors;
    }

    /**
     * Load template parameters
     */
    public function loadTemplateVariables()
    {
        $vars = array(
            'self'        => $this->moduleName,
            'user'        => Auth::instance()->get_user(),
            'menu'        => Kohana::$config->load('admin/main.menu'),
            'breadcrumbs' => Breadcrumb::instance()->getItems(),
            'constant'    => Kohana::$config->load('admin/constant'),
            'messages'    => array_slice(
                Message::instance()->get(), 0, 3
            ),
            'errors' => $this->getModuleErrors(),
        );

        $this->addTemplateVariable('admin', $vars);
        return parent::loadTemplateVariables();
    }
}
