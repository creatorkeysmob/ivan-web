<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Redirect.
*/
class Admin_Controller_Redirect extends Admin_Controller_Crud {
    public $moduleName = 'redirect';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Redirect');
        $this->_crud_form = Former_Form::factory('Admin_Form_Redirect');

        // List properties
        $this->_list_properties = array(
            'sort' => array( 'from' => 'ASC' ),
            'fields' => array(
                array( 'name' => 'from', 'label' => 'Старый URL' ),
                array( 'name' => 'to', 'label' => 'Новый URL' ),
            ),
            'custom' => array(
                'sortable' => false,
                'pagination' => true,
                'drop' => false,
                'hide' => true,
            ),
        );

        // Load config
        $this->_crud_config = Kohana::$config->load('admin/redirect');
    }
}

