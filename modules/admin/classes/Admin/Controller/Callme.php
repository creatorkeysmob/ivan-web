<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Callme.
*/
class Admin_Controller_Callme extends Admin_Controller_Crud {
    public $moduleName = 'callme';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Callme');
        $this->_crud_form = Former_Form::factory('Admin_Form_Callme');

        // List properties
        $this->_list_properties = array(
            'sort' => array('id' => 'DESC'),
            'fields' => array(
                array( 'name' => 'date', 'label' => 'Дата' ),
                array( 'name' => 'text', 'label' => 'Сообщение' ),
            ),
            'custom' => array(
                'format_html' => true,
                'pagination'  => false,
                'drop'        => true,
                'only_list'   => true,
                'hide'        => false,
            ),
        );
    }
}