<?php defined('SYSPATH') OR die('No direct script access.');

/**
* Class Admin_Controller_Call.
*/
class Admin_Controller_Feedback extends Admin_Controller_Crud {
    public $moduleName = 'feedback';

    public function before() {
        parent::before();

        /** @noinspection PhpUndefinedClassInspection */
        $this->_crud_model = ORM::factory('Feedback');
        $this->_crud_form = Former_Form::factory('Admin_Form_Feedback');

        // List properties
        $this->_list_properties = array(
            'sort' => array('id' => 'DESC'),
            'fields' => array(
                array( 'name' => 'date', 'label' => 'Дата' ),
                array( 'name' => 'text', 'label' => 'Сообщение' ),
            ),
            'custom' => array(
                'format_html' => true,
                'pagination'  => false,
                'drop'        => true,
                'only_list'   => true,
                'hide'        => false,
            ),
        );
    }
}
