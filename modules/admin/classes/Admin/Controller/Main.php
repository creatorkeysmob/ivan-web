<?php defined( 'SYSPATH' ) or die( 'No direct access allowed.' );

class Admin_Controller_Main extends Admin_Controller_Abstract {
    public $moduleName = 'main';

    public function action_index() {
        /** @noinspection PhpUndefinedClassInspection */
        $this->redirect(
            Route::get('admin_base')->uri(
                array(
                    'controller' => 'pages',
                    'action' => 'index'
                )
            )
        );
    }

    public function action_login() {
        // Redirect to admin
        if ( Auth::instance()->logged_in() ) {
            /** @noinspection PhpUndefinedClassInspection */
            $this->redirect(
                Route::get( 'admin_base' )->uri()
            );
        }

        // Login action
        $errors = array();
        $form = Former_Form::factory( 'admin_form_login', $this->request->post() );
        if ( $this->request->method() === Kohana_Request::POST ) {
            $login = $this->request->post( 'login' );
            $password = $this->request->post( 'password' );
            $remember = (bool)$this->request->post( 'remember' );
            if ( $form->validate() ) {
                if ( Auth::instance()->login( $login, $password, $remember ) ) {
                    $this->redirect(
                        Route::get( 'admin_base' )->uri()
                    );
                } else {
                    $errors = array( 'login' => 'Логин и/или пароль неверны' );
                    Message::instance()->set(
                        $errors, Message::TYPE_ERROR
                    );
                }
            } else {
                $errors = $form->getErrors();
                Message::instance()->set(
                    $errors, Message::TYPE_ERROR
                );
            }
        }

        //show login form
        $this->render( 'Admin/Auth/Login',
            array(
                'login'    => ( isset( $login ) ) ? $login : '',
                'remember' => ( isset( $remember ) ) ? $remember : '',
                'errors'   => $errors
            )
        );
    }

    public function action_logout() {
        setcookie("wa_admin", 1, time() - 3600, '/');
        if ( Auth::instance()->logged_in() ) {
            Auth::instance()->logout( FALSE, TRUE );
        }

        $this->redirect(
            Route::get( 'admin_auth' )->uri(
                array(
                    'action' => 'login'
                )
            )
        );
    }
}
