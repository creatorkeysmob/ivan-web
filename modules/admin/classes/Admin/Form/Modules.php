<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Modules.
 */
class Admin_Form_Modules extends Former_Form {
    public function initElements() {

        $id = explode('/admin/catalog/edit/id/', $_SERVER['REQUEST_URI']);
        $id = $id[1];

        $forms  = DB::select('head', 'alias')
                ->from('formconstruct')
                ->order_by('head', 'ASC')
                ->execute()
                ->as_array();

        $this->_elements += array(

            array(
                'type' => 'section',
                'label' => 'Модуль',
                'view' => 'open'
            ),

                'module' => array(
                    'label'   => 'Модуль',
                    'type'    => 'select2lvl',
                    'choices' => array(
                        'Map'          => array(
                            'head'   => 'Карта',
                            'alias'  => 'Map',
                            'models' => array(),
                        ),
                        'Team'         => array(
                            'head'   => 'Команда',
                            'alias'  => 'Team',
                            'models' => array(),
                        ),
                        'News'         => array(
                            'head'   => 'Новости',
                            'alias'  => 'News',
                            'models' => array(),
                        ),
                        'Review'       => array(
                            'head'   => 'Отзывы',
                            'alias'  => 'Review',
                            'models' => array(),
                        ),
                        'Partners'     => array(
                            'head'   => 'Партнёры',
                            'alias'  => 'Partners',
                            'models' => array(),
                        ),
                        'Advantages'   => array(
                            'head'   => 'Преемущества',
                            'alias'  => 'Advantages',
                            'models' => array(),
                        ),
                        'Certificates' => array(
                            'head'   => 'Сертефикаты',
                            'alias'  => 'Certificates',
                            'models' => array(),
                        ),
                        'Slider'       => array(
                            'head'   => 'Слайдер',
                            'alias'  => 'Slider',
                            'models' => array(),
                        ),
                        'Services'     => array(
                            'head'   => 'Услуги',
                            'alias'  => 'Services',
                            'models' => array(),
                        ),
                        'Form'         => array(
                            'head'   => 'Форма',
                            'alias'  => 'Form',
                            'models' => $forms,
                        ),
                        /****************** Modules by Admin The Creator ******************/
                        
#################################
#################################
                        /******************************************************************/
                    ),
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),
                'head' => array(
                    'label' => 'Заголовок',
                    'type' => 'text',
                ),
                'body' => array(
                    'label' => 'Описание',
                    'type' => 'wysiwyg',
                ),
            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}
