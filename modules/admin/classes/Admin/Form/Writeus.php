<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Writeus.
 */
class Admin_Form_Writeus extends Former_Form {
    public function initElements() {
        $this->_elements += array(
            array(
                'type'  => 'section',
                'label' => 'Напишите сообщение',
                'view'  => 'open'
            ),

            'fio' => array(
                'label'    => 'ФИО',
                'type'     => 'text',
                'required' => true,
            ),
            'email' => array(
                'label'    => 'Email',
                'type'     => 'text',
                'required' => true,
                'class'    => 'mail',
            ),
            'message' => array(
                'label'    => 'Сообщение',
                'type'     => 'textarea',
                'required' => true,
            ),


            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}

