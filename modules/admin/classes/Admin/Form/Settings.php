<?php defined('SYSPATH') or die('No direct access allowed.');

class Admin_Form_Settings extends Former_Form
{
    public function initElements() {
        $this->_elements += array(
            array(
                'type'      => 'section',
                'label'     => 'Живое редактирование',
                'view'      => 'open'
            ),
                'life_edit'   => array(
                    'label'     => 'Включить',
                    'type'      => 'checkbox',
                ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Основные настройки',
                'view'      => 'open'
            ),
                'sitename'   => array(
                    'label'     => 'Название сайта',
                    'type'      => 'text',
                ),
                'main_head'   => array(
                    'label'     => 'Слоган',
                    'type'      => 'text',
                ),
                'logo'   => array(
                    'label'     => 'Логотип',
                    'type'      => 'image',
                ),
				/*
                'favicon'   => array(
                    'label'     => 'favicon',
                    'type'      => 'image',
                ),*/
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Контакты',
                'view'      => 'open'
            ),
                'phone' => array(
                    'label'     => 'Телефон',
                    'type'      => 'text',
                ),
                'email' => array(
                    'label'     => 'Email',
                    'type'      => 'text',
                ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),



            array(
                'type'      => 'section',
                'label'     => 'Формы',
                'view'      => 'open'
            ),
                'email_feedback' => array(
                    'label'     => 'Email для сообщений',
                    'type'      => 'text',
                    'help'      => 'Можно указать несколько адресов через запятую.'
                ),
                'agreement' => array(
                    'label'     => 'Текст согласия с политикой конфиденциальности',
                    'type'      => 'wysiwyg',
                ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),


            array(
                'type'      => 'section',
                'label'     => 'Продвижение',
                'view'      => 'open'
            ),
                'prefix' => array(
                    'label'     => 'Title префикс',
                    'type'      => 'text',
                ),
                'postfix' => array(
                    'label'     => 'Title постфикс',
                    'type'      => 'text',
                ),
                'title' => array(
                    'label'     => 'Title главной',
                    'type'      => 'text',
                ),
                'description' => array(
                    'label'     => 'Description главной',
                    'type'      => 'textarea',
                ),
                'keywords' => array(
                    'label'     => 'Keywords главной',
                    'type'      => 'textarea',
                ),

                array(
                    'type'      => 'section',
                    'view'      => 'close'
                ),

                array(
                    'type'      => 'section',
                    'label'     => 'Соц.сети в меню',
                    'view'      => 'open'
                ),

                'link_vk' => array(
                    'label'     => 'Ссылка на вконтакте',
                    'type'      => 'text',
                ),

                'link_inst' => array(
                    'label'     => 'Ссылка на инстаграм',
                    'type'      => 'text',
                ),

                'link_yb' => array(
                    'label'     => 'Ссылка на ютуб',
                    'type'      => 'text',
                ),

                'link_fb' => array(
                    'label'     => 'Ссылка на фейсбук',
                    'type'      => 'text',
                ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Скрипты',
                'view'      => 'open'
            ),
                'script_head' => array(
                    'label'     => 'В шапке', // в шапке
                    'type'      => 'script',
                    'attributes' => array(
                        'style' => 'height: 200px;'
                    )
                ),
                'script_footer' => array(
                    'label'     => 'В футере', // в футере
                    'type'      => 'script',
                    'attributes' => array(
                        'style' => 'height: 200px;'
                    )
                ),
                'map' => array(
                    'label'     => 'Карта', // в шапке
                    'type'      => 'script',
                    'attributes' => array(
                        'style' => 'height: 200px;'
                    )
                ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
        );

        parent::initElements();
    }
}
