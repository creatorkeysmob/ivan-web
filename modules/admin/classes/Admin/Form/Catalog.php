<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Catalog.
 */
class Admin_Form_Catalog extends Former_Form {
    public function initElements() {

        $id = explode('/admin/catalog/edit/id/', $_SERVER['REQUEST_URI']);
        $id = $id[1];

        // $brands  = DB::select('head','id', 'models')
        //         ->from('brands')
        //         ->order_by('head', 'ASC')
        //         ->execute()
        //         ->as_array();

        // $brandsNmodels = array();
        // foreach( $brands as $brand ){
        //     $brandsNmodels[] = array(
        //         'id'     => $brand['id'],
        //         'head'   => $brand['head'],
        //         'models' => unserialize($brand['models']),
        //     );
        // }


        // unset($brands);
        //var_dump($brandsNmodels);exit;


        // $brandsNmodels = array(
        //                     'Бренд 1' => 'Бренд 1',
        //                     'Бренд 2' => 'Бренд 2',
        //                     'Бренд 3' => 'Бренд 3',
        //                     'Бренд 4' => 'Бренд 4',
        //                     'Бренд 5' => 'Бренд 5',
        //                     'Бренд 6' => 'Бренд 6'
        //                 );

        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),
            'img' => array(
                'label' => 'Изображение',
                'type'  => 'image',
            ),
            'head' => array(
                'label' => 'Название',
                'type' => 'text',
                'required' => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),
            'alias' => array(
                'label'     => 'Ссылка',
                'type'      => 'link',
            ),
            // 'brand' => array(
            //     'label'   => 'Линейки',
            //     'type'    => 'select2lvl',
            //     'choices' => $brandsNmodels,
            // ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Тип',
                'view'      => 'open'
            ),
            'new' => array(
                'label'     => 'NEW',
                'type'      => 'checkbox',
            ),
            'hit' => array(
                'label'     => 'Хит',
                'type'      => 'checkbox',
            ),
            'price' => array(
                'label'     => 'Цена',
                'type'      => 'integer',
            ),
            'discount' => array(
                'label'     => 'Скидка',
                'type'      => 'integer',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Характеристики',
                'view'      => 'open'
            ),

            'articul' => array(
                'label'     => 'Артикул',
                'type'      => 'text',
            ),
            'industry' => array(
                'label'     => 'Отрасль',
                'type'      => 'text',
            ),
            'functions' => array(
                'label'     => 'Назначение',
                'type'      => 'text',
            ),
            'product' => array(
                'label'     => 'Продукт',
                'type'      => 'text',
            ),


            'body' => array(
                'label' => 'Описание',
                'type' => 'wysiwyg',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type'      => 'section',
                'label'     => 'Альбом',
                'view'      => 'open'
            ),
            'album' => array(
                'label'     => 'Изображения',
                'type'      => 'albumv2',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),



            array(
                'type'      => 'section',
                'label'     => 'Продвижение',
                'view'      => 'open'
            ),
            'title' => array(
                'label'     => 'Title страницы',
                'type'      => 'text',
            ),
            'description' => array(
                'label'     => 'Description страницы',
                'type'      => 'textarea',
            ),
            'keywords' => array(
                'label'     => 'Keywords страницы',
                'type'      => 'textarea',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

        );
        parent::initElements();
    }
}
