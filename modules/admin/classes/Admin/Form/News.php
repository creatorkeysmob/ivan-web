<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_News.
 */
class Admin_Form_News extends Former_Form {
    public function initElements() {

        $id = explode('/admin/news/edit/id/', $_SERVER['REQUEST_URI']);
        $id = $id[1];

        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),

            // 'qwe' => array(
            //     'label' => 'Фотоальбом',
            //     'type'  => 'albumv2',
            //     'id'    => $id,
            // ),

            'img' => array(
                'label' => 'Изображение',
                'type' => 'image',
            ),

            'created' => array(
                'label' => 'Дата',
                'type' => 'datetime',
            ),

            'head' => array(
                'label' => 'Название',
                'type' => 'text',
                'required' => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),

            'alias' => array(
                'label' => 'Ссылка',
                'type' => 'link',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Описания',
                'view' => 'open'
            ),
            'preview' => array(
                'label' => 'Текст превью',
                'type' => 'wysiwyg',
            ),

            'body' => array(
                'label' => 'Основной текст',
                'type' => 'wysiwyg',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

  	        array(
                'type'      => 'section',
                'label'     => 'Продвижение',
                'view'      => 'open'
                ),
                'title' => array(
                    'label'     => 'Title страницы',
                    'type'      => 'text',
                ),
                'description' => array(
                    'label'     => 'Description страницы',
                    'type'      => 'textarea',
                ),
                'keywords' => array(
                    'label'     => 'Keywords страницы',
                    'type'      => 'textarea',
                ),

                array(
                    'type'      => 'section',
                    'view'      => 'close'
                ),

        );
        parent::initElements();
    }
}
