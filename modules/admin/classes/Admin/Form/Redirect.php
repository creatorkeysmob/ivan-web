<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Redirect.
 */
class Admin_Form_Redirect extends Former_Form {
    public function initElements() {
        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),
            'from' => array(
                'label' => 'Старый URL',
                'type' => 'text',
                'required'  => true,
                'help'      => 'Ссылка должна быть относительной, например "/articles/page?test=1"',
                'rules' => array(
                    array('not_empty'),
                ),
            ),
            'to' => array(
                'label' => 'Новый URL',
                'type' => 'text',
                'required'  => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),
            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}

