<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Call.
 */
class Admin_Form_Formconstruct extends Former_Form {
    public function initElements() {
        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),
                'head' => array(
                    'label' => 'Заголовок',
                    'type'  => 'text',
                ),
                'alias' => array(
                    'label' => 'Код',
                    'type'  => 'link',
                ),
                'body' => array(
                    'label' => 'Текст',
                    'type'  => 'wysiwyg',
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

            array(
                'type'  => 'section',
                'label' => 'Поля',
                'view'  => 'open'
            ),

                'fields' => array(
                    'label' => '',
                    'type'  => 'groups',
                    'id'    => $id,
                    'param' => array(
                        0 => array( 'title' => 'Обязательный', 'type' => 'checkbox', ),
                        1 => array( 'title' => 'Название',     'type' => 'text', ),
                        2 => array( 'title' => 'Тип',          'type' => 'select',
                            'options' => array(
                                'text'          => 'Текстовое поле',
                                'phone'         => 'Только цифры',
                                'email'         => 'Email',
                                'textarea'      => 'Сообщение',
                                'file'          => 'Файл',
                                'calendar'      => 'Календарь',
                                'calendar_time' => 'Календарь и время',
                                'time'          => 'Время',
                                'checkbox'      => 'Чекбокс',
                                'checkboxes'    => 'Группа чекбоксов',
                                'select'        => 'Селект',
                                'multyselect'   => 'Мультиселект',
                                'radio'         => 'Группа радиокнопок',
                            ),
                            'params' => 'b_select b_multyselect b_checkboxes b_radio b_file',
                        ),
                     ),
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

            array(
                'type'  => 'section',
                'label' => 'Сообщения',
                'view'  => 'open'
            ),
                'tnx' => array(
                    'label' => 'Текст об успешной отправке письма',
                    'type'  => 'wysiwyg',
                ),
            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}
