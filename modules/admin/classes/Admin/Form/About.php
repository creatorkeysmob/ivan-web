<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_About.
 */
class Admin_Form_About extends Former_Form {
    public function initElements() {
      $pages = DB::select('head','id')->from('pages')
                       ->where('hide', '=', '0')
                       ->and_where('module', '=', 'about')
                       ->order_by('priority', 'ASC')
           ->execute()
           ->as_array('id','head');

           $id = explode('/admin/about/edit/id/', $_SERVER['REQUEST_URI']);
           $id = $id[1];

        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Первый блок',
                'view' => 'open'
            ),

            'page' => array(
                'label'     => 'Страницы',
                'type'      => 'select',
                'choices'   => $pages,
            ),

            'head' => array(
                'label' => 'Название',
                'type' => 'text',
                'required' => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),

            'post' => array(
                'label' => 'Чем занимается организация',
                'type' => 'text',
            ),

            'logo_menu' => array(
                'label' => 'Меню из логотипов',
                'type' => 'groups',
                'id'    => $id,
                'param' => array(
                    0 => array( 'title' => 'Ссылка',      'type' => 'link', ),
                    1 => array( 'title' => 'Логотип',      'type' => 'img', ),
                 ),
            ),

            'background' => array(
                'label' => 'Фон',
                'type' => 'image',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок Обо мне',
                'view' => 'open'
            ),

            'block1_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block1_body' => array(
                'label' => 'Текст блока',
                'type' => 'wysiwyg',
            ),

            'block1_img' => array(
                'label' => 'Фото Ивана',
                'type' => 'image',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок Я могу поделиться с вами',
                'view' => 'open'
            ),

            'block2_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block2_body' => array(
                'label' => 'Текст блока',
                'type' => 'wysiwyg',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок со статьями',
                'view' => 'open'
            ),

            'block3_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block3_body' => array(
                'label' => 'Текст блока',
                'type' => 'groups',
                'id'    => $id,
                'param' => array(
                    0 => array( 'title' => 'Название',      'type' => 'text', ),
                    1 => array( 'title' => 'Ссылка',      'type' => 'link', ),
                    2 => array( 'title' => 'Превью',      'type' => 'textarea', ),
                    3 => array( 'title' => 'Автор',      'type' => 'text', ),
                    4 => array( 'title' => 'Картинка',      'type' => 'img', ),
                 ),
            ),

            'block3_link' => array(
                'label' => 'Ссылка на все статьи',
                'type' => 'text',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            array(
                'type' => 'section',
                'label' => 'Блок со слайдером с видео',
                'view' => 'open'
            ),

            'block4_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block4_body' => array(
                'label' => 'Слайдер с видео',
                'type' => 'wysiwyg',
            ),

            'block4_link' => array(
                'label' => 'Ссылка на все видео',
                'type' => 'text',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок Фотогалереи из инстаграма',
                'view' => 'open'
            ),

            'block5_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block5_body' => array(
                'label' => 'Фотогалерея из инстаграма',
                'type'  => 'albumv2',
            ),

            'block5_link' => array(
                'label' => 'Ссылка на инстаграм',
                'type' => 'text',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок внизу с формой',
                'view' => 'open'
            ),

            'block6_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block6_body' => array(
                'label' => 'Текст',
                'type'  => 'wysiwyg',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),


        );
        parent::initElements();
    }
}
