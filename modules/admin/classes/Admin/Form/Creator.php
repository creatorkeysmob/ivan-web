<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Call.
 */
class Admin_Form_Creator extends Former_Form {
    public function initElements() {
        $this->_elements += array(
            /******************************/
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),
                'head' => array(
                    'label' => 'Название',
                    'type'  => 'text',
                    'required'  => true,
                    'rules'     => array(
                        array('not_empty')
                    ),
                ),
                'alias' => array(
                    'label' => 'Название (En)',
                    'type'  => 'link',
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

            /******************************/
            array(
                'type'  => 'section',
                'label' => 'Опции',
                'view'  => 'open'
            ),

                'img'  => array(
                    'label'     => 'Изображение',
                    'type'      => 'checkbox',
                ),

                'lvl'  => array(
                    'label'     => 'Вложенные страницы',
                    'type'      => 'checkbox',
                ),
                'search'  => array(
                    'label'     => 'Включить в поиск',
                    'type'      => 'checkbox',
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

            /******************************/
            array(
                'type'  => 'section',
                'label' => 'Модули главной',
                'view'  => 'open'
            ),
                'main'  => array(
                    'label'     => 'Добавить в модули главной',
                    'type'      => 'checkbox',
                ),
                'main_count'  => array(
                    'label'     => 'Количество',
                    'type'      => 'integer',
                ),
                'tmpl'  => array(
                    'label'     => 'Шаблон',
                    'type'      => 'imgcheckbox',
                    'choices'     => array(
                        1 => array('img' => '/assets/admin/images/tmpl/1.png', 'title' => 'Новость'),
                        2 => array('img' => '/assets/admin/images/tmpl/2.png', 'title' => 'ФИО'),
                        3 => array('img' => '/assets/admin/images/tmpl/3.png', 'title' => 'Просто фотка'),
                        4 => array('img' => '/assets/admin/images/tmpl/4.png', 'title' => 'Слайдер'),
                        5 => array('img' => '/assets/admin/images/tmpl/5.png', 'title' => 'Услуга'),
                        6 => array('img' => '/assets/admin/images/tmpl/6.png', 'title' => 'Без изображения'),
                    ),
                ),
            array(
                'type' => 'section',
                'view' => 'close'
            ),

            /******************************/
            array(
                'type'  => 'section',
                'label' => 'Поля',
                'view'  => 'open'
            ),
                'fields' => array(
                    'label' => 'Дополнительные параметры',
                    'type'  => 'groups',
                    'id'    => $id,
                    'param' => array(
                        0 => array( 'title' => 'Название параметра',      'type' => 'text', ),
                        1 => array( 'title' => 'Название параметра (En)', 'type' => 'en_text', ),
                        2 => array( 'title' => 'Тип',                     'type' => 'select',
                            'options' => array(
                                'text;varchar(255)'  => 'Строка',
                                'textarea;text'      => 'Текстовое поле',
                                'wysiwyg;text'       => 'Визуальный редактор',
                                'image;varchar(255)' => 'Изображение',
                                'datetime;datetime'  => 'Дата',
                                'int(11);integer'    => 'Число',
                                'file;varchar(255)'  => 'Файл',
                            ),
                            'params' => false,
                        ),
                     ),
                ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}
