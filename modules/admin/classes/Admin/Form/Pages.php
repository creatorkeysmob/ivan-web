<?php defined('SYSPATH') or die('No direct access allowed.');

class Admin_Form_Pages extends Former_Form
{
    public function initElements() {
        // Load allowed modules from config
        $modules = Kohana::$config->load('admin/main.modules.public');
        if( !$modules ) {
            throw new Exception('Pages: public modules does not exists');
        }
        // Prepare select options
        $options = array();
        foreach($modules as $_key => $_value) {
            $options[$_key] = $_value['name'];
        }
        // Load allowed templates from config
        $templates = Kohana::$config->load('admin/main.modules.templates');
        if( !$templates ) {
            throw new Exception('Pages: public modules does not exists');
        }
        // Prepare select options
        $templates_opt = array();
        foreach($templates as $_key => $_value) {
            $templates_opt[$_value['template']] = $_value['name'];
        }


        // Prepare form
        $this->_elements += array(
            'parent_id' => array(
                'type'      => 'hidden',
                'render'    => false
            ),
            'section_id' => array(
                'type'      => 'hidden',
                'render'    => false
            ),
            'menushow' => array(
                'type'      => 'text',
                'default'   => 0,
                'render'    => false
            ),
            'bottomshow' => array(
                'type'      => 'text',
                'default'   => 0,
                'render'    => false
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            array(
                'type'      => 'divider',
            ),
            array(
                'type'      => 'section',
                'label'     => 'Основная информация',
                'view'      => 'open'
            ),
            'head' => array(
                'label'     => 'Название',
                'type'      => 'text',
                'required'  => true,
                'rules'     => array(
                    array('not_empty')
                ),
            ),
            'alias' => array(
                'label'     => 'Псевдоним',
                'type'      => 'link',
            ),
            'created' => array(
                'label'     => 'Дата публикации',
                'type'      => 'datetime',
            ),
/*            'redirect' => array(
                'label'     => 'Перенаправление',
                'type'      => 'text',
            ),*/
            'module' => array(
                'label'     => 'Модуль',
                'type'      => 'select',
                'choices'   => $options
            ),
            // 'template' => array(
            //     'label'     => 'Шаблон',
            //     'type'      => 'select',
            //     'choices'   => $templates_opt,
            // ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            array(
                'type'      => 'divider',
            ),


            array(
                'type'      => 'section',
                'label'     => 'Текстовая информация',
                'view'      => 'open'
            ),
            // 'prew' => array(
            //     'label'     => 'Дополнительный текст',
            //     'type'      => 'wysiwyg',
            // ),
            'body' => array(
                'label'     => 'Основной текст',
                'type'      => 'wysiwyg',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
            array(
                'type'      => 'divider',
            ),
            array(
                'type'      => 'section',
                'label'     => 'Продвижение',
                'view'      => 'open'
            ),
            'title' => array(
                'label'     => 'Title страницы',
                'type'      => 'text',
            ),
            'description' => array(
                'label'     => 'Description страницы',
                'type'      => 'textarea',
            ),
            'keywords' => array(
                'label'     => 'Keywords страницы',
                'type'      => 'textarea',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
        );

        parent::initElements();
    }
}
