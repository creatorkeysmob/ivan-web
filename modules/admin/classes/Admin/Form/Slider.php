<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Slider.
 */
class Admin_Form_Slider extends Former_Form {
    public function initElements() {
        $this->_elements += array(

            array(
                'type'  => 'section',
                'label' => 'Картинка',
                'view' => 'open'
            ),
            'img' => array(
                'label' => 'Изображение',
                'type'  => 'image',
            ),
            'head' => array(
                'label' => 'Название',
                'type'  => 'text',
            ),

            'logo' => array(
                'label' => 'Логотип',
                'type'  => 'image',
            ),

            'background' => array(
                'label' => 'Фон слайда',
                'type'  => 'image',
            ),

            'btn_link' => array(
                'label' => 'Текст кнопки',
                'type'  => 'text',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Цвета',
                'view' => 'open'
            ),
            'btn_color' => array(
                'label' => 'Цвет кнопки',
                'type'  => 'color',
            ),

            'btn_hover' => array(
                'label' => 'Цвет ховера кнопки',
                'type'  => 'color',
            ),

            'nav_color' => array(
                'label' => 'Цвет кнопок навигации',
                'type'  => 'color',
            ),

            'nav_hover' => array(
                'label' => 'Цвет ховера кнопок навигации',
                'type'  => 'color',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Описания',
                'view' => 'open'
            ),

            'post' => array(
                'label' => 'Описание',
                'type'  => 'text',
            ),

            'quotes' => array(
                'label' => 'Цитата',
                'type'  => 'text',
            ),

            'alias' => array(
                'label' => 'Ссылка',
                'type'  => 'text',
            ),

            array(
                'type' => 'section',
                'view' => 'close'
            ),

        );
        parent::initElements();
    }
}
