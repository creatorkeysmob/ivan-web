<?php defined('SYSPATH') or die('No direct access allowed.');

class Admin_Form_Login extends Former_Form
{
    public function initElements() {
        $this->_elements += array(
            'login' => array(
                'type'      => 'text',
                'label'     => 'Логин',
                'rules'     => array(
                    array('not_empty')
                ),
            ),

            'password' => array(
                'type'     => 'password',
                'label'    => 'Пароль',
            )
        );
        parent::initElements();
    }
}