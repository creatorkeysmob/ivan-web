<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Events.
 */
class Admin_Form_Events extends Former_Form {
    public function initElements() {

        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),

            'img' => array(
                'label' => 'Изображение',
                'type' => 'image',
            ),

            'date' => array(
                'label' => 'Дата',
                'type' => 'datetime',
            ),

            'head' => array(
                'label' => 'Название',
                'type' => 'text',
                'required' => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Описания',
                'view' => 'open'
            ),
            
            'place' => array(
                'label' => 'Место',
                'type' => 'text',
            ),

            'body' => array(
                'label' => 'Основной текст',
                'type' => 'wysiwyg',
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),


        );
        parent::initElements();
    }
}
