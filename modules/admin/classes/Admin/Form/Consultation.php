<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Consultation.
 */
class Admin_Form_Consultation extends Former_Form {
    public function initElements() {
        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Основная информация',
                'view' => 'open'
            ),

            'date' => array(
                'label' => 'Дата',
                'type' => 'textdisabled',
            ),
            'text' => array(
                'label' => 'Сообщение',
                'type' => 'textareadisabled',
            ),
            array(
                'type' => 'section',
                'view' => 'close'
            ),
        );
        parent::initElements();
    }
}