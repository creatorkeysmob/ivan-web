<?php defined('SYSPATH') or die('No direct access allowed.');

class Admin_Form_Users extends Former_Form {
    public function initElements() {
        // Load allowed modules from config
        $modules = Kohana::$config->load('admin/main.modules.admin');

        // Prepare select options
        $options = array();
        foreach($modules as $_key => $_value) {
            $options[$_key] = $_value['name'];
        }
        $this->_elements += array(
            array(
                'type'      => 'section',
                'label'     => 'Основная информация',
                'view'      => 'open'
            ),
            'username' => array(
                'label'    => 'Логин',
                'type'     => 'text',
                'required' => true,
                'rules'     => array(
                    array('not_empty')
                ),
            ),
            'password' => array(
                'label'    => 'Пароль',
                'type'     => 'password',
                'help'     => 'Пароль обязательно указывать при регистрации пользователя.'
            ),
            'fio'      => array(
                'label'    => 'ФИО',
                'type'     => 'text',
            ),
            'email'    => array(
                'label'    => 'Эл.почта',
                'type'     => 'text',
                'required' => true,
                'rules'     => array(
                    array('not_empty'),
                    array('email')
                ),
            ),
            array(
                'type' => 'divider',
            ),
            'role'     => array(
                'label'    => 'Роль',
                'type'     => 'select',
                'choices'  => array(
                    1 => 'Пользователь',
                    2 => 'Администратор'
                )
            ),
            'modules' => array(
                'label' => 'Доступ',
                'type' => 'autocomplete',
                'default' => array(),
                'choices' => $options,
                'help'     => 'Применимо только для роли "Пользователь"'
            ),
            array(
                'type'      => 'section',
                'view'      => 'close'
            ),
        );

        parent::initElements();
    }
}