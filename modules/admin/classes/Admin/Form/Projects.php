<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Admin_Form_Projects.
 */
class Admin_Form_Projects extends Former_Form {
    public function initElements() {
      $pages = DB::select('head','id')->from('pages')
                       //->where('hide', '=', '0')
                       ->and_where('module', '=', 'projects')
                       ->order_by('priority', 'ASC')
           ->execute()
           ->as_array('id','head');

           $id = explode('/admin/projects/edit/id/', $_SERVER['REQUEST_URI']);
           $id = $id[1];
//var_dump($pages);
        $this->_elements += array(
            array(
                'type' => 'section',
                'label' => 'Первый блок',
                'view' => 'open'
            ),
            'page' => array(
                'label'     => 'Страницы',
                'type'      => 'select',
                'choices'   => $pages
            ),

            'logo' => array(
                'label' => 'Логотип',
                'type' => 'image',
            ),
            'background' => array(
                'label' => 'Фон',
                'type' => 'image',
            ),

            'head' => array(
                'label' => 'Название проекта',
                'type' => 'text',
                'required' => true,
                'rules' => array(
                    array('not_empty'),
                ),
            ),

            'post' => array(
                'label' => 'Чем занимается организация',
                'type' => 'text',
            ),

            'info_in_head' => array(
                'label' => 'Количество участников',
                'type'  => 'groups',
                'id'    => $id,
                'param' => array(
                    0 => array( 'title' => 'Число',      'type' => 'text', ),
                    1 => array( 'title' => 'Участники',  'type' => 'text', ),
                    2 => array( 'title' => 'Что делают', 'type' => 'text', ),
                 ),
            ),

            // 'link' => array(
            //     'label' => 'Ссылка на сайт организации',
            //     'type' => 'link',
            // ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок О проекте',
                'view' => 'open'
            ),

            'block1_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block1_body' => array(
                'label' => 'Текст блока',
                'type' => 'wysiwyg',
            ),

            'block1_img' => array(
                'label' => 'Фото Ивана',
                'type' => 'image',
            ),

            'block1_bg' => array(
                'label' => 'Фон блока',
                'type' => 'image',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок со слайдером с видео',
                'view' => 'open'
            ),

            'block2_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block2_body' => array(
                'label' => 'Слайдер с видео',
                'type' => 'wysiwyg',
                /*'type' => 'groups',
				'id'    => $id,
                'param' => array(
                    0 => array( 'title' => 'Видео',   'type' => 'video', ),
                    1 => array( 'title' => 'Картинка превью видео',      'type' => 'img', ),
                 ),*/
            ),

            'block2_bg' => array(
                'label' => 'Фон',
                'type' => 'image',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок с текстами из статей',
                'view' => 'open'
            ),

            'block3' => array(
                'label' => 'Текст блока',
                'type'  => 'groups',
                'id'    => $id,
                'param' => array(
                    0 => array( 'title' => 'Название',   'type' => 'text', ),
                    1 => array( 'title' => 'Текст',      'type' => 'textarea', ),
                    2 => array( 'title' => 'Автор',      'type' => 'text', ),
                 ),
            ),

            'block3_bg' => array(
                'label' => 'Фон',
                'type' => 'image',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок Фотогалереи',
                'view' => 'open'
            ),

            'block4_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block4_body' => array(
                'label' => 'Фотогалерея',
                'type'  => 'albumv2',
            ),

            'block4_bg' => array(
                'label' => 'Фон',
                'type' => 'image',
            ),

            'block4_btn_color' => array(
                'label' => 'Цвет кнопок слайдера',
                'type'  => 'color',
            ),

            'block4_btn_hover' => array(
                'label' => 'Цвет ховера кнопок слайдера',
                'type'  => 'color',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок Фотогалереи из инстаграма',
                'view' => 'open'
            ),

            'block5_title' => array(
                'label' => 'Заголовок блока',
                'type' => 'text',
            ),

            'block5_body' => array(
                'label' => 'Фотогалерея из инстаграма',
                'type'  => 'albumv2',
            ),

            // 'block5_bg' => array(
            //     'label' => 'Фон',
            //     'type' => 'image',
            // ),
            // 'block5_link' => array(
            //     'label' => 'Ссылка на инстаграм организации',
            //     'type' => 'link',
            // ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),

            array(
                'type' => 'section',
                'label' => 'Блок со ссылками на соц.сети',
                'view' => 'open'
            ),

            // 'block6_link' => array(
            //     'label' => 'Ссылка на сайт организации',
            //     'type' => 'link',
            // ),
            'link' => array(
                'label' => 'Ссылка на сайт организации',
                'type' => 'text',
            ),

            'block6_link_name' => array(
                'label' => 'Текст ссылки',
                'type' => 'text',
            ),

            'block6_bg' => array(
                'label' => 'Фон блока',
                'type' => 'image',
            ),

            'vk' => array(
                'label' => 'Ссылка на вк организации',
                'type' => 'text',
            ),
            'inst' => array(
                'label' => 'Ссылка на инстаграм организации',
                'type' => 'text',
            ),
            'youtube' => array(
                'label' => 'Ссылка на youtube организации',
                'type' => 'text',
            ),
            'fb' => array(
                'label' => 'Ссылка на фейсбук организации',
                'type' => 'text',
            ),
            'social_color' => array(
                'label' => 'Цвет кнопок соц. сетей',
                'type'  => 'color',
            ),
            'social_hover' => array(
                'label' => 'Цвет ховера кнопок соц. сетей',
                'type'  => 'color',
            ),

            array(
                'type'      => 'section',
                'view'      => 'close'
            ),


        );
        parent::initElements();
    }
}
